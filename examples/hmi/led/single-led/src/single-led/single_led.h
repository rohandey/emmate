/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   single_led.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The a LED blink in different pattern using led-helper lib
 *
 *
 *
 */

#ifndef SINGLE_LED_H_
#define SINGLE_LED_H_

#include "emmate.h"
#include "thing.h"


/**
 * @brief	Init function for single_led module
 *
 */
void single_led_init();

/**
 * @brief	Execution function for single_led module
 *
 */
void single_led_loop();

#endif	/* SINGLE_LED_H_ */
