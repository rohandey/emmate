
/*
 * unix_current_time.h
 *
 *  Created on: 27-May-2019
 *      Author: Pawan Kumar Kashyap
 */
#include "core_config.h"
#include "core_constant.h"
#include "core_error.h"
#include "core_logger.h"
#include "threading.h"
#include "hmi.h"
#include "systime.h"
#include "thing.h"


/**
 * @brief This function start the led blink as per unix time
 *
 * @return
 *
 **/
void show_startup_notification();




