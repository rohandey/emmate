/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   button_interaction.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Human interaction with the system using Button
 *
 *
 *
 */

#include "button_interaction.h"
#include "button_helper.h"
#include "gpio_helper_api.h"
#include "gpio_core.h"

#define BLINK_GPIO 		LED_4_GPIO

#define TAG "button_interaction"

#define DELAY_TIME		DELAY_10_SEC

static uint32_t bit = 0;

/********************************************** Module's Static Functions **********************************************************************/

void btn_interaction_handler(uint8_t io_pin, BTN_INTERACTION_TYPE intact_type) {

	if (io_pin == SWITCH_2_GPIO && intact_type == DOUBLE_PRESSED) {
		bit ^= 1;
		// set the GPIO pin value 1 for On & 0 for Off
		if (bit == 1) {
			CORE_LOGI(TAG, "Button pressed LED On");
		} else {
			CORE_LOGI(TAG, "Button pressed LED Off");
		}
		set_gpio_value(BLINK_GPIO, bit);
	}
}

/*******************************************************************************************************************/

void button_interaction_init() {

	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s\r\n", YOUR_THING_NAME);

	core_err res = configure_gpio(BLINK_GPIO, GPIO_IO_MODE_OUTPUT,
			GPIO_IO_FLOATING);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "Failed to initialize BLINK_GPIO");
	}

	/*
	 * Initialize the basic Buttons activity. (button details are in thing/thing.h)
	 */
	core_err ret = init_double_press_btn_interface(SWITCH_2_GPIO,
			GPIO_IO_FLOATING, btn_interaction_handler);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Failed to initialize the SWITCH_2");
	}

}

void button_interaction_loop() {
	CORE_LOGD(TAG, "*** Start of %s ***\r\n", __func__);

	/*
	 * In this example the Led will glow when the button pressed and again it will off when button pressed.
	 */

	// write down your code...
	CORE_LOGD(TAG, "*** End of %s ***\r\n", __func__);

	TaskDelay(DELAY_20_SEC / TICK_RATE_TO_MS);

}
