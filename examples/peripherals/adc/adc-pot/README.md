# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

<img src="res/fritzing/esp32_adc_pot.png" width="500">

[//]: ![ESP32-DevKit-C](fritzing/esp32_adc_pot.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to use the `peripherals/adc` module's API to read an analog voltage. To select a different the analog pin change the macro `ADC_POT_DETECT_AN_GPIO` in `thing/thing.h`

#### Example specific configurations
This example is configured ..

<img src="res/adc-example-config.png" width="500">

The example does the following:

- Initializes an analog pin via `init_adc_peripheral()` function
- Reads the raw value and equivalent voltage by calling `get_adc_peripheral_data()`
- Prints the values whenever there is a change