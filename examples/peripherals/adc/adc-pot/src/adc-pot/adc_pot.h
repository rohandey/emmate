/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   adc_pot.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The use of ADC to read voltage from potentiometer
 *
 *
 *
 */

#include "emmate.h"
#include "thing.h"

/**
 * @brief	Init function for adc_pot module
 *
 */
void adc_pot_init();

/**
 * @brief	Execution function for adc_pot module
 *
 */
void adc_pot_loop();

