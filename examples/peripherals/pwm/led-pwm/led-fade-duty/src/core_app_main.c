
#include "led_fade_duty.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	CORE_LOGI(TAG, "Calling led_pwm_init() in led_pwm.c in led_pwm directory ...");
	led_pwm_init();
	CORE_LOGI(TAG, "Returned from led_pwm_init()");

	while(1){
		led_pwm_loop();
		TaskDelay(DELAY_20_MSEC/ TICK_RATE_TO_MS);
	}
}
