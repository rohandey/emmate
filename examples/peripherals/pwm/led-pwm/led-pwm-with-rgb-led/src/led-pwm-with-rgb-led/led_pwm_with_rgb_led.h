/**
 * This is an example header file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#ifndef YOUR_MODULE_H_
#define YOUR_MODULE_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

typedef enum {
	WHITE = 0,
	RED,
	LIME,
	BLUE,
	YELLOW,
	CYAN,
	MAGENTA,
	SILVER,
	GRAY,
	MAROON,
	OLIVE,
	GREEN,
	PURPLE,
	TEAL,
	NAVY,
} RGB_COLOURS;

/**
 *
 * */
void led_pwm_with_rgb_led_init();

/**
 * */
void led_pwm_with_rgb_led_loop();

#endif	/* YOUR_MODULE_H_ */
