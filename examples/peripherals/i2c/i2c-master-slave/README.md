# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

<img src="res/fritzing/i2c_master_slave.png" width="500">

[//]: ![image](fritzing/i2c_master_slave.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates the use of `peripherals/i2c` module's Master & Slave APIs. The `MASTER_SDA_GPIO`, `MASTER_SCL_GPIO`, `SLAVE_SDA_GPIO` & `SLAVE_SCL_GPIO` (i2c pins) are defined in `thing/thing.h`

#### Example specific configurations
This example is configured ..

<img src="res/i2c-example-config.png" width="500">

This example does the following:

- Initializes an i2c master client using `init_i2c_core_master()` function
- Initializes an i2c slave client using `init_i2c_core_slave()` function
- Exchange data between through Master & Slave I2C port
- Prints the read & write values