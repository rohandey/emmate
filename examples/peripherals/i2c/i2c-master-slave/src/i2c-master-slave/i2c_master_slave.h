/**
 * This is an example header file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#ifndef I2C_MASTER_SLAVE_H_
#define I2C_MASTER_SLAVE_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

/**
 *
 * */
void i2c_master_slave_init();

/**
 * */
void i2c_master_slave_loop();

#endif	/* I2C_MASTER_SLAVE_H_ */
