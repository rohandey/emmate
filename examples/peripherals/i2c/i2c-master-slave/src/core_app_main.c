
#include "i2c_master_slave.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	i2c_master_slave_init();

	while(1){
		i2c_master_slave_loop();
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
