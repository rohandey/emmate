# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

<img src="res/fritzing/esp32_eeprom_module.png" width="500">

[//]: ![image](fritzing/esp32_eeprom_module.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates the use of `peripherals/i2c` module APIs. It uses an eeprom chip 24Cxx to read & write data. The `SDA_PIN` & `SCL_PIN` (i2c pins) are defined in `thing/thing.h`

#### Example specific configurations
This example is configured ..

<img src="res/i2c-example-config.png" width="500">

This example does the following:

- Initializes an i2c master client using `init_i2c_core_master()` function
- Writes some random data into the eeprom
- Read the same memory location
- Prints the read values