# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

<img src="res/fritzing/threading.png" width="500">

[//]: ![image](../fritzing/threading.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to use the `threading` module APIs.

#### Example specific configurations
The 'Threading' configuration is internally enabled. So, for this example, no other configuration is needed.


This examples does the following things:

- Creates a thread (task) called `task_1()` using `TaskCreate()` function
- Loops the thread (task) for 100 times
- Destroys the thread (task) by calling `TaskDelete()` 