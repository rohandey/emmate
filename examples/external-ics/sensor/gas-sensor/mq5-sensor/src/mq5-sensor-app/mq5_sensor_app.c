/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "mq5_sensor_app.h"
#include "mq5.h"

#define TAG	"mq5-sens-app"

#define MQ5_PIN THING_MK1_AN_PIN

void mq5_sensor_app_init() {
	CORE_LOGI(TAG, "In your_module_init");

	CORE_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	CORE_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* Do all necessary initializations here */
	mq5_init_sensor(MQ5_PIN, 2000, 3.3, 0);
	mq5_start_process();
}

void mq5_sensor_app_loop() {
	CORE_LOGI(TAG, "In your_module_loop");

	float val = 0;
	mq5_get_deteced_gas_level(&val);

	CORE_LOGI(TAG, "Gas Level = %f", val);
}
