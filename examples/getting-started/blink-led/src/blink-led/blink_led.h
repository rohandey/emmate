/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   blink_led.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Blink of a LED.
 *
 *
 *
 */

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

/**
 * @brief	Init function for blink_led module
 *
 */
void blink_led_init();

/**
 * @brief	Execution function for blink_led module
 *
 */
void blink_led_loop();

