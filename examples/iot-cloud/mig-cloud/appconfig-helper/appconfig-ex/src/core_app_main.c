
#include "appconfig_ex.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	CORE_LOGI(TAG, "Calling your_module_init() in your_module.c in your-module directory ...");
	appconfig_ex_init();
	CORE_LOGI(TAG, "Returned from your_module_init()");

	while(1){
		appconfig_ex_loop();
		TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);
	}
}
