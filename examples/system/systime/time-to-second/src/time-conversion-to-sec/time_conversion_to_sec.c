/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   time_conversion_to_sec.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * Time conversion to second
 *
 *
 *
 */

#include "time_conversion_to_sec.h"
#define TAG "time_conversion_to_sec"

void time_to_sec_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);
#if 0
	// Hard-coded Date/Time in milliseconds
	uint64_t milisec_now = CURRENT_TIME_MILLIS;

	// Create structure timeval variable
	struct timeval now_tv;

	// set UTC time in seconds
	now_tv.tv_sec = milisec_now / 1000;

	// set UTC time's miliseconds
	now_tv.tv_usec = (milisec_now % 1000) * 1000;

	/* Set the system time */
	if (core_settimeofday(&now_tv, NULL) != 0) {
		CORE_LOGE(TAG, "Failed to set System time...");
	} else {
		CORE_LOGI(TAG, "Successfully set the System time...");
	}
#endif
}

void time_to_sec_loop() {
	struct tm now;    // create a struct for reading system time

	now.tm_sec = 50; /* Seconds.	[0-60] (1 leap second) 	*/
	now.tm_min = 8; /* Minutes.	[0-59] 					*/
	now.tm_hour = 8; /* Hours.	[0-23] 					*/
	now.tm_mday = 11; /* Day.		[1-31] 					*/
	now.tm_mon = 6; /* Month.	[0-11] 					*/
	now.tm_year = 2019; /* Year		[]						*/

	char strftime_buf[64];

	// make time string from current-time structure data, in human readable format.
	strftime(strftime_buf, sizeof(strftime_buf), "%c", &now);

	// print the Set Date/Time
	CORE_LOGI(TAG, "Set date/time is: %s", strftime_buf);

	time_t time = convert_tm_to_seconds(&now); //read time from convert_tm_to_seconds()

	CORE_LOGI(TAG, "Converted Set Date/Time in Seconds = %ld\n", time);  // print converted seconds
}
