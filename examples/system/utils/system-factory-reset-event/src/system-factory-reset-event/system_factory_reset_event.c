/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "system_factory_reset_event.h"
#include "system_utils.h"

#define TAG	"system_factory_reset_event"

/**
 *	declear application's factory reset event callback function
 */
void applications_factory_reset_event_cb(){
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "######################## Application's Factory Reset Event Callback ####\n");

	CORE_LOGI(TAG, "Hello!! User, system's factory reset event Occure");
	CORE_LOGI(TAG, "Complete your unfinished process and leave from this callback function");
	CORE_LOGI(TAG, "Good Bye!!! User, leaving from the application's factory reset event callback functions\n");

	CORE_LOGI(TAG, "######################## Application's Factory Reset Event Callback ####");
	CORE_LOGI(TAG,"");

}

void system_factory_reset_event_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	CORE_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* Do all necessary initializations here */
	// Registern application's factory-reset event's drive by system when factory-reset event occre
	register_factory_reset_event_function(applications_factory_reset_event_cb);

	CORE_LOGI(TAG, "Returning from %s", __func__);
}

void system_factory_reset_event_loop() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "Now press and Hold the reset button to execute the system reset event.....\n");
}
