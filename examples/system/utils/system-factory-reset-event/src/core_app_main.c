
#include "system_factory_reset_event.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	CORE_LOGI(TAG, "Calling system_factory_reset_event_init() in system_factory_reset_event.c in system-factory-reset-event directory ...");
	system_factory_reset_event_init();
	CORE_LOGI(TAG, "Returned from system_factory_reset_event_init()");

	while(1){
		CORE_LOGD(TAG, "Calling system_factory_reset_event_loop() in system_factory_reset_event.c in system-factory-reset-event directory ...");
		system_factory_reset_event_loop();
		TaskDelay(DELAY_20_SEC / TICK_RATE_TO_MS);
	}
}
