add_library(${CMAKE_PROJECT_NAME}_inc INTERFACE)

list(APPEND EXTRA_LIBS 
	som
	emmate_config)

target_link_libraries(${CMAKE_PROJECT_NAME}_inc INTERFACE ${EXTRA_LIBS})

target_include_directories(${CMAKE_PROJECT_NAME}_inc 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)
