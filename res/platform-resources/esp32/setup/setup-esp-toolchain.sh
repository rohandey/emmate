#!/bin/bash

######## Setup ESP Directory	

export CURR_DIR=`pwd`

#create ESP directory in $HOME(~)
if [ ! -z "$1" ]
then
	ESP_DIR_PATH=$1	
else
	ESP_DIR_PATH=$CURR_DIR
fi

#mkdir -p $ESP_DIR_PATH

#change directory to $ESP_DIR_PATH
cd $ESP_DIR_PATH

# get the OS type
OS_TYPE=$(python $EMMATE_RELEASE_PATH/platform-resources/getOSPlatform.py)

#change directory to $ESP_DIR_PATH
cd $ESP_DIR_PATH

if [[ $OS_TYPE == "Linux" ]]
then

	######## Setup ESP-Toolchain
	
	## for 64-bit Linux:
	TOOLCHAIN_URL_x64="https://dl.espressif.com/dl/xtensa-esp32-elf-gcc8_2_0-esp32-2019r1-linux-amd64.tar.gz"
	DOWNLOADED_x64TOOLCHAIN_FILE_NAME="xtensa-esp32-elf-gcc8_2_0-esp32-2019r1-linux-amd64.tar.gz"
	
	## for 32-bit Linux:
	TOOLCHAIN_URL_x32="https://dl.espressif.com/dl/xtensa-esp32-elf-gcc8_2_0-esp32-2019r1-linux-i686.tar.gz"
	DOWNLOADED_x32TOOLCHAIN_FILE_NAME="xtensa-esp32-elf-gcc8_2_0-esp32-2019r1-linux-i686.tar.gz"
	
	
	#TODO: check OS's running on 64-Bit or 32-bit
	
	# Check by cmd: 'uname -m'
	# if(res = x86_64 or amd64 or x64) then OS is 64-bit
	# if(res = x86 or i686 or i386) then OS is 64-bit
	
	
	# default toolchain 64-bit
	TOOLCHAIN_URL=$TOOLCHAIN_URL_x64
	DOWNLOADED_TOOLCHAIN_FILE_NAME=$DOWNLOADED_x64TOOLCHAIN_FILE_NAME
	
	echo "### Starting ESP-Toolchain Downaload #########################"
	wget -c $TOOLCHAIN_URL	# if download failed then it will resume the download where it left off.
		
	LAST_CMD_RETURN_STAT=$?
	
	if [[ $LAST_CMD_RETURN_STAT -eq 0 ]]
	then
		# extract toolchain from tar.gz
		tar -xzf $DOWNLOADED_TOOLCHAIN_FILE_NAME
	else
		echo "### Failed to Download ESP-Toolchain #########################"
		echo
		exit $EXECUTION_STAT
	fi

	echo "### Completed ESP-Toolchain Download #########################"
	echo
	echo


	######## Setup ESP-Toolchain environment Variable
	echo "### Setup Toolchain environment Variable #########################"
	echo "Please follow the below instructions to set the ESP-Toolchain environment variables"
	echo
	echo "Step 1: To make xtensa-esp32-elf available for all terminal sessions, add the following line to your ~/.profile file:"
	echo
	echo -e "	\e[38;5;0;104;1m export PATH=\"$ESP_DIR_PATH/xtensa-esp32-elf/bin:"'$PATH'"\" \e[0m"
	echo
	#echo "Step 2: Alternatively, you may create an alias for the above command. This way you can get the toolchain only when you need it. To do this, add different line to your ~/.profile file:"
	#echo
	#echo -e "	\e[38;5;0;104;1m alias get_esp32='export PATH=\"$ESP_DIR_PATH/xtensa-esp32-elf/bin:"'$PATH'"\"""' \e[0m"
	#echo
	echo "Step 3: Log off and log in back to make this change effective. Run the following command to check if PATH is set:"
	echo
	echo -e "	\e[38;5;0;104;1m printenv PATH \e[0m"
	echo

elif [[ $OS_TYPE == "Windows" ]]
then 
	## for Windows:
	TOOLCHAIN_SETUP_FILE_URL_WINDOWS="https://dl.espressif.com/dl/esp-idf-tools-setup-2.2.exe"
	DOWNLOADED_WINDOWS_TOOLCHAIN_SETUP_FILE_NAME="esp-idf-tools-setup.exe"

	# default toolchain 	
	TOOLCHAIN_SETUP_FILE_URL=$TOOLCHAIN_SETUP_FILE_URL_WINDOWS
	DOWNLOADED_TOOLCHAIN_SETUP_FILE_NAME=$DOWNLOADED_WINDOWS_TOOLCHAIN_SETUP_FILE_NAME

	echo "### Starting ESP-Toolchain Downaload #########################"
	curl $TOOLCHAIN_SETUP_FILE_URL	-o $DOWNLOADED_TOOLCHAIN_SETUP_FILE_NAME 
	
	LAST_CMD_RETURN_STAT=$?
	#LAST_CMD_RETURN_STAT=0

	if [[ $LAST_CMD_RETURN_STAT -eq 0 ]]
	then
		# extract toolchain setup file
		./$DOWNLOADED_TOOLCHAIN_SETUP_FILE_NAME
	else
		echo "### Failed to Download ESP-Toolchain Setup File #########################"
		echo
		exit $EXECUTION_STAT
	fi
	
else

	echo "### Failed to Download ESP-Toolchain Setup File : Unkonwn Operating System #########################"
	
fi



