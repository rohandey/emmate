# ESP32 Environment Setup

For doing the following environment setup you would need a computer running **Windows 10/7**.


### Install Prerequisites
To compile with ESP-IDF you need to get the following packages. Please run the below command in a terminal.

* Install **Git Bash**
* Install **Python-3** with `Customize installation` having `Python Launcher for Windows`, from [Python for Windows](https://docs.python.org/3/using/windows.html#)

## ESP-IDF (SDK) Setup
To setup ESP32, you also need **ESP-IDF (SDK)**. Please follow the below steps to download and setup the SDK. Run the following commands in a **Windows 10/7 Command Prompt**:

```
> cd D:
> md D:\esp
> cd D:\esp
```

#### 1. Download and Setup ESP-IDF (SDK)
First you need to copy the SDK setup script from `(release-package-path)\platform-resources\setup\setup-esp-sdk.bat` to `D:\esp`. To do so run the following commands:

```
> cd D:\esp
> xcopy /i /d /y /f /s -v -u -r (release-package-path)\platform-resources\setup\setup-esp-sdk.bat D:\esp
> setup-esp-sdk.bat
```

ESP-IDF SDK will be downloaded into `D:\esp\esp-idf`. This will take several minutes to complete, depending upon your internet connection.

#### 2. Set IDF_PATH Environment Variable
Set up `IDF_PATH` adding into **Windows 10/7** system **Environment Variable** . To do so following this steps:

* Pressing the `Windows` and `R` key on your keyboard at the same time
* Type `sysdm.cpl` into the input field and hit `Enter` or press _Ok_.
* In the new window that opens, click on the _Advanced_ tab and afterwards on the _Environment Variables_ button in the bottom right of the window.

<img src="sys_properties.png" width="500">

* Create a user specific variable by clicking the _New_ button below the user-specific section.

<img src="env_var.png" width="400">

* In the prompt Create a new variable with the name `IDF_PATH` and copy your `D:\esp\esp-idf`. Press _Ok_ in the prompt to create the variable, followed by _Ok_ on the Environment Variables window. You are all set now.

<img src="env_set.png" width="400"> 

* Run the following command to check if **`IDF_PATH`** is set:

```
> echo %IDF_PATH%
```

## Toolchain Setup

Please follow the below steps to setup the Toolchain. Run the following commands in a **Windows 10/7 Command Prompt**:

```
> cd D:\esp
> md D:\esp\tools
> cd D:\esp\tools
```

#### 1. Setup IDF_TOOLS_PATH Enviroment variable
Set up `IDF_TOOLS_PATH` adding into **Windows 10/7** system **Environment Variable** . To do so following this steps:

* Pressing the `Windows` and `R` key on your keyboard at the same time
* Type `sysdm.cpl` into the input field and hit `Enter` or press _Ok_.
* In the new window that opens, click on the _Advanced_ tab and afterwards on the _Environment Variables_ button in the bottom right of the window.

<img src="sys_properties.png" width="500">

* Create a user specific variable by clicking the _New_ button below the user-specific section.

<img src="env_var.png" width="400">

* In the prompt Create a new variable with the name `IDF_TOOLS_PATH` and copy your `D:\esp\tools`. Press _Ok_ in the prompt to create the variable, followed by _Ok_ on the Environment Variables window. You are all set now.

<img src="idf_tools_path.png" width="400"> 

* Run the following command to check if **`IDF_PATH`** is set:

```
> echo %IDF_TOOLS_PATH%
```

#### 2. Download and Setup ESP-IDF (SDK)
First you need to copy the Toolchain setup script from `(release-package-path)\platform-resources\setup\setup-esp-toolchain.bat` to `D:\esp\tools`. To do so run the following commands:

```
> cd D:\esp\tools
> xcopy /i /d /y /f /s -v -u -r (release-package-path)\platform-resources\setup\setup-esp-toolchain.bat D:\esp\tools
> setup-esp-toolchain.bat
```



