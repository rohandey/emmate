@ECHO OFF

:: setting current directory
set CURR_DIR=%cd%
::ECHO %CURR_DIR%

:: importing Configuration File
set CONFIG_PATH=%CURR_DIR%\setup_conf.bat
ECHO %CONFIG_PATH%

::for /f "delims=" %%x in (%CONFIG_PATH%) do (set "%%x")
::ECHO {%RELEASE_PKG_DIR%}
::PAUSE

:: Source or Import the Config-file
call %CONFIG_PATH%
::ECHO %RELEASE_PKG_DIR%

set CURRENT_DATE=%date%
set CURRENT_TIME=%time%

::ECHO %CURRENT_DATE%
::ECHO %CURRENT_TIME%


ECHO .....................................................................
ECHO Setting up your project
ECHO .....................................................................
ECHO Core distribution Location : %RELEASE_PKG_DIR%
ECHO .....................................................................


IF "%RELEASE_PKG_DIR%"=="" (
ECHO "Environment variable 'EMMATE_RELEASE_PATH' is not set"
ECHO "You need to update your 'EMMATE_RELEASE_PATH' environment variable."
PAUSE
exit 1
)

cd ..
set PROJ_DIR=%cd%
::ECHO %PROJ_DIR%

ECHO Project Location : %PROJ_DIR%
ECHO .....................................................................


:: creating required directories
mkdir %PROJ_DIR%\platform\core
:: ############## HAVE TO GET ESP FROM SOMEWHERE ##############
mkdir %PROJ_DIR%\platform\esp


::copying a temporary sample src
cd %RELEASE_PKG_DIR%\setup\files\emmate-sample-proj


ECHO .....................................................................
ECHO	Copy Build Directory
ECHO .....................................................................
:: Always copy the build folder from RELEASE_PKG_DIR/setup/files
::mkdir %PROJ_DIR%\build
xcopy /S /I /D /E /H /Y /F  %cd%\build\* %PROJ_DIR%\build


ECHO .....................................................................
ECHO	Copy Src Directory
ECHO .....................................................................
:: Copy the src folder from RELEASE_PKG_DIR/setup/files
:: Only if src is NOT PRESENT in PROJ_DIR
IF NOT EXIST "%PROJ_DIR%\src" (
	ECHO "Setting up new project. So copying the src to %PROJ_DIR%"
	mkdir %PROJ_DIR%\src
	xcopy /S /I /D /E /H /Y /F %cd%\src %PROJ_DIR%\src
	
	echo "Copying the Eclipse files to %PROJ_DIR%"
	xcopy /S /I /D /E /H /Y /F %cd%\.externalToolBuilders\* %PROJ_DIR%\.externalToolBuilders
	xcopy /S /I /D /E /H /Y /F %cd%\.launches\* %PROJ_DIR%\.launches
	xcopy /S /I /D /E /H /Y /F %cd%\.settings\* %PROJ_DIR%\.settings
	xcopy %cd%\.cproject %PROJ_DIR%
	xcopy %cd%\.project %PROJ_DIR%
	
) ELSE (
	:: Check if the src has the version dir and conf file (required for building the examples)
	:: Copy the version dir and conf file if not present
	IF NOT EXIST "%PROJ_DIR%\src\version" (
		ECHO "Copying the version conf file to $PROJ_DIR/src/version"
		mkdir %PROJ_DIR%\src\version
		xcopy /S /D /I /E /H /Y /F %cd%\src\version\app_version.conf %PROJ_DIR%\src\version
		
		echo "Copying the Eclipse files to %PROJ_DIR%"
		xcopy /S /D /I /E /H /Y /F %cd%\.externalToolBuilders\* %PROJ_DIR%\.externalToolBuilders
		xcopy /S /D /I /E /H /Y /F %cd%\.launches\* %PROJ_DIR%\.launches
		xcopy /S /D /I /E /H /Y /F %cd%\.settings\* %PROJ_DIR%\.settings
		xcopy %cd%\.cproject %PROJ_DIR%
		xcopy %cd%\.project %PROJ_DIR%
		
	)
	ECHO "Source directory already present. Not touching the project's source files"
)


ECHO .....................................................................
ECHO	Copy EmMate-Core Directory
ECHO .....................................................................
:: copying the core src
cd %RELEASE_PKG_DIR%\src

xcopy /S /D /I /E /H /Y /F . %PROJ_DIR%\platform\core

ECHO .....................................................................
ECHO	Copy ESP-Core Directory
ECHO .....................................................................
:: copying the platform src
cd %RELEASE_PKG_DIR%\platform

xcopy /S /D /I /E /H /Y /F . %PROJ_DIR%\platform\esp


cd %CURR_DIR%

PAUSE