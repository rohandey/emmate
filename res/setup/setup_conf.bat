:: #####################################################################
:: # Specify the directory location of the extracted core zip against
:: # this property RELEASE_PKG_DIR
:: #   For example,
:: #      If the distribution zip is extracted at /home/iquesters/emmate
:: #      Then the RELEASE_PKG_DIR="/home/iquesters/emmate"
:: #####################################################################
:: #RELEASE_PKG_DIR="<enter the path where the emmate release package is extracted>"
SET RELEASE_PKG_DIR=%EMMATE_RELEASE_PATH%