/*
 * This file contains Pin Configurations for Team Thing board v2.2
 * More Details about this board can be found at: https://mig.iquesters.com/?s=somthing&p=resources
 */

#ifndef THING_H_
#define THING_H_

#include "som.h"

#define YOUR_THING_NAME	"My Cool Thing"

#define LEDS_NUMBER		3  /*!< Number of leds used */

/*
 * The following LEDs are used by the EmMate Framework for showing notifications during startup.
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will and can be used in any other purpose.
 * Before changing these SOM pin numbers, please note that 'HAVE_SYSTEM_HMI' has to be disabled in core_config.h
 * You can change the 'HAVE_SYSTEM_HMI' configuration via the EmMate Configuration tool.
 * Run './build.sh -menuconfig' to start the tool
 * */

/* The below LED configurations are for SoM ESP32-WROOM-32D
 * More Details about this board can be found at: https://mig.iquesters.com/?s=somthing&p=resources
 * */
#define SYSTEM_HMI_LED_MONO_RED		SOM_PIN_100   /*!< Used by the EmMate Framework as RED LED */
#define SYSTEM_HMI_LED_GREEN		SOM_PIN_102   /*!< Used by the EmMate Framework as GREEN LED */
#define SYSTEM_HMI_LED_BLUE			SOM_PIN_104   /*!< Used by the EmMate Framework as BLUE LED */

/**/
#define LEDS_ACTIVE_STATE 	1
/**/
#define LEDS_LIST { SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE }

#define BUTTONS_NUMBER	1

/*
 * The following BUTTON is used by the EmMate Framework as a Factory Reset Button for deleting all saved configurations.
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will and can be used in any other purpose.
 * Before changing the SOM pin numbers, please note that 'HAVE_SYSTEM_HMI' has to be disabled in core_config.h
 * You can change the 'HAVE_SYSTEM_HMI' configuration via the EmMate Configuration tool.
 * Run './build.sh -menuconfig' to start the tool
 */
#define SYSTEM_RESET_BUTTON		SOM_PIN_98

/**/
#define BUTTONS_ACTIVE_STATE	0
/**/
#define BUTTONS_LIST		{ SYSTEM_RESET_BUTTON }

/******************************************* SDMMC GPIO Selection **************************************************/
/* If using SDMMC module with SPI, the following defines must be made else compilation error will occur */
#define SDMMC_CLK 	SOM_PIN_47
#define SDMMC_MISO 	SOM_PIN_49
#define SDMMC_MOSI 	SOM_PIN_51
#define SDMMC_CS 	SOM_PIN_53

#endif /* THING_H_ */
