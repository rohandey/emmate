#####################################################################
# Copyright (c) 2012-2016, iquesters - All Rights Reserved.
#####################################################################

#--------------------------------------------------------------------
# DISTRIBUTION CONTENTS
#--------------------------------------------------------------------
This distribution directory must contain the following directories
and files:

1. conf				<DIR>
2. lib				<DIR>
3. sql				<DIR>
4. war				<DIR>
5. LICENSE			<FILE>
6. readme.txt			<FILE>


1. conf				<DIR>
The conf directory will contain "<filename>.properties" file(s) in a
particular directory structure. These are configuration files which
are needed by the core to function properly. Detailed description of
each config file is provided in the conf file itself.


2. lib				<DIR>
The lib directory will contain the supporting libraries required by
the core-<version>.jar to function properly. These jars needs to be
included in the classpath of artifacts developed using the iquesters
core framework. These are mostly to be placed in the lib directory of
the tomcat server used.


3. sql				<DIR>
The sql directory will contain a set of directories named
"<version-number>" and each directory will contain "<filename>.sql"
file(s) if any database change needs to be applied for that
particular release version. There will also be a readme.txt with
usage instructions.


4. war				<DIR>
The war directory will contain core-<version>.war file that will be
used to build the projects that will be developed using the iquesters
core framework.

5. LICENSE			<FILE>
The LICENSE file will contain license related information


6. readme.txt			<FILE>
The readme.txt file is the file you are reading now.


#--------------------------------------------------------------------
# RUNNING THE APPLICATION
#--------------------------------------------------------------------
To run an application based on the iquesters core framework, the
following steps should be followed to setup the environment:

# Setting up the configuration environment
#--------------------------------------------------------------------
1. Setup a environment variable "CORE_CONF_LOCATION"
   For example,
      CORE_CONF_LOCATION=D:/iquesters/core-conf

2. Create a directory with the name of the war you intend to deploy
   in the CORE_CONF_LOCATION.
   For example,
      If, CORE_CONF_LOCATION=D:/iquesters/core-conf
      Create a directory "mywarfile" in D:/iquesters/core-conf if
      your war file is "mywarfile.war"
   After this step you will have a new directory as,
      D:/iquesters/core-conf/mywarfile

3. Copy all files from the distribution conf directory to the above
   directory you just created.
   After this step you will have all contents of distribution conf
   directory copied under D:/iquesters/core-conf/mywarfile


# Setting up the runtime server environment
#--------------------------------------------------------------------
1. Copy the jar files present in the distribution lib directory to
   the lib of the tomcat server under a new directory "core"
   After this step you will have a new directory as,
      ${catalina.home}/lib/core

2. Update the "shared.loader" property in the catalina.properties
   located at ${catalina.home}/conf as below:
      shared.loader="${catalina.base}/lib/core","${catalina.base}/lib/core/*.jar","${catalina.home}/lib/core","${catalina.home}/lib/core/*.jar"

NOTE: You will have to restart tomcat after this.


# Setting up the runtime server environment
#--------------------------------------------------------------------
1. Copy the war file present in the distribution war directory to
   the webapp directory of the tomcat server used.