@ECHO OFF

rem  get the OS type
echo EMMATE_RELEASE_PATH = %EMMATE_RELEASE_PATH%
rem set OS_TYPE=< python %EMMATE_RELEASE_PATH%\platform-resources\getOSPlatform.py

python %EMMATE_RELEASE_PATH%\platform-resources\getOSPlatform.py > tmpFile
set /p OS_TYPE= < tmpFile 
del tmpFile

ECHO OS_TYPE = %OS_TYPE%

rem  setting current directory
set CURR_DIR=%cd%
set PWD=%cd%
rem ECHO %CURR_DIR%

rem  importing Configuration File
set CONFIG_PATH=%CURR_DIR%\build.conf
rem ECHO %CONFIG_PATH%

rem  Source or Import the Config-file
rem call %CONFIG_PATH%

rem eol stops comments from being parsed
rem otherwise split lines at the = char into two tokens
for /F "eol=# delims== tokens=1,*" %%a in (%CONFIG_PATH%) do (
    rem proper lines have both a and b set
    rem if NOT "%%a"=="" if NOT "%%b"=="" set %%a=%%b
    if NOT "%%a"=="" set %%a=%%b
)
rem ECHO %BUILD_MODE%
rem pause

set CURRENT_DATE=%date%
set CURRENT_TIME=%time%


rem importing Build CMD Option Helper
set BUILD_OPTION_HELPER_PATH=%CURR_DIR%\build-support\build-opt-helper.bat
rem ECHO %BUILD_OPTION_HELPER_PATH%
rem call %BUILD_OPTION_HELPER_PATH%


rem importing MenuConfiguration File
set MENUCONFIG_EXECUTION_SCRIPT=%CURR_DIR%\build-support\execute-menuconfig.bat
set MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH=%CURR_DIR%\menuconfig-gen-execution-status
rem source $MENUCONFIG_EXECUTION_SCRIPT


set BUILD_HELP_SCRIPT=%CURR_DIR%\build-support\build-helps.sh

rem #Collect Project Path & Project Name
rem #go to Project Directory
cd ..
set PROJECT_DIR=%cd%
for /f "delims=" %%A in ('cd') do (
     set foldername=%%~nxA
)
set PROJECT_NAME=%foldername%
set APP_SRC_PATH=%PROJECT_DIR%\src
set BIN_DIRECTORY=%PROJECT_DIR%\bin
set CMAKE_BIN_DIR=%PROJECT_DIR%\cmake_build

rem pause

rem #return to Current Directory
cd %CURR_DIR%


rem ## Script Logics ###############################################################################
ECHO .....................................................................
ECHO Initiating build for %PROJECT_NAME%
ECHO .....................................................................
ECHO.

rem pause

echo %BUILD_MODE%
rem # if BUILD_MODEis not set then set it to gui-config
rem if "%BUILD_MODE%"==""(
rem 	set BUILD_MODE="gui-config"
rem )else(
rem 	echo "all set"
rem )
rem pause

echo.
echo .....................................................................
echo Build Mode             : %BUILD_MODE%
echo Build Date             : %CURRENT_DATE%
echo Build Time             : %CURRENT_TIME%
echo Build By               : %USERNAME%
echo .....................................................................
echo Project Location : %PROJECT_DIR%
echo .....................................................................
echo Distribution Location : %BIN_DIRECTORY%
echo .....................................................................
echo.

echo %cd%


rem # Getting platform info
set PLATFORM_CONFIG_PATH=%CURR_DIR%\build-support\platform.conf
rem eol stops comments from being parsed
rem otherwise split lines at the = char into two tokens
for /F "eol=# delims== tokens=1,*" %%a in (%PLATFORM_CONFIG_PATH%) do (
    rem proper lines have both a and b set
    rem if NOT "%%a"=="" if NOT "%%b"=="" set %%a=%%b
    if NOT "%%a"=="" set %%a=%%b
)

rem #export 'PLATFORM_TARGET' Environment variable for Menuconfig 
set PLATFORM_TARGET=%PLATFORM%
echo PLATFORM_TARGET = %PLATFORM_TARGET%


rem #setting Build Mode
set PROJ_BUILD_MODE=%BUILD_MODE%
echo %PROJ_BUILD_MODE%

@setlocal EnableDelayedExpansion
rem # read menuconfig execution status
if exist %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH% (
	echo %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH%
	rem eol stops comments from being parsed
	rem otherwise split lines at the = char into two tokens
	for /F "eol=# tokens=1,* delims==" %%a in (%MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH%) do (
	    rem proper lines have both a and b set
	    rem if NOT "%%a"=="" if NOT "%%b"=="" set %%a=%%b
	    if NOT "%%a"=="" set %%a=%%b
	    
	 	if "%%a" == "MENUCONFIG_EXECUTION_STATUS" (
			rem echo inside MENUCONFIG_EXECUTION_STATUS=%%b..
			if "%%b" == "y " (
		 		rem #echo "If menuconfig execute then Deleting BIN_DIRECTORY/.PROJECT_NAME if present" >> $LOG_FILE
		 		rem # delete hidden .PROJECT_NAME/* files
		 		rd /s /q %BIN_DIRECTORY%
		 		rem rd /s /q %CMAKE_BIN_DIR%
		 		del /f /q %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH%
	 		)
		)	
	)
)




set LOG_DIR=%BIN_DIRECTORY%\log
set LOG_FILE=%LOG_DIR%\build.log

md %LOG_DIR%

echo Setting up the build...
call %CURR_DIR%\build-support\build_setup.bat > %LOG_FILE%
echo.
echo.

rem #Creating BIN_DIRECTORY
mkdir %BIN_DIRECTORY%

IF "%1" ==  "-menuconfig"  (
	echo. 
	echo ## Execute EmMate Menuconfig ################################
	call %MENUCONFIG_EXECUTION_SCRIPT% -menuconfig >> %LOG_FILE%
	echo %errorlevel%
	echo.
	echo ## Execute EmMate Menuconfig Successful ################################
	exit /b 0
) 

if "%1"=="-make" (

	echo ## Execute EmMate Menuconfig ################################
	call %MENUCONFIG_EXECUTION_SCRIPT% -menuconfig >> %LOG_FILE%
	echo %errorlevel%
	echo.
	echo ## Execute EmMate Menuconfig Successful ################################

	echo ## Execute EmMate Project Build ################################
	rem #Creating Application Version
	echo Updating the version ...
	call %CURR_DIR%\build-support\version.bat >> %LOG_FILE%
	
	echo.
	echo Execute ESP32 Platform Build Process
	call %CURR_DIR%\build-support\%PLATFORM%\platform-process.bat  >> %LOG_FILE%
	
	call %IDF_PATH%\export.bat
	
	rem cd !PROJECT_DIR!\bin\.!PROJECT_NAME!
	rem idf.py build

	rem --------------------------------------------------------------------------------------------------
	rem ----------------------------------- BUILDING WITH CMAKE ------------------------------------
	echo "Starting CMAKE Build"
	
	
	if not exist %CMAKE_BIN_DIR%\NUL mkdir %CMAKE_BIN_DIR%
	cd %CMAKE_BIN_DIR%
	
	rem if exist %CMAKE_BIN_DIR%\NUL echo "Folder already exists"
	rem if not exist %CMAKE_BIN_DIR%\NUL echo "Folder does not exist"
	
	call cmake .. -DCMAKE_TOOLCHAIN_FILE=%IDF_PATH%\tools\cmake\toolchain-esp32.cmake -GNinja
	call cmake --build .
	
	echo errorlevel: %errorlevel%
	
	if "%errorlevel%" NEQ "0" ( 
		echo ## Execute EmMate Project Build Failed ################################ 
	) else ( 
		echo ## Execute EmMate Project Build Successful ################################ 
	)
	exit /b 0
)

if "%1"=="-flash" (
	call %IDF_PATH%\export.bat
	
	rem cd !PROJECT_DIR!\bin\.!PROJECT_NAME!
	rem idf.py flash
	
	cd %CMAKE_BIN_DIR%
	python %IDF_PATH%\components\esptool_py\esptool\esptool.py -p COM3 -b 921600 write_flash @flash_project_args
	exit /b 0
)

if "%1"=="-log" (
	call %IDF_PATH%\export.bat
	
	rem cd !PROJECT_DIR!\bin\.!PROJECT_NAME!
	rem idf.py monitor
	
	cd %CMAKE_BIN_DIR%
	rem set PROJECT_ELF=%PROJECT_NAME%.elf
	rem echo PROJECT_NAME = %PROJECT_NAME%
	rem echo PROJECT_ELF = %PROJECT_ELF%
	python %IDF_PATH%\tools\idf_monitor.py -p COM3 %PROJECT_NAME%.elf
	
	exit /b 0
)



if "%1"=="-clean" (
	rd /s /q %BIN_DIRECTORY% 
rem ------------- CLEAN CMAKE BIN DIRECTORY -------------
	rd /s /q %CMAKE_BIN_DIR%
rem -----------------------------------------------------
	del /f /q %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH% 

	exit /b 0
)


call %BUILD_OPTION_HELPER_PATH%


pause