@ECHO OFF

echo platform-processs

rem ## Script Functions ###############################################################################
rem make_executable() {
rem 	local script=$1
rem 	if [ ! -x $script ]
rem 	then
rem 		echo "The script file is not executable...making the script executable"
rem 		#make is executable
rem 		chmod a+x $script
rem 	fi
rem }

echo.
echo Selected Platform is ESP32

set DESTINATION_PATH=%BIN_DIRECTORY%\.%PROJECT_NAME%
echo %DESTINATION_PATH%

rem #create ESP project Folder-structure 
rem echo Creating ESP Application Project structure...
rem call %CURR_DIR%\build-support\esp32\gen-espfolderstruct.bat %DESTINATION_PATH%	%PROJECT_NAME%

rem #copying all src & component file into ESP structured directory
rem echo Copying CORE and ESP Component Files into ESP Project structure...
rem call %CURR_DIR%\build-support\esp32\component_src_copy.bat %DESTINATION_PATH%	%PROJECT_DIR% platform

rem #create component.mk files
rem make_executable ./build-support/esp32/gen-componentmk.sh
rem echo "Creating ESP Component's component.mk files..."
rem ./build-support/esp32/gen-componentmk.sh $DESTINATION_PATH	$PROJECT_DIR >> $LOG_FILE

rem #create CMakeLists.txt files
rem make_executable ./build-support/esp32/gen-componentmk.sh
rem echo Creating ESP Component's CMakeLists files...
rem call %CURR_DIR%\build-support\esp32\gen-cmakelist.bat %DESTINATION_PATH%	%PROJECT_DIR%


rem #copying all src & component file into ESP structured directory
rem echo Copying Project Source Files into ESP Project structure...
rem call %CURR_DIR%\build-support\esp32\component_src_copy.bat %DESTINATION_PATH%	%PROJECT_DIR% 	src

rem #create Root CMakeLists.txt files

echo "Creating ESP Project's Root CMakeLists.txt files..."
echo %PROJECT_DIR%
call %CURR_DIR%\build-support\esp32\gen-cmakelist.bat %PROJECT_DIR%	%PROJECT_DIR%