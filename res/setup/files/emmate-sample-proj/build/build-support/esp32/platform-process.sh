#!/bin/bash

## Script Functions ###############################################################################
make_executable() {
	local script=$1
	if [ ! -x $script ]
	then
		echo "The script file is not executable...making the script executable"
		#make is executable
		chmod a+x $script
	fi
}

echo
echo "Selected Platform is ESP32"

DESTINATION_PATH=$BIN_DIRECTORY/".$PROJECT_NAME"

#create ESP project Folder-structure 
#make_executable ./build-support/esp32/gen-espfolderstruct.sh
#echo "Creating ESP Application Project structure..."
#./build-support/esp32/gen-espfolderstruct.sh	$DESTINATION_PATH	$PROJECT_NAME	>> $LOG_FILE

#copying all src & component file into ESP structured directory
#make_executable ./build-support/esp32/component_src_copy.sh
#echo "Copying ESP Component & Source Files into ESP Project structure..."
#./build-support/esp32/component_src_copy.sh $DESTINATION_PATH	$PROJECT_DIR	>> $LOG_FILE

#create component.mk files
#make_executable ./build-support/esp32/gen-componentmk.sh
#echo "Creating ESP Component's component.mk files..."
#./build-support/esp32/gen-componentmk.sh $DESTINATION_PATH	$PROJECT_DIR >> $LOG_FILE


#create Root CMakeLists.txt files
make_executable ./build-support/esp32/gen-cmakelist.sh
echo "Creating ESP Project's Root CMakeLists.txt files..."
./build-support/esp32/gen-cmakelist.sh $PROJECT_DIR	$PROJECT_DIR >> $LOG_FILE