@ECHO OFF

echo gen-cmakelists.bat



set DESTINATION_PATH=%1
set PROJECT_PATH=%2

rem set COMPONENTS_PATH=%DESTINATION_PATH%\components
rem set MAIN_PATH=%DESTINATION_PATH%\main

rem echo #### Create Project's CMakeLists.txt ###########################################

rem set ROOT_CMAKELIST_TXT=%DESTINATION_PATH%\CMakeLists.txt
rem set TEMP_ROOT_CMAKELIST_TXT=%DESTINATION_PATH%\CMakeLists.tmp


rem echo # The following lines of boilerplate have to be in your project's > %TEMP_ROOT_CMAKELIST_TXT%
rem echo # CMakeLists in this exact order for cmake to work correctly >> %TEMP_ROOT_CMAKELIST_TXT%
rem echo cmake_minimum_required(VERSION 3.5) >> %TEMP_ROOT_CMAKELIST_TXT%
rem echo. >> %TEMP_ROOT_CMAKELIST_TXT%
rem echo include($ENV{IDF_PATH}/tools/cmake/project.cmake) >> %TEMP_ROOT_CMAKELIST_TXT%
rem echo project(%PROJECT_NAME%) >> %TEMP_ROOT_CMAKELIST_TXT%
rem 
rem if not exist %ROOT_CMAKELIST_TXT% (
rem 	type %TEMP_ROOT_CMAKELIST_TXT% > %ROOT_CMAKELIST_TXT%
rem ) else (
rem 	FC %TEMP_ROOT_CMAKELIST_TXT%  %ROOT_CMAKELIST_TXT%
rem 	
rem 	if %errorlevel% NEQ 0 (
rem 		type %TEMP_ROOT_CMAKELIST_TXT% > %ROOT_CMAKELIST_TXT%
rem 	)
rem )

rem del /s /f /q %TEMP_ROOT_CMAKELIST_TXT%

rem echo ################################################################################


echo #### Create Project's CMake Config ###########################################

set EMMATE_CONFIG_FILE_PATH=%CURR_DIR%\..\emmate_config
set CMAKE_CONFIG_FILE_PATH=%DESTINATION_PATH%\emmate_config.cmake
ECHO %CMAKE_CONFIG_FILE_PATH%
set TEMP_CMAKE_CONFIG_FILE_PATH=%DESTINATION_PATH%\emmate_config_cmake.tmp

echo # CMake Configuration > %TEMP_CMAKE_CONFIG_FILE_PATH%

@setlocal EnableDelayedExpansion 
rem rem eol stops comments from being parsed
rem rem otherwise split lines at the = char into two tokens
for /F "eol=# delims== tokens=1,*" %%a in (%EMMATE_CONFIG_FILE_PATH%) do (
    rem proper lines have both a and b set
    rem if NOT "%%a"=="" if NOT "%%b"=="" set %%a=%%b
    rem if NOT "%%a"=="" set %%a=%%b
    
    rem echo %%a=%%b
    
    call set val=%%b
	set val=!val:"=!
    
    rem echo %%a=!val!
    
    
    rem set "cmake_conf=set( %%a "!val!" )"
	rem echo !cmake_conf! >> %TEMP_CMAKE_CONFIG_FILE_PATH%
	
	if not "%%b" == "" ( 
		set "cmake_conf=set( %%a "!val!" )"
		echo !cmake_conf! >> %TEMP_CMAKE_CONFIG_FILE_PATH%
	) else (
		set "cmake_conf=set( %%a "!val!" )"
		echo !cmake_conf! >> %TEMP_CMAKE_CONFIG_FILE_PATH%
	)
	   
)

rem type %TEMP_CMAKE_CONFIG_FILE_PATH%


if not exist %CMAKE_CONFIG_FILE_PATH% (
	echo "copy  %TEMP_CMAKE_CONFIG_FILE_PATH% == %CMAKE_CONFIG_FILE_PATH%"
	type %TEMP_CMAKE_CONFIG_FILE_PATH% > %CMAKE_CONFIG_FILE_PATH%
) else (
 	FC %TEMP_CMAKE_CONFIG_FILE_PATH%  %CMAKE_CONFIG_FILE_PATH%
	if !errorlevel! NEQ 0 (
		echo "copy  %TEMP_CMAKE_CONFIG_FILE_PATH% ---- %CMAKE_CONFIG_FILE_PATH%"
		type %TEMP_CMAKE_CONFIG_FILE_PATH% > %CMAKE_CONFIG_FILE_PATH%
	)
)

del /s /f /q %TEMP_CMAKE_CONFIG_FILE_PATH%

echo ################################################################################


rem echo #### Create Main Directory's CMakeLists.txt ###########################################

rem if not exist "%COMPONENTS_PATH%\%%G\CMakeLists.txt" (

rem 	for /f "delims=" %%i in ('dir %MAIN_PATH%\*.c /s /B /O:N') do (
rem 		set cfile=%%i
rem 		call set cfile=!cfile:%MAIN_PATH%\=!
rem 		call set cfile=!cfile:\=/!
rem 		rem echo !cfile!
rem 		set cfile_list=!cfile_list! "!cfile!"
rem 		rem echo !cfile_list!
rem 	)

rem 	for /f "delims=" %%i in ('dir %MAIN_PATH%\*.cpp /s /B /O:N') do (
rem 		set cppfile=%%i
rem 		call set cppfile=!cppfile:%MAIN_PATH%\=!
rem 		call set cppfile=!cppfile:\=/!
rem 		rem echo !cppfile!
rem 		set cppfile_list=!cppfile_list! "!cppfile!"
rem 		rem echo !cppfile_list!
rem 	)

rem 	echo idf_component_register(SRCS !cfile_list! !cppfile! > %MAIN_PATH%\CMakeLists.txt


rem 	for /f "delims=" %%i in ('dir %MAIN_PATH% /A:D /s /b /O:N') do (
rem 		set hfile_dir=%%i
rem 		call set hfile_dir=!hfile_dir:%MAIN_PATH%\=!
rem 		call set hfile_dir=!hfile_dir:\=/!
rem 		echo !hfile_dir!
rem 		set hfile_dir_list=!hfile_dir_list! "!hfile_dir!"
rem 		echo !hfile_dir_list!
rem 	)
rem 	set "hfile_dir_list=!hfile_dir_list!)"

rem 	echo  			INCLUDE_DIRS "." !hfile_dir_list! >>  %MAIN_PATH%\CMakeLists.txt
rem 	echo.

rem )
rem echo.

rem set "cfile_list="
rem set "cppfile_list="
rem set "hfile_dir_list="
	

rem echo ###################################################################################


rem echo #### Create Components's CMakeLists.txt ###########################################

rem for /f "delims=" %%G in ('dir %COMPONENTS_PATH%  /A:D /B /O:N') do (

	rem echo %%G
	rem echo idf_component_register(SRCS > %COMPONENTS_PATH%\%%G\CMakeLists.txt
	
rem 	if not exist "%COMPONENTS_PATH%\%%G\CMakeLists.txt" (
	
rem 		for /f "delims=" %%i in ('dir %COMPONENTS_PATH%\%%G\*.c /s /B /O:N') do (
rem 			set cfile=%%i
rem 			call set cfile=!cfile:%COMPONENTS_PATH%\%%G\=!
rem 			call set cfile=!cfile:\=/!
			rem echo !cfile!
rem 			set cfile_list=!cfile_list! "!cfile!"
rem 			echo ...!cfile_list!
rem 			echo.
rem 		)
		
rem 		for /f "delims=" %%i in ('dir %COMPONENTS_PATH%\%%G\*.cpp /s /B /O:N') do (
rem 			set cppfile=%%i
rem 			call set cppfile=!cppfile:%COMPONENTS_PATH%\%%G\=!
rem 			call set cppfile=!cppfile:\=/!
			rem echo !cppfile!
rem 			set cppfile_list=!cppfile_list! "!cppfile!"
rem 			echo ###!cppfile_list!
rem 			echo.
rem 		)

rem 		echo idf_component_register(SRCS !cfile_list! !cppfile_list! > %COMPONENTS_PATH%\%%G\CMakeLists.txt
		
		
rem 		for /f "delims=" %%i in ('dir %COMPONENTS_PATH%\%%G /A:D /s /b /O:N') do (
rem 			set hfile_dir=%%i
rem 			call set hfile_dir=!hfile_dir:%COMPONENTS_PATH%\%%G\=!
rem 			call set hfile_dir=!hfile_dir:\=/!
rem 			echo !hfile_dir!
rem 			set hfile_dir_list=!hfile_dir_list! "!hfile_dir!"
rem 			echo !hfile_dir_list!
rem 		)
rem 		set "hfile_dir_list=!hfile_dir_list!"
		
rem 		echo  			INCLUDE_DIRS "." !hfile_dir_list! >>  %COMPONENTS_PATH%\%%G\CMakeLists.txt
rem 		echo.
		
rem 		if exist "%COMPONENTS_PATH%\%%G\cmakelist_requires.list" (
rem 			echo %COMPONENTS_PATH%\%%G\cmakelist_requires.list
rem 			for /F "delims= " %%a in (%COMPONENTS_PATH%\%%G\cmakelist_requires.list) do ( 
				rem echo **%%a**
				
				rem for /F "delims= " %%i in ("%%a") do echo ..%%i..
				
rem 				if "%%a" == "core_app_main" (
					rem echo $.....%%a.....$
rem 					set "requires=!requires! %%a"
rem 				) else if "%%a" == "thing" (
					rem echo $.....%%a.....$
rem 					set "requires=!requires! %%a"
rem 				) else (
					rem if "%%a" component present in %COMPONENTS_PATH%\ then this component will add as requires in cmakelist
rem 					for /f "delims=" %%G in ('dir %COMPONENTS_PATH%  /A:D /B /O:N') do (
						rem echo ..%%G..
rem 						if "%%a" == "%%G" (
							rem echo $.##.%%a.##.$
rem 							set "requires=!requires! %%a"
rem 							set requires_dir_found=true
rem 						)
rem 					)
					
					rem if "%%a" component not present in %COMPONENTS_PATH%\ then it will search for component into %IDF_PATH%\components then 
					rem this component will add as requires in cmakelist
rem 					if not !requires_dir_found! == "true" (
rem 						for /f "delims=" %%G in ('dir %IDF_PATH%\components  /A:D /B /O:N') do (
							rem echo ..%%G..
rem 							if "%%a" == "%%G" (
rem 								echo $.##.%%a.##.$
rem 								set "requires=!requires! %%a"
rem 								set requires_dir_found=true
rem 							)
rem 						)
rem 					)

rem 				)
rem 				set "requires_dir_found="
rem 			)
			
rem 			set "requires=!requires!)"
			
rem 			if not "!requires!" == "" (
rem 				echo  			REQUIRES !requires! >>  %COMPONENTS_PATH%\%%G\CMakeLists.txt
rem 			) else (
rem 				set "requires= )"
rem 				echo  			REQUIRES !requires! >>  %COMPONENTS_PATH%\%%G\CMakeLists.txt
rem 			)
rem 			echo.
rem 		) else (
rem 			set "requires= )"
				
rem 			echo  			REQUIRES !requires! >>  %COMPONENTS_PATH%\%%G\CMakeLists.txt
rem 			echo. 
rem 		)
rem 	)
	
rem 	set "cfile_list="
rem 	set "cppfile_list="
rem 	set "hfile_dir_list="
rem 	set "requires="
	
rem )

rem echo ###################################################################################
