@ECHO OFF


rem ## Main Script ###############################################################################



set DESTINATION_PATH=%1
set PROJECT_PATH=%2

set COPY_ARG=%3

set COMPONENTS_PATH=%DESTINATION_PATH%\components
set MAIN_PATH=%DESTINATION_PATH%\main


set APP_SRC_PATH=%PROJECT_PATH%\src

set CORE_SRC_PATH=%PROJECT_PATH%\platform\core

set ESP_SRC_PATH=%PROJECT_PATH%\platform\esp


if "%COPY_ARG%" == "platform" (

echo.
echo.
echo ### Copying Core and Platform's All Source files #############################################
echo Copying Core and Platform's Component and Source Files into ESP Project structure %1.....
echo.

	echo 3rd arg "%COPY_ARG%" 


	echo .....................................................................
	rem #Copy Core Source file to Project/main...
	echo Copying Application Source files to %MAIN_PATH%...
	xcopy /i /d /y /f %CORE_SRC_PATH%\*.* %MAIN_PATH%
	echo .....................................................................
	echo.
	
	echo.
	echo.
	
	set CSV_FILE_PATH=%CURR_DIR%\build-cpy.csv
	echo CSV_FILE_PATH=!CSV_FILE_PATH!.....................
	
rem	set VAR_COUNT=1
	
	@setlocal EnableDelayedExpansion 
	if exist !CSV_FILE_PATH! (
	 	echo !CSV_FILE_PATH!  exist
	
		for /F "eol=# tokens=1,2,3,4,5 delims=, " %%a in (!CSV_FILE_PATH!) do ( 
			 rem echo .%%a
			 rem echo ..%%b
			 rem echo ...%%c
			 rem echo ....%%d..
			 rem echo .....%%e
					
			if "%%d" == "y" (
				rem echo option is enable
				set core_path_data=%%b
				rem echo !core_path_data!
				set esp_path_data=%%c
				rem echo !esp_path_data!
				rem call set dir_names=%%core_path_data:%PROJECT_DIR%\platform\core\=%%
				rem call echo !core_path_data:%PROJECT_DIR%\platform\core\=!
				call set dir_names=!core_path_data:%PROJECT_DIR%\platform\core\=!
				rem echo dir_names=!dir_names!
				
				set TARGET_PATH=%COMPONENTS_PATH%\!dir_names!\
				rem echo TARGET_PATH=!TARGET_PATH!
				
				if not exist "!TARGET_PATH!" (
					md !TARGET_PATH!
	 			) else (
	 				echo. 
	 			)
	 			
	rem 		# copy directory recursively
				if "%%e" == "-r" (
					rem echo recursive
					echo .....................................................................
					for %%g in (!core_path_data!) do echo ############# Copying  core\%%~ng to !TARGET_PATH!
					if exist "!core_path_data!" (
						rem /i /d /y all used for copy only update file
						rem /f show full path of the src-dist
						rem /s for recursive 
						xcopy /i /d /y /f /s !core_path_data!\* !TARGET_PATH! 
						rem /exclude:Kconfig
					)
					echo.
					
					for %%g in (!esp_path_data!) do echo ############# Copying  esp\%%~ng to !TARGET_PATH!
					if exist "!esp_path_data!" (
						xcopy /i /d /y /f /s !esp_path_data!\* !TARGET_PATH! 
					 	rem /exclude:Kconfig
					)
					echo .....................................................................
	
				) else (
	
					rem echo not recursive
					echo .....................................................................
					for %%g in (!core_path_data!) do echo ############# Copying  core\%%~ng to !TARGET_PATH!
					if exist "!core_path_data!" (
						rem /i /d /y all used for copy only update file
						rem /f show full path of the src-dist
						rem /s for recursive 
						xcopy /i /d /y /f !core_path_data!\* !TARGET_PATH! 
						rem /exclude:Kconfig
					)
					echo.
					
					for %%g in (!esp_path_data!) do echo ############# Copying  esp\%%~ng to !TARGET_PATH!
					if exist "!esp_path_data!" (
						xcopy /i /d /y /f !esp_path_data!\* !TARGET_PATH! 
					 	rem /exclude:Kconfig
					)
					
					echo .....................................................................
					
				)
				
			)
		)
		
	) else  (
		echo !CSV_FILE_PATH!  not exist
		exit /b 99
	)
	
	rem echo copy extra files
	echo ############# Start Copying  ESP's extra files
	rem echo %ESP_SRC_PATH%
	for /f "delims=" %%G in ('dir %ESP_SRC_PATH%  /A:D /B /O:N') do (
		
		rem echo %%G
		if not exist  %CORE_SRC_PATH%\%%G (
			 echo %CORE_SRC_PATH%\%%G not exist
			xcopy /S /I /E /H /Y /F  %ESP_SRC_PATH%\%%G\* %COMPONENTS_PATH%\%%G
		)
	
	)
	echo ############# End Copying ESP's extra files
	
	echo.
	echo.

)

echo.
echo.

if "%COPY_ARG%" == "src" (

echo 3rd arg "%COPY_ARG%"

echo.
echo.
echo ### Copying Application's Source files #############################################
echo.

rem	echo .....................................................................
	rem #Copy Application Component Source file to Project/components...
	echo Copy Application Component and Source file into ESP Project structure %1.....
	rem rd /s /q %COMPONENTS_PATH%\core_app_main
	xcopy /i /d /y /f %APP_SRC_PATH%\*.* %COMPONENTS_PATH%\core_app_main\
	del /s /f /q %COMPONENTS_PATH%\core_app_main\core_config.h 
	
	xcopy /i /d /y /f %APP_SRC_PATH%\core_config.h %COMPONENTS_PATH%\emmate_config\
	xcopy /i /d /y /f %CURR_DIR%\build-support\esp32\CMakeLists.txt %COMPONENTS_PATH%\emmate_config\
	echo.
	echo.

	for /f "delims=" %%G in ('dir %APP_SRC_PATH%  /A:D /B /O:N') do (
		rem echo %%G

		if "%%G" == "version" (
			echo ############# Copying  src\%%G to  %COMPONENTS_PATH%
			rem rd /s /q %COMPONENTS_PATH%\%%G
			xcopy /i /d /y /f %APP_SRC_PATH%\%%G\*.* %COMPONENTS_PATH%\%%G\
			
			echo. > %COMPONENTS_PATH%\%%G\version.h
			
			for /f "tokens=* delims=" %%l in ( %CORE_SRC_PATH%\%%G\version.h ) do ( 
				rem echo %%l
				echo %%l >> %COMPONENTS_PATH%\%%G\version.h
			)
			
			for /f "tokens=* delims=" %%l in ( %APP_SRC_PATH%\%%G\version.h ) do ( 
				rem echo %%l
				echo %%l >> %COMPONENTS_PATH%\%%G\version.h
			)
		) else (
			echo ############# Copying  src\%%G to  %COMPONENTS_PATH%
			rem rd /s /q %COMPONENTS_PATH%\%%G
			xcopy /i /d /y /f /s %APP_SRC_PATH%\%%G\*.* %COMPONENTS_PATH%\%%G\
			echo.
		)

	)

	echo.


	echo Find and delete Kconfig files recursively from  %COMPONENTS_PATH%

	del /s /f /q %COMPONENTS_PATH%\Kconfig 

	echo .....................................................................

)

echo #####################################################################
echo.



		
