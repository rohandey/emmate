#!/bin/bash

TEMP_FILE_EXT=.tmp

CPP_FILES=.cppfiles.tmp
C_FILES=.cfiles.tmp

HPP_FILES=.hppfiles.tmp
H_FILES=.hfiles.tmp

## Script Functions ###############################################################################
create_component_mk(){
	local curr_components_path=$1
	cd $curr_components_path
	
	#Creating components.mk into Project/components...
	echo -e "######### Creating components.mk into  $(basename $curr_components_path)..."
	cd $curr_components_path
	
	COMPONENTS_MK_PATH=$curr_components_path/component.mk
	TEMP_COMPONENTS_MK_PATH=$curr_components_path/.tmp_component.tmp
	
	#echo " " > $COMPONENTS_MK_PATH
	
	# Adding MACRO for SRCDIRS into component.mk
	echo "COMPONENT_SRCDIRS := ." > $TEMP_COMPONENTS_MK_PATH
	echo " " >> $TEMP_COMPONENTS_MK_PATH
	
	# Adding MACRO for INCLUDEDIRS into component.mk
	echo "COMPONENT_ADD_INCLUDEDIRS := ." >> $TEMP_COMPONENTS_MK_PATH
	echo " " >> $TEMP_COMPONENTS_MK_PATH
	
	#### Read .C files from ./$C_FILES
	if [ -f ./$C_FILES ]
	then
		# remove '\n' from list of files
		local c_files=$(sed ':a;N;$!ba;s/\n/ /g' ./$C_FILES)
		#echo Cfiles: $c_files
		#echo
	fi
	
	#### Read .H files from ./$H_FILES
	if [ -f ./$H_FILES ]
	then
		# remove '\n' from list of files
		local h_files=$(sed ':a;N;$!ba;s/\n/ /g' ./$H_FILES)
		#echo Hfiles: $h_files
		#echo
	fi
	
	#### Read .Cpp files from ./$CPP_FILES
	if [ -f ./$CPP_FILES ]
	then		
		# Adding MACRO CFLAGS for including the C++ compiler into component.mk
		echo "CFLAGS += -std=c++14" >> $TEMP_COMPONENTS_MK_PATH
		echo " " >> $TEMP_COMPONENTS_MK_PATH
		
		# remove '\n' from list of files
		local cpp_files=$(sed ':a;N;$!ba;s/\n/ /g' ./$CPP_FILES)
		#echo CPPfiles: $cpp_files
		#echo
		
	fi
	
	#### Read .Hpp files from ./$HPP_FILES
	if [ -f ./$HPP_FILES ]
	then
		# remove '\n' from list of files
		local hpp_files=$(sed ':a;N;$!ba;s/\n/ /g' ./$HPP_FILES)
		#echo HPPfiles: $hpp_files
		#echo
	fi
	
	local C_CPP_FILES_ARR=($c_files $cpp_files)
	#echo -e "Printing the CFILES array:"
	#echo "${C_CPP_FILES_ARR[*]}"
	
	# Get the array count
	count=${#C_CPP_FILES_ARR[@]}
	
	# Finally, extract each element of the C & CPP files path from array and store the directory name in the component.mk file 
	echo " " >> $TEMP_COMPONENTS_MK_PATH
	if [ $count -gt 0 ]
	then
		TEMP_C_CPP_COMPONENTS_MK_PATHS=$curr_components_path/.tempC.tmp
		echo Generating COMPONENT_SRCDIRS...
		echo -e "Number of C & CPP files: $count"
		echo "COMPONENT_SRCDIRS += \\" > $TEMP_C_CPP_COMPONENTS_MK_PATHS
		for ((i=0; i<$count-1; i++))
		do
			elem=${C_CPP_FILES_ARR[$i]}
			echo $elem
			echo -e "\t$(dirname "$elem") \\" >> $TEMP_C_CPP_COMPONENTS_MK_PATHS
		done
		elem=${C_CPP_FILES_ARR[$count-1]}
		echo $elem.
		echo -e "\t$(dirname "$elem") \\" >> $TEMP_C_CPP_COMPONENTS_MK_PATHS
		
		sed -i -e '/\. \\/d' $TEMP_C_CPP_COMPONENTS_MK_PATHS
		
		# remove duplicates strings
		awk '!a[$0]++' $TEMP_C_CPP_COMPONENTS_MK_PATHS >> $TEMP_COMPONENTS_MK_PATH
		
		echo Done!
		echo
	fi
	
	
	local H_HPP_FILES_ARR=($h_files $hpp_files)
	#echo -e "Printing the HFILES array:"
	#echo "${H_HPP_FILES_ARR[*]}"

	# Get the array count
	count=${#H_HPP_FILES_ARR[@]}
	
	# Finally, extract each element of the H & HPP files path from array and store the directory name in the component.mk file
	echo " " >> $TEMP_COMPONENTS_MK_PATH
	if [ $count -gt 0 ]
	then
		TEMP_H_HCPP_COMPONENTS_MK_PATHS=$curr_components_path/.tempH.tmp
		echo Generating COMPONENT_ADD_INCLUDEDIRS...
		echo -e "Number of H & HPP files: $count"
		echo "COMPONENT_ADD_INCLUDEDIRS += \\" > $TEMP_H_HCPP_COMPONENTS_MK_PATHS
		for ((i=0; i<$count-1; i++))
		do
			elem=${H_HPP_FILES_ARR[$i]}
			echo $elem
			echo -e "\t$(dirname "$elem") \\" >> $TEMP_H_HCPP_COMPONENTS_MK_PATHS
		done
		elem=${H_HPP_FILES_ARR[$count-1]}
		echo $elem.
		echo -e "\t$(dirname "$elem") \\" >> $TEMP_H_HCPP_COMPONENTS_MK_PATHS
		
		sed -i -e '/\. \\/d' $TEMP_H_HCPP_COMPONENTS_MK_PATHS
		
		# remove duplicates strings
		awk '!a[$0]++' $TEMP_H_HCPP_COMPONENTS_MK_PATHS >> $TEMP_COMPONENTS_MK_PATH
		
		echo Done!
	fi
	
	if [ -f $COMPONENTS_MK_PATH ]
	then
		components_mk_file_diff=$(diff $COMPONENTS_MK_PATH $TEMP_COMPONENTS_MK_PATH)
		#echo $components_mk_file_diff
		if [[ -z $components_mk_file_diff ]]
		then
			# do nothing
			# : used for pass from this (if/else) state
			: 
		else
			#echo execute_component.mk file_check
			cat $TEMP_COMPONENTS_MK_PATH > $COMPONENTS_MK_PATH
		fi
	else
		cat $TEMP_COMPONENTS_MK_PATH > $COMPONENTS_MK_PATH
	fi
	
	
	# remove all temp files from current directory
	rm -fv $(find . -maxdepth 1 -mindepth 1 -name '*.tmp') 
	
}

#######################################################################
#### Add .C and .CPP file paths #######################################
# First find all the .c & .cpp files and list them in cfiles.txt
find_c_file(){
	local curr_path=$1
	#echo $curr_path
	cd $curr_path
	local cfiles=($(find . -name "*.c"))
	local cfiles_count=${#cfiles[@]}
	#echo C_file Count: $cfiles_count
	if [ $cfiles_count -gt 0 ]
	then
		(find . -name "*.c") > ./$C_FILES
	fi
}

find_cpp_file(){
	local curr_path=$1
	#echo $curr_path
	cd $curr_path
	local cppfiles=($(find . -name "*.cpp"))
	local cppfiles_count=${#cppfiles[@]}
	#echo CPP_file Count: $cppfiles_count
	if [ $cppfiles_count -gt 0 ]
	then
		(find . -name "*.cpp") > ./$CPP_FILES
	fi
}


#######################################################################
#### Add .H and .HPP file paths #######################################
## First find all the .h & .hpp files and list them in hfiles.txt
find_h_file(){
	local curr_path=$1
	#echo $curr_path
	cd $curr_path
	local hfiles=($(find . -name "*.h"))
	local hfiles_count=${#hfiles[@]}
	#echo H_file Count: $hfiles_count
	if [ $hfiles_count -gt 0 ]
	then
		(find . -name "*.h") > ./$H_FILES
	fi
}

find_hpp_file(){
	local curr_path=$1
	#echo $curr_path
	cd $curr_path
	local hppfiles=($(find . -name "*.hpp"))
	local hppfiles_count=${#hppfiles[@]}
	#echo HPP_file Count: $hppfiles_count
	if [ $hppfiles_count -gt 0 ]
	then
		(find . -name "*.hpp") > ./$HPP_FILES
	fi
}



## Main Script ###############################################################################
echo "Create component.mk files for every components"

DESTINATION_PATH=$1
PROJECT_PATH=$2

COMPONENTS_PATH=$DESTINATION_PATH/components

cd $COMPONENTS_PATH
#echo $PWD

for entry in $COMPONENTS_PATH/*
do
	cd $COMPONENTS_PATH
	#echo $entry
	
	if [[ -d $entry ]] 
	then
		# This is a Directory path
		find_c_file $entry
		find_cpp_file $entry
		find_h_file $entry
		find_hpp_file $entry
		create_component_mk $entry
	elif [[ -f $entry ]]
	then
		# This is a File path
		# Do Nothing
		# : used for pass from this (if/else) state
		:
		
	else
    	echo "$entry is not valid"
    	exit 1
	fi
	echo
	echo

done


# create component.mk file for parent path project/component
CORE_APP_MAIN_DIR="$COMPONENTS_PATH/core_app_main"
cd $COMPONENTS_PATH
#echo $PWD
mkdir -v -p $CORE_APP_MAIN_DIR
if [ -f *.c ]
then
	mv -v *.c "$CORE_APP_MAIN_DIR"
fi

if [ -f *.cpp ]
then
	mv -v *.cpp "$CORE_APP_MAIN_DIR"
fi

if [ -f *.h ]
then
	mv -v *.h "$CORE_APP_MAIN_DIR"
fi

if [ -f *.hpp ]
then
	mv -v *.hpp "$CORE_APP_MAIN_DIR"
fi

#mv -v *.c *.cpp *.h *.hpp "$CORE_APP_MAIN_DIR"

find_c_file "$CORE_APP_MAIN_DIR"
find_cpp_file "$CORE_APP_MAIN_DIR"
find_h_file "$CORE_APP_MAIN_DIR"
find_hpp_file "$CORE_APP_MAIN_DIR"
create_component_mk "$CORE_APP_MAIN_DIR"
echo
echo

