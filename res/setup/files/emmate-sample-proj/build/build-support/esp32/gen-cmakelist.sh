#!/bin/bash

DESTINATION_PATH=$1
PROJECT_PATH=$2


echo #### Create Project's CMakeLists.txt ###########################################

# ROOT_CMAKELIST_TXT=$DESTINATION_PATH/CMakeLists.txt
# TEMP_ROOT_CMAKELIST_TXT=$DESTINATION_PATH/CMakeLists.tmp


# 	echo #### Create Project's CMakeLists.txt for ESP32 ###########################################
	
# 	echo "# The following lines of boilerplate have to be in your project's" > $TEMP_ROOT_CMAKELIST_TXT
# 	echo "# CMakeLists in this exact order for cmake to work correctly" >> $TEMP_ROOT_CMAKELIST_TXT
# 	echo "cmake_minimum_required(VERSION 3.5)" >> $TEMP_ROOT_CMAKELIST_TXT
# 	echo "" >> $TEMP_ROOT_CMAKELIST_TXT
# 	echo "include($""ENV{IDF_PATH}/tools/cmake/project.cmake)" >> $TEMP_ROOT_CMAKELIST_TXT
# 	echo "project($PROJECT_NAME)" >> $TEMP_ROOT_CMAKELIST_TXT
	
# 	if [ -f $ROOT_CMAKELIST_TXT ]
# 	then
# 		file_diff=$(diff $ROOT_CMAKELIST_TXT $TEMP_ROOT_CMAKELIST_TXT)
# 		if [ -z $file_diff ]
# 		then
# 			# do nothing
# 			# : used for pass from this (if/else) state
			: 
# 		else
# 			#echo execute_component.mk file_check
# 			cat $TEMP_ROOT_CMAKELIST_TXT > $ROOT_CMAKELIST_TXT
# 		fi
# 	else
# 		cat $TEMP_ROOT_CMAKELIST_TXT > $ROOT_CMAKELIST_TXT
# 	fi
	
	
# 	# remove all temp files from current directory
# 	rm -fv $TEMP_ROOT_CMAKELIST_TXT
# 	echo ################################################################################


########################################################################################################################################################################################################
########################################################################################################################################################################################################

# 	echo #### Create Project's CMakeLists.txt for Emmate ###########################################
	
# 	ROOT_CMAKELIST_TXT=$DESTINATION_PATH/CMakeLists-glob.txt
# 	TEMP_ROOT_CMAKELIST_TXT=$DESTINATION_PATH/CMakeLists-glob.tmp
	
	
# 	echo "cmake_minimum_required(VERSION 3.10)" >> $TEMP_ROOT_CMAKELIST_TXT
# 	echo "project($PROJECT_NAME C CXX ASM)" >> $TEMP_ROOT_CMAKELIST_TXT
	
# 	echo "#set_property(GLOBAL PROPERTY GLOBAL_DEPENDS_DEBUG_MODE 1)" >> $TEMP_ROOT_CMAKELIST_TXT
	
# 	echo "include(emmate_config.cmake)" >> $TEMP_ROOT_CMAKELIST_TXT
	
# 	echo "add_subdirectory(emmate)" >> $TEMP_ROOT_CMAKELIST_TXT
# 	echo "add_subdirectory(src)" >> $TEMP_ROOT_CMAKELIST_TXT
	
	
# 	if [ -f $ROOT_CMAKELIST_TXT ]
# 	then
# 		file_diff=$(diff $ROOT_CMAKELIST_TXT $TEMP_ROOT_CMAKELIST_TXT)
# 		if [ -z $file_diff ]
# 		then
			# do nothing
			# : used for pass from this (if/else) state
			: 
# 		else
			#echo execute_component.mk file_check
# 			cat $TEMP_ROOT_CMAKELIST_TXT > $ROOT_CMAKELIST_TXT
# 		fi
# 	else
# 		cat $TEMP_ROOT_CMAKELIST_TXT > $ROOT_CMAKELIST_TXT
# 	fi
	
	
	# remove all temp files from current directory
# 	rm -fv $TEMP_ROOT_CMAKELIST_TXT
# 	echo ################################################################################

echo ################################################################################


echo #### Create Project's CMake Config ###########################################

EMMATE_CONFIG_FILE_PATH=$CURR_DIR/../emmate_config
CMAKE_CONFIG_FILE_PATH=$DESTINATION_PATH/emmate_config.cmake
TEMP_CMAKE_CONFIG_FILE_PATH=$DESTINATION_PATH/emmate_config_cmake.tmp

source $EMMATE_CONFIG_FILE_PATH

echo "# CMake Configuration" > $TEMP_CMAKE_CONFIG_FILE_PATH

config_var=$(grep --regexp ^[A-Z] $EMMATE_CONFIG_FILE_PATH | cut -d= -f 1)

for argv in $config_var
do
	echo -e "set ( $argv \"${!argv}\" )" >> $TEMP_CMAKE_CONFIG_FILE_PATH
done

if [ -f $CMAKE_CONFIG_FILE_PATH ]
then
	file_diff=$(diff $CMAKE_CONFIG_FILE_PATH $TEMP_CMAKE_CONFIG_FILE_PATH)
	if [ -z "$file_diff" ]
	then
		# do nothing
		# : used for pass from this (if/else) state
		: 
	else
		#echo execute_component.mk file_check
		cat $TEMP_CMAKE_CONFIG_FILE_PATH > $CMAKE_CONFIG_FILE_PATH
	fi
else
	cat $TEMP_CMAKE_CONFIG_FILE_PATH > $CMAKE_CONFIG_FILE_PATH
fi


# remove all temp files from current directory
rm -fv $TEMP_CMAKE_CONFIG_FILE_PATH



echo ################################################################################