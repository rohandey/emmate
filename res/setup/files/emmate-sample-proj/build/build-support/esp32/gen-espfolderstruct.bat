@ECHO OFF

echo gen-espfolderstructure


rem #
rem #------------------------------------------------------------------------------------------------------------------------------
rem # To run this script's function pass the following arguments from command line
rem # 1. Project Source-Code Directory where platform-specific project has been created.
rem # 2. Project Specific Name
rem # 3. Project Library / Framework Directory where platform specific libraries / Framework have been copied.

rem # Example: " gen-espfolderstruct.sh project_path project_name project_SDK_path  "

rem #echo "Destination Path $1"
set DESTINATION_PATH=%1
echo %DESTINATION_PATH%
set PROJECT_NAME=%2
echo %PROJECT_NAME%
set PROJECT_SDK=%3
echo %PROJECT_SDK%

echo.
echo.
echo ### Creating ESP Application Project Structure #############################################
echo Creating ESP Application Project Structure for Project:	%PROJECT_NAME%

set CURR_SCRIPT_DIR=%PROJECT_DIR%\build\build-support\esp32
echo %CURR_SCRIPT_DIR%

echo .....................................................................
echo Creating %DESTINATION_PATH%\components Directory
md %DESTINATION_PATH%\components
echo Creating %DESTINATION_PATH%\main Directory
md %DESTINATION_PATH%\main
echo .....................................................................
echo.

echo .....................................................................
echo Copying component.mk to %DESTINATION_PATH%\main Directory"
rem set componentMkPath=%CURR_SCRIPT_DIR%\component.mk
rem cp -v -u $componentMkPath $DESTINATION_PATH/main
echo .....................................................................
echo.

echo .....................................................................
echo Copying sdkconfig.defaults to %DESTINATION_PATH% Directory
rem #echo $PWD
rem #echo /platform/esp32/sdkconfig
xcopy /i /d /y /f %CURR_SCRIPT_DIR%\sdkconfig  %DESTINATION_PATH%
echo .....................................................................
echo.

echo .....................................................................
echo Coying partitions.csv to %DESTINATION_PATH% Directory
rem #echo $PWD
rem #echo /platform/esp32/partitions.csv
xcopy /i /d /y /f  %CURR_SCRIPT_DIR%\partitions.csv  %DESTINATION_PATH%
echo .....................................................................
echo

rem #if [ $PROJ_BUILD_MODE == "coredev" ]
rem #then
rem #	echo .....................................................................
rem #	echo -e "Copying Makefile to $DESTINATION_PATH"
rem #	cp -v -u $CURR_SCRIPT_DIR/Makefile  $DESTINATION_PATH
rem #else
 	echo .....................................................................
 	echo Creating Makefile to %DESTINATION_PATH% Directory
rem #MakeFile Contents

	set MAKEFILE_PATH="%DESTINATION_PATH%\Makefile"
	set TEMP_MAKEFILE_PATH="%DESTINATION_PATH%\Makefile.tmp"

	echo. &echo. > %TEMP_MAKEFILE_PATH%
	echo PROJECT_NAME := %PROJECT_NAME% >> %TEMP_MAKEFILE_PATH%
	echo. >> %TEMP_MAKEFILE_PATH%
	
 	if not "%PROJECT_SDK%" == "" (
 		echo ........
 		set idf_path=%PROJECT_SDK%
		echo. >> %TEMP_MAKEFILE_PATH%
		echo. >> %TEMP_MAKEFILE_PATH%
		echo ifndef IDF_PATH >> %TEMP_MAKEFILE_PATH%
		echo export IDF_PATH := %idf_path% >> %TEMP_MAKEFILE_PATH%
		echo endif >> %TEMP_MAKEFILE_PATH%
		echo. >> %TEMP_MAKEFILE_PATH%
	) else (
		echo. >> %TEMP_MAKEFILE_PATH%
		echo include %IDF_PATH%\make\project.mk >> %TEMP_MAKEFILE_PATH%
		echo. >> %TEMP_MAKEFILE_PATH%
	)
	
	if not exist %MAKEFILE_PATH% (
		type %TEMP_MAKEFILE_PATH% > %MAKEFILE_PATH%
	) else (
		FC %TEMP_MAKEFILE_PATH%  %MAKEFILE_PATH%
		if %errorlevel% NEQ 0 (
			type %TEMP_MAKEFILE_PATH% > %MAKEFILE_PATH%
		)
	)

	del /s /f /q %TEMP_MAKEFILE_PATH%
	


	echo .....................................................................
	echo.

echo #####################################################################
echo.


