@ECHO OFF

rem echo Start execution execute-menuconf.bat
rem echo %PLATFORM_TARGET%

rem echo %~1

rem pause

rem exporting MenuConfiguration Helper Files
set MENUCONFIG_PY_PATH=%CURR_DIR%\core-menuconfig\menuconf_gen.py
echo MENUCONFIG_PY_PATH = %MENUCONFIG_PY_PATH%

set BASE_KCONFIG_FILE_PATH=%CURR_DIR%\core-menuconfig\Kconfig
echo BASE_KCONFIG_FILE_PATH = %BASE_KCONFIG_FILE_PATH%

rem pause

SETLOCAL EnableDelayedExpansion
rem if "%PLATFORM_TARGET%"=="esp32" (
rem 	echo ........--------
	rem pause
	
rem 	call set PLATFORM_KCONFIG_FILE_PATH=!CURR_DIR!\build-support\esp32\Kconfig.platform
rem 	pause
rem 	call set SOM_KCONFIG_FILE_PATH=!CURR_DIR!\build-support\esp32\Kconfig.som
	
rem 	pause
rem )

if "%PLATFORM_TARGET%"=="esp32" call set PLATFORM_KCONFIG_FILE_PATH=!CURR_DIR!\build-support\esp32\Kconfig.platform & call set SOM_KCONFIG_FILE_PATH=!CURR_DIR!\build-support\esp32\Kconfig.som

echo PLATFORM_KCONFIG_FILE_PATH = %PLATFORM_KCONFIG_FILE_PATH%
echo SOM_KCONFIG_FILE_PATH = %SOM_KCONFIG_FILE_PATH%

rem pause

set TEMP_KCONFIG_FILE_PATH=%CURR_DIR%\core-menuconfig\Kconfig.tmp
ECHO TEMP_KCONFIG_FILE_PATH = %TEMP_KCONFIG_FILE_PATH%

set MENUCONF_CONFIG_FILE_PATH=%CURR_DIR%\..\emmate_config
ECHO MENUCONF_CONFIG_FILE_PATH = %MENUCONF_CONFIG_FILE_PATH%

set MENUCONF_CONFIG_OLD_FILE_PATH=%CURR_DIR%\..\emmate_config.old
ECHO MENUCONF_CONFIG_OLD_FILE_PATH = %MENUCONF_CONFIG_OLD_FILE_PATH%

set MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH=%CURR_DIR%\menuconfig-gen-execution-status
ECHO MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH = %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH%

set AUTO_CONFIG_H_FILE_PATH=%CURR_DIR%\..\emmate\emmate_config\core_config.h
ECHO AUTO_CONFIG_H_FILE_PATH = %AUTO_CONFIG_H_FILE_PATH%


set GET_FILE_MODIFY_TIME=%CURR_DIR%\build-support\getFileModifyTime.py
ECHO GET_FILE_MODIFY_TIME = %GET_FILE_MODIFY_TIME%


rem exporting Module's Kconfigs File path
set CORE_EMBEDDED_KCONFIG_FILE_PATH=%CURR_DIR%\..\emmate\
ECHO CORE_EMBEDDED_KCONFIG_FILE_PATH = %CORE_EMBEDDED_KCONFIG_FILE_PATH%

rem export CORE_EMBEDDED_CORE_SRC_FILE_PATH=$CURR_DIR/../platform/core/
rem export CORE_EMBEDDED_ESP32_SRC_FILE_PATH=$CURR_DIR/../platform/esp/

rem exporting Configured Build File/Directory location mapping CSV
set CSV_FILE_PATH=%CURR_DIR%\build-cpy.csv
ECHO CSV_FILE_PATH = %CSV_FILE_PATH%
rem pause


rem ### Execute Core Menuconfig #############################################
ECHO.
ECHO.
ECHO ### Execute Core Menuconfig #############################################

echo %cd%
if "%PLATFORM_TARGET%"=="esp32" (
SETLOCAL enabledelayedexpansion
	if not "%IDF_PATH%"=="" (
		ECHO .....................................................................
		ECHO Collect ESP32-IDF Version:
		echo IDF=!IDF_PATH!
		cd !IDF_PATH!
		echo !cd!
		for /f "" %%i in ('git describe --tags --dirty') do set PLATFORM_IDF_VERSION=%%i
		echo !PLATFORM_IDF_VERSION!
		ECHO .....................................................................
		ECHO.
	) else (
		echo ....IDF_PATH not define....
	)

)
echo !PLATFORM_IDF_VERSION!
ECHO.

rem pause

ECHO .....................................................................
ECHO Create %TEMP_KCONFIG_FILE_PATH%...
rem # create $TEMP_KCONFIG_FILE_PATH

type %BASE_KCONFIG_FILE_PATH% > %TEMP_KCONFIG_FILE_PATH%
rem pause
set ABSOLUTE_PATH_SOM_KCONFIG_FILE_PATH=%SOM_KCONFIG_FILE_PATH:\=\\%
echo %ABSOLUTE_PATH_SOM_KCONFIG_FILE_PATH%

echo %PLATFORM_IDF_VERSION%

set ABSOLUTE_PATH_PLATFORM_KCONFIG_FILE_PATH=%PLATFORM_KCONFIG_FILE_PATH:\=\\%
echo %ABSOLUTE_PATH_PLATFORM_KCONFIG_FILE_PATH%

rem SETLOCAL enabledelayedexpansion
if "%OS_TYPE%" == "Windows" (
	echo .....
	ECHO 	source "%ABSOLUTE_PATH_SOM_KCONFIG_FILE_PATH%" >> %TEMP_KCONFIG_FILE_PATH%
	ECHO 	source "%ABSOLUTE_PATH_PLATFORM_KCONFIG_FILE_PATH%"  >> %TEMP_KCONFIG_FILE_PATH%
)

rem pause

ECHO.
ECHO 	menu "EmMate Module Configurations" >> %TEMP_KCONFIG_FILE_PATH%

rem # find Base Kconfig file to start the Menuconfig window
call :search_and_add_kconfig_path
 
ECHO 	endmenu >> %TEMP_KCONFIG_FILE_PATH%
ECHO .....................................................................
ECHO.

ECHO.

rem pause
rem echo %~1
set arg=%~1
rem echo %arg%
rem pause

IF "%1"=="-menuconfig" (
	ECHO ............"menuconfig"
	CALL :run_menuconf_gen
) else (

	echo .. 
	
	rem  link https://stackoverflow.com/questions/10868026/how-can-i-use-multiple-conditions-in-if-in-batch-file
	if EXIST "%MENUCONF_CONFIG_FILE_PATH%" (
		echo. %MENUCONF_CONFIG_FILE_PATH% exist
	) else (
		rem # run menuconf_gen.py for execute menuconfig application
		CALL :run_menuconf_gen
	)
	
	if EXIST "%AUTO_CONFIG_H_FILE_PATH%" (
		echo. %AUTO_CONFIG_H_FILE_PATH% exist	
	) else (
		rem # run menuconf_gen.py for execute menuconfig application
		CALL :run_menuconf_gen
	)
)

rem pause

if %ERRORLEVEL% EQU 0 (
	echo. skip
) else (
	echo "#####################################################################"
	echo
	pause
	exit %ERRORLEVEL%
)

rem pause

echo .....................................................................
echo Create %CSV_FILE_PATH%..

echo # Directory Paths of Kconfig files > %CSV_FILE_PATH%

rem  link https://stackoverflow.com/questions/48467703/reading-key-value-pair-using-batch-files
rem for /f "delims=" %%a in (%MENUCONF_CONFIG_FILE_PATH%) do set ###%%a


rem rem eol stops comments from being parsed
rem rem otherwise split lines at the = char into two tokens
for /F "eol=# delims== tokens=1,*" %%a in (%MENUCONF_CONFIG_FILE_PATH%) do (
    rem proper lines have both a and b set
    rem if NOT "%%a"=="" if NOT "%%b"=="" set %%a=%%b
    if NOT "%%a"=="" set %%a=%%b
)

 
rem @echo !CONFIG_USE_VERSION!

::pause


for /f "delims=" %%a in ('dir %CORE_EMBEDDED_KCONFIG_FILE_PATH%\%KCONFIG_FILE_NAME%  /S /B /O:N') do (
	SET mydir=%%~dpa
	rem echo !mydir!
	SET mydir=!mydir:~0,-1!
	rem echo !mydir!
	
	for /f "delims=" %%i in ("!mydir!") do (
		rem echo %%~ni
		SET parent_dir=%%~ni
	)
	
	rem echo parent_dir=!parent_dir!
	
	SET "STRING="
	call :get_upper_case_str !parent_dir!
	
	set "config_var_name=CONFIG_USE_!STRING:-=_!"
	rem echo config_var_name=!config_var_name!
	rem for /f "delims=" %%k in ('call echo %%!config_var_name!%%') do set var=%%k
	call set config_var_value=%%!config_var_name!%%
	rem echo config_var_value=!config_var_value!
	
	call set config_var_core_module_path=!mydir!
	rem echo config_var_core_module_path=!config_var_core_module_path!
	
	rem call echo %%mydir:%PROJECT_DIR%=%%
	rem echo %%mydir:%PROJECT_DIR%=%%
	
	call set stripped_core_path=%%mydir:%PROJECT_DIR%=%%
	rem echo stripped_core_path=!stripped_core_path!
	
	call set stripped_esp32_path=!stripped_core_path:core=esp!
	rem echo stripped_esp32_path=!stripped_esp32_path!
	
	call set config_var_esp32_module_path=%PROJECT_DIR%!stripped_esp32_path!
	rem echo config_var_esp32_module_path=!config_var_esp32_module_path!
	
	rem SETLOCAL enabledelayedexpansion
	IF "!config_var_name!"=="CONFIG_USE_ONEWIRE" ( 
		echo !config_var_name!,!config_var_core_module_path!,!config_var_esp32_module_path!,!config_var_value!,-r  >> %CSV_FILE_PATH%
	) else IF "!config_var_name!"=="CONFIG_USE_SOM" ( 
		echo !config_var_name!,!config_var_core_module_path!,!config_var_esp32_module_path!,!config_var_value!,-r  >> %CSV_FILE_PATH%
	) ELSE (
		echo !config_var_name!,!config_var_core_module_path!,!config_var_esp32_module_path!,!config_var_value!  >> %CSV_FILE_PATH%
	)
	
		
	rem pause
	rem echo "------------------------------------------------------------------------------------------------------------------------"
	rem echo.
	
)

rem echo .....................................................................
echo Remove %TEMP_KCONFIG_FILE_PATH%...
del %TEMP_KCONFIG_FILE_PATH%
rem echo .....................................................................
rem echo
rem echo %errorlevel%
exit /b 0







rem  ## Script Functions ###############################################################################

:search_and_add_kconfig_path

	rem echo search_and_add_kconfig_path
	
	set KCONFIG_FILE_NAME="Kconfig"
	set KCONFIG_PROBUILD_FILE_NAME="Kconfig.projbuild"
	
	rem SETLOCAL enabledelayedexpansion
	rem for /f "delims=" %%G in ('dir %CORE_EMBEDDED_KCONFIG_FILE_PATH%\%KCONFIG_FILE_NAME%  /S /B') do (
	rem  search directory name in the sorted by name
	for /f "delims=" %%G in ('dir %CORE_EMBEDDED_KCONFIG_FILE_PATH%  /A:D /B /O:N') do (
		rem echo ..%%G
		
		rem set CORE_KCONFIG_FILES_ARR[!idx!]=%CORE_EMBEDDED_KCONFIG_FILE_PATH%/%%G/Kconfig
		set konfig_file_path=%CORE_EMBEDDED_KCONFIG_FILE_PATH%\%%G\Kconfig
		set konfig_file_path=!konfig_file_path:\=\\!
		rem echo !konfig_file_path!
		
		echo 	source "!konfig_file_path!" >> %TEMP_KCONFIG_FILE_PATH%
	)

	rem pause
	rem echo $$$$
	EXIT /B %ERRORLEVEL%



:run_menuconf_gen
	echo run_menuconf_gen
	for %%? in (%MENUCONF_CONFIG_FILE_PATH%) do set config_file_old_creation_time=%%~t?
	rem echo %config_file_old_creation_time%
	ECHO config_file_old_creation_time = %config_file_old_creation_time%
	rem # execute menuconf_gen.py
	ECHO .....................................................................
	ECHO execute menuconf_gen.py...
	
	if "%PROJ_BUILD_MODE%"=="gui-config" (

		if "%OS_TYPE%"=="Windows" (
			echo python %MENUCONFIG_PY_PATH% --config %MENUCONF_CONFIG_FILE_PATH% --kconfig %TEMP_KCONFIG_FILE_PATH% --output autoconf %AUTO_CONFIG_H_FILE_PATH% --build-mode %PROJ_BUILD_MODE%
			echo.
			echo.
			
			rem The below command 'py -3' requires "Python Launcher for Windows"
			rem Install Python version greater than 3.3 in “Customize installation” mode
			py -3 %MENUCONFIG_PY_PATH% --config %MENUCONF_CONFIG_FILE_PATH% --kconfig %TEMP_KCONFIG_FILE_PATH% --output autoconf %AUTO_CONFIG_H_FILE_PATH% --build-mode %PROJ_BUILD_MODE%
		)
	)
	
	rem if "%PROJ_BUILD_MODE%"=="cli-config" (
	rem 	if "%OS_TYPE%"=="Windows"(
	rem 		python %MENUCONFIG_PY_PATH% --config %MENUCONF_CONFIG_FILE_PATH% --kconfig %TEMP_KCONFIG_FILE_PATH% --output autoconf %AUTO_CONFIG_H_FILE_PATH% --output config %MENUCONF_CONFIG_FILE_PATH% --build-mode %PROJ_BUILD_MODE%   
	rem 	)
	rem )
	
	rem pause
	echo python menuconfig runs well
	
	
	for %%? in (%MENUCONF_CONFIG_FILE_PATH%) do set config_file_creation_time=%%~t?
	ECHO config_file_creation_time = %config_file_creation_time%
	
	
	rem echo ----- %MENUCONF_CONFIG_FILE_PATH% 
	rem echo ----- %MENUCONF_CONFIG_OLD_FILE_PATH%
	
	fc %MENUCONF_CONFIG_FILE_PATH% %MENUCONF_CONFIG_OLD_FILE_PATH%
	echo %errorlevel%
	
	rem pause
	
	if "%config_file_creation_time%" == "%config_file_old_creation_time%" (
		echo skip..
	) 
	if [%errorlevel%] EQU [0] ( 
	 	echo skip..
	) else (
		echo conf file update
		rem ECHO @echo off > %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH%
		rem ECHO set MENUCONFIG_EXECUTION_STATUS=y >> %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH%
		ECHO MENUCONFIG_EXECUTION_STATUS=y > %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH%
		call set errorlevel=0
	)
	rem pause
	
	ECHO .....................................................................
	ECHO.
	
	rem pause
	rem echo @@@@
	EXIT /B %ERRORLEVEL%



:get_upper_case_str
SET STRING=%1
IF [%STRING%]==[] GOTO:EOF
SET STRING=%STRING:a=A%
SET STRING=%STRING:b=B%
SET STRING=%STRING:c=C%
SET STRING=%STRING:d=D%
SET STRING=%STRING:e=E%
SET STRING=%STRING:f=F%
SET STRING=%STRING:g=G%
SET STRING=%STRING:h=H%
SET STRING=%STRING:i=I%
SET STRING=%STRING:j=J%
SET STRING=%STRING:k=K%
SET STRING=%STRING:l=L%
SET STRING=%STRING:m=M%
SET STRING=%STRING:n=N%
SET STRING=%STRING:o=O%
SET STRING=%STRING:p=P%
SET STRING=%STRING:q=Q%
SET STRING=%STRING:r=R%
SET STRING=%STRING:s=S%
SET STRING=%STRING:t=T%
SET STRING=%STRING:u=U%
SET STRING=%STRING:v=V%
SET STRING=%STRING:w=W%
SET STRING=%STRING:x=X%
SET STRING=%STRING:y=Y%
SET STRING=%STRING:z=Z%
rem ECHO %STRING%



