

ECHO --------------------- EmMate Build Help ---------------------

ECHO Usage: .\build.bat [options]

ECHO Option: 

::MAKE_OPT_ARR=("-make" "Build the EmMate project")
::CLEAN_OPT_ARR=("-clean" "Clean the EmMate project")
::FLASH_OPT_ARR=("-flash" "Flash the binaries to the EmMate Device")
::ERASE_FLASH_OPT_ARR=("-erase-flash" "Erase the flashed binaries from the EmMate Device")
::LOG_OPT_ARR=("-log" "View the Output Log of the EmMate Device")

ECHO -menuconfig			Execute the EmMate Project Menuconfig
ECHO -help				Show Help Information of the EmMate Build CMD
ECHO -make				Build the EmMate project(currently make into ESP Project structure)
ECHO -flash				Flash the binaries to the EmMate Device
ECHO -log				View the Output Log of the EmMate Device

ECHO.
ECHO.