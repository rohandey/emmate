#-------------------------------------------------------------------------------
# SETTING UP APPLICATION
#-------------------------------------------------------------------------------
To build an application based on the iquesters EmMate framework, the
following steps should be followed to setup the project:

1. Create a project folder.

2. Create a folder named "setup" in your project.

3. Copy setup.sh and setup.conf files from the core release package to the setup
   folder in your project.
   
4. Open setup.conf file and specify the directory location of the extracted core
   release zip against the property "RELEASE_PKG_DIR".
   For example,
      If the distribution zip is extracted at D:/iquesters/EmMate
      Then the RELEASE_PKG_DIR=D:/iquesters/EmMate
   NOTE: D:/iquesters/EmMate should contain all directories and files
         mentioned in DISTRIBUTION ZIP CONTENTS section

5. Run the setup.sh (shell script).
   This will create the required folder structure and copy other dependency
   files in your project.