/*
 * persistent_mem_sys_standby.h
 *
 *  Created on: 05-Nov-2019
 *      Author: Rohan Dey
 */

#ifndef PERSISTENT_MEM_SYS_STANDBY_H_
#define PERSISTENT_MEM_SYS_STANDBY_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#include "core_constant.h"
#include "core_common.h"

/**
 * @brief This function reads the System Standby Mode from the selected persistent memory and returns the same via out parameter
 *
 * @param[out]	p_standby_mode	Pointer to integer. 0 - system not in standby mode, 1 - system in standby mode
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err read_sys_standby_mode_from_persistent_mem(int *p_standby_mode);

/**
 * @brief This function writes the System Standby Mode to the selected persistent memory
 *
 * @param[in]	standby_mode	Integer value to be saved in the memory. 0 - system not in standby mode, 1 - system in standby mode
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err write_sys_standby_mode_to_persistent_mem(int standby_mode);

/**
 * @brief This function reads the System Standby Heartbeat Frequency from the selected persistent memory and returns the same via out parameter
 *
 * @param[out]	p_standby_hb_freq	Pointer to integer. Heartbeat frequency in seconds
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err read_sys_standby_hbfreq_from_persistent_mem(int *p_standby_hb_freq);

/**
 * @brief This function writes the System Standby Heartbeat Frequency to the selected persistent memory
 *
 * @param[in]	standby_mode	Integer value to be saved in the memory. Heartbeat frequency in seconds
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err write_sys_standby_hbfreq_to_persistent_mem(int standby_hb_freq);

#ifdef __cplusplus
}
#endif

#endif /* PERSISTENT_MEM_SYS_STANDBY_H_ */
