/*
 * persistent_mem_sys_hb.c
 *
 *  Created on: 06-Nov-2019
 *      Author: Rohan Dey
 */


#include "persistent_mem_sys_hb.h"
#include "persistent_mem_helper.h"
#include "migcloud_storage_util.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG LTAG_PERSISTENT

/*
 * Read the system heartbeat frequency from persistent memory
 */
core_err read_syshb_freq_from_persistent_mem(int *p_hb_freq) {
	core_err ret = CORE_FAIL;

	if ((ret = persistent_read_config_by_key(SYS_HBFREQ_NVS_KEY, (void*) p_hb_freq, sizeof(int))) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Reading Heartbeat Frequency from persistent memory failed");
	}
	return ret;
}

/*
 * Write the system heartbeat frequency into persistent memory
 */
core_err write_syshb_freq_to_persistent_mem(int hb_freq) {
	core_err ret = CORE_FAIL;

	if ((ret = persistent_write_config_by_key(SYS_HBFREQ_NVS_KEY, (void*) &hb_freq, sizeof(int))) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Writing Heartbeat Frequency to persistent memory failed");
	}
	return ret;
}
