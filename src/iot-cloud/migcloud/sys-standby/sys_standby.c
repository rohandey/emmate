/*
 * sys_standby.c
 *
 *  Created on: 05-Nov-2019
 *      Author: Rohan Dey
 */


#include "sys_standby.h"
#include "http_client_api.h"
//#include "http_client_core.h"
//#include "http_constant.h"
#include "input_processor.h"
//#include "inpproc_utils.h"
//#include "system.h"
#include "migcloud_urls.h"
#include "system_utils.h"
#include "core_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_HMI
#include "system_hmi_led_notification.h"
#endif
#if CONFIG_USE_PERSISTENT
//#include "persistent_mem.h"
#include "migcloud_storage.h"
#endif
#include <string.h>

SYSTEM_STANDBY_MODE m_standby_mode_stat;		/*!< System's Standby Mode status variable */
static migcloud_standby_hw_function m_systemstandby_func = NULL;

#define TAG LTAG_MIG_STANDBY
#define SYSTEM_STANDBY_RESPONSE_SIZE	(sizeof(int) + sizeof(CoreError) + 1)

typedef struct {
	int standby_stat;
} SystemStandbyStatRequest;

typedef struct {
	int status; /**< Status. Success or Failure */
	CoreError error; /**< Error object */
} SystemStandbyStatResponse;

core_err migcloud_set_standby_hw_mode(SYSTEM_STANDBY_MODE standby_mode) {
	if (standby_mode > STANDBY_MODE_ON) {
		return CORE_ERR_INVALID_ARG;
	}
	m_standby_mode_stat = standby_mode;
	return CORE_OK;
}

SYSTEM_STANDBY_MODE migcloud_get_standby_hw_mode() {
	return m_standby_mode_stat;
}

static core_err make_sys_standby_update_status_request(char **ppbuf, int *plen, SystemStandbyStatRequest *sys_standby_stat_req) {
//	core_err ret = CORE_FAIL;
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);
	char *serialized_string = NULL;

	CORE_LOGV(TAG, "Going to make Device Configuration Reset Status JSON with the following values:");

	/* Set JSON key value */
	CORE_LOGD(TAG, "[%s]: %d", GET_VAR_NAME(sys_standby_stat_req->standby_stat, "->"), sys_standby_stat_req->standby_stat);
	json_object_set_number(root_object, GET_VAR_NAME(sys_standby_stat_req->standby_stat, "->"),
			(int) sys_standby_stat_req->standby_stat);

	serialized_string = json_serialize_to_string(root_value);

	size_t len = json_serialization_size(root_value);
	len = len - 1;  // since json_serialization_size returns size + 1
	CORE_LOGD(TAG, "System Standby Status Request JSON Len = %d\r\n", len);

	char *ptemp = (char*) malloc(len);
	if (ptemp == NULL) {
		CORE_LOGE(TAG, "make_sys_standby_status_request malloc failed!");
		return CORE_FAIL;
	}
	memset(ptemp, 0x00, len);
	memcpy(ptemp, serialized_string, len);
	*plen = len;
	*ppbuf = ptemp;

	json_value_free(root_value);
	json_free_serialized_string(serialized_string);
	return CORE_OK;
}

static core_err parse_sys_standby_update_status_response(char *json_buff, SystemStandbyStatResponse *sys_standby_stat_resp) {
	core_err ret = CORE_FAIL;

	/* Parse the common info: stat and error */
	ret = inproc_parse_json_common_info(json_buff, &sys_standby_stat_resp->status, &sys_standby_stat_resp->error);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "inproc_parse_json_common_info failed! not proceeding further!");
		ret = CORE_FAIL;
	}

	return ret;
}


static core_err send_sys_standby_update_stat_to_server(bool standby_stat) {
	core_err ret = CORE_FAIL;

	char *http_resp = NULL;
	size_t http_resp_len = 0;
	char *json_buf = NULL;
	int json_len = 0;

	SystemStandbyStatRequest sys_standby_stat_req;
	memset(&sys_standby_stat_req, 0, sizeof(SystemStandbyStatRequest));

	sys_standby_stat_req.standby_stat = (int) standby_stat;

	ret = make_sys_standby_update_status_request(&json_buf, &json_len, &sys_standby_stat_req);
	CORE_LOGI(TAG, "Request: %.*s", json_len, json_buf);

	/* Allocate memory for the http response */
	http_resp = (char*) calloc(SYSTEM_STANDBY_RESPONSE_SIZE, sizeof(char));
	if (http_resp == NULL) {
		CORE_LOGE(TAG, "memory allocation for http response failed");
		goto free_memory;
	}
	memset(http_resp, 0, SYSTEM_STANDBY_RESPONSE_SIZE);
	http_resp_len = 0;

	/* Do http operation */
	uint16_t http_stat = 0;
	ret = do_http_operation(IQ_SYS_SYS_STANDBY_STATUS_POST_URL, IQ_HOST_PORT, IQ_HOST_ROOTCA, HTTP_CLIENT_METHOD_POST,
	IQ_HOST, HTTP_USER_AGENT,
	CONTENT_TYPE_APPLICATION_JSON, json_buf, json_len, http_resp, &http_resp_len, SYSTEM_STANDBY_RESPONSE_SIZE,
			&http_stat);

	if (ret == CORE_OK) {
		CORE_LOGI(TAG, "Response: %.*s", http_resp_len, http_resp);

		SystemStandbyStatResponse sys_standby_stat_resp;
		memset(&sys_standby_stat_resp, 0, sizeof(SystemStandbyStatResponse));

		ret = parse_sys_standby_update_status_response(http_resp, &sys_standby_stat_resp);
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "System Standby Status Response parsing failed, don't know what to do yet!!");
			// TODO: Handle error
		}
	} else {
		CORE_LOGE(TAG, "HTTP failed with status code = [ %d ]!", http_stat);
		ret = CORE_FAIL;
	}

	free_memory:
	/* Free the allocated http respose memory */
	CORE_LOGD(TAG, "Freeing allocated response memory");
	free(json_buf);
	free(http_resp);

	return ret;
}
/**
 * @brief		This function calls the registered system standby callback. The system standby function should be registered
 * 				by calling migcloud_register_standby_hw_event_function()
 *
 * @note		The called 'system standby function' must not block.
 *
 * @param[in]	standby Signifies whether the standby mode is being activated or deactivated
 * */
static void notify_system_standby_event(bool standby_status) {
	if (m_systemstandby_func != NULL)
		m_systemstandby_func(standby_status);
}

void do_system_standby_operation(SYSTEM_STANDBY_MODE standby_status, int hb_standby_interval_sec) {
	/*
	 * System Standby:
	 * 1. Notify application via callback
	 * 2. Save STANDBY mode and frequency in NVS
	 * 3. Inform the server that STANDBY mode is activated/deactivated
	 * 4. Show STANDBY notification
	 * 5. On reboot in standby mode, get the standby mode and frequency from NVS
	 * 6. Notify application via callback
	 * 7. System should remain in standby mode
	 * */
	core_err ret = CORE_FAIL;

	/* Update the standby global variable */
	migcloud_set_standby_hw_mode(standby_status);

	/* Notify the application */
	notify_system_standby_event(standby_status);

#if CONFIG_USE_PERSISTENT
	/* Save the current standby mode and heartbeat frequency into the persistent memory */
	ret = write_sys_standby_mode_to_persistent_mem((int) standby_status);
	ret = write_sys_standby_hbfreq_to_persistent_mem(hb_standby_interval_sec);
#endif

	/* Inform the server */
	if (ret == CORE_OK) {
		send_sys_standby_update_stat_to_server(true);
	} else {
		send_sys_standby_update_stat_to_server(false);
	}

	/* Start Standby Mode system notification */
	if (standby_status) {
		show_system_standby_on_notification();
	} else {
		show_system_standby_off_notification();
	}
}

core_err migcloud_register_standby_hw_event_function(migcloud_standby_hw_function systemstandby_func) {
	if (systemstandby_func != NULL) {
		m_systemstandby_func = systemstandby_func;
		return CORE_OK;
	} else {
		return CORE_ERR_INVALID_ARG;
	}
}


