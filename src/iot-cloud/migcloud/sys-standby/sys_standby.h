/*
 * sys_standby.h
 *
 *  Created on: 05-Nov-2019
 *      Author: Rohan Dey
 */

#ifndef SYS_STANDBY_H_
#define SYS_STANDBY_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

typedef enum {
	STANDBY_MODE_OFF = 0,	/**< The EmMate System's Standby mode is OFF. Normal operation */
	STANDBY_MODE_ON = 1		/**< The EmMate System's Standby mode is ON. */
} SYSTEM_STANDBY_MODE;

/**
 * @brief		This function sets the system's standby mode. This function should not be called from the application
 *
 * @param[in]	standby_mode The standby mode of type SYSTEM_STANDBY_MODE
 *
 * @return
 * 			- CORE_OK					If the value was set successfully
 * 			- CORE_ERR_INVALID_ARG		If the value is greater than 1
 * */
core_err migcloud_set_standby_hw_mode(SYSTEM_STANDBY_MODE standby_mode);

/**
 * @brief	Function to get the EmMate system's standby mode.
 *
 * @return	STANDBY_MODE_ON		See the enum SYSTEM_STANDBY_MODE
 * 			STANDBY_MODE_OFF	See the enum SYSTEM_STANDBY_MODE
 */

SYSTEM_STANDBY_MODE migcloud_get_standby_hw_mode();

/**
 * @brief	Function pointer type to point to a system standby event notification callback
 *
 * @note	The function signature should be as follows:
 *
 * 			void app_systemstandby(bool standby)
 */
typedef void (*migcloud_standby_hw_function)(bool);

/**
 * @brief		This function registers a function of type migcloud_standby_hw_function, which will be called when an system standby
 * 				mode is activated and deactivated.
 *
 * @note		The system standby is a feature of the migCloud (https://mig.iquesters.com/?s=cloud) web application.
 * 				This feature allows the user to inform the device to go into standby mode from the server. Generally when a
 * 				standby mode is activated the a device should perform minimum operations.
 *
 * 				Upon activation of the standby mode from the server the following things will happen:
 * 				- The system heartbeat frequency will change to standby frequency
 * 				- The 'app post data' module will keep working as normal
 * 				- The system will notify the application about the standby mode change (i.e. activated or deactivated)
 * 				- It is the responsibility of the application to perform the necessary tasks required in the standby mode
 *
 * 				While in standby mode, if the device is rebooted, the EmMate System will remain in standby mode and
 * 				perform tasks as per the standby mode. In this case the application will not be notified. The application can
 * 				get the standby state by calling migcloud_get_standby_hw_mode()
 *
 * 				Upon deactivation of the standby mode the system and application should go back into doing normal operations.
 *
 * @param[in]	systemstandby_func This is the pointer to the function which will be registered
 *
 * @return
 * 			- CORE_OK					If the function was registered successfully
 * 			- CORE_ERR_INVALID_ARG		If the input param is NULL
 * */
core_err migcloud_register_standby_hw_event_function(migcloud_standby_hw_function systemstandby_func);

#ifdef __cplusplus
}
#endif

#endif /* SYS_STANDBY_H_ */
