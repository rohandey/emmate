/*
 * migcloud_http_status_update.h
 *
 *  Created on: 12-Nov-2019
 *      Author: Rohan Dey
 */

#ifndef MIGCLOUD_HTTP_STATUS_UPDATE_H_
#define MIGCLOUD_HTTP_STATUS_UPDATE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "fota_core.h"

typedef enum {
	MIGCLOUD_TASK_APP_CONFIG = 0, 	/*< Migcloud task type - application configuration */
	MIGCLOUD_TASK_SYSTEM_CONFIG, 	/*< Migcloud task type - system configuration */
	MIGCLOUD_TASK_DEVICE_CONFIG, 	/*< Migcloud task type - device configuration */
	MIGCLOUD_TASK_FOTA, 			/*< Migcloud task type - Firmware Over The Air Update */
	MIGCLOUD_TASK_RELEASE_SOMTHING	/*< Migcloud task type - release somthing */
} MIGCLOUD_TASK_TYPE;

typedef enum {
	MIGCLOUD_STATUS_NEW = 1,
	MIGCLOUD_STATUS_AVAILABLE = 2,
	MIGCLOUD_STATUS_NEEDED = 3,
	MIGCLOUD_STATUS_DOWNLOADING = 30,
	MIGCLOUD_STATUS_DOWNLOADED = 31,
	MIGCLOUD_STATUS_DOWNLOAD_FAILED = 32,
	MIGCLOUD_STATUS_UPDATING = 20,
	MIGCLOUD_STATUS_UPDATED = 21,
	MIGCLOUD_STATUS_UPDATE_FAILED = 22,
	MIGCLOUD_STATUS_PROCESSING = 90,
	MIGCLOUD_STATUS_PROCESSED = 91,
	MIGCLOUD_STATUS_PROCESS_FAILED = 92,
	MIGCLOUD_STATUS_UNKNOWN = 0,
} MIGCLOUD_TASK_STATUSES;

/**
 * @brief	This structure is used when updating a FOTA task's status to the migCloud server
 **/
typedef struct {
	char fota_id[FOTA_ID_LEN + 1]; /*< The FOTA ID against which the status will be updated */
	int dl_try; /*< The download try count when the firmware is being downloaded */
} MigcloudFotaStatus;

/**
 * @brief	This function is used to send all kinds to status updates to the migCloud server through HTTP(S)
 *
 * @note	When using this function for tasks other than FOTA update status, then the param - fota_stat_info
 * 			must be set to NULL
 *
 * @param[in]	task_type		This signifies the type of task this status is being sent for. See enum MIGCLOUD_TASK_TYPE
 * @param[in]	status			Status Code
 * @param[in]	fota_stat_info	This information if required if the task_type is MIGCLOUD_TASK_FOTA. Otherwise this must be set to NULL
 *
 * @return
 * 				CORE_OK		Success
 * 				CORE_FAIL	Failure
 */
core_err migcloud_send_status_via_http(MIGCLOUD_TASK_TYPE task_type, MIGCLOUD_TASK_STATUSES status,
		MigcloudFotaStatus *fota_stat_info);

#ifdef __cplusplus
}
#endif

#endif /* MIGCLOUD_HTTP_STATUS_UPDATE_H_ */
