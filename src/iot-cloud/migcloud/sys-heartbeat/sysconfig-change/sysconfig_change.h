/*
 * sysconfig_change.h
 *
 *  Created on: 12-Nov-2019
 *      Author: Rohan Dey
 */

#ifndef SYSCONFIG_CHANGE_H_
#define SYSCONFIG_CHANGE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

typedef enum {
//	APPCFG_STATUS_NEW = 1,
//	APPCFG_STATUS_AVAILABLE = 2,
//	APPCFG_STATUS_NEEDED = 3,
//	APPCFG_STATUS_DOWNLOADING = 30,
//	APPCFG_STATUS_DOWNLOADED = 31,
//	APPCFG_STATUS_DOWNLOAD_FAILED = 32,
//	APPCFG_STATUS_UPDATING = 20,
	SYSCFG_STATUS_UPDATED = 21,
	SYSCFG_STATUS_UPDATE_FAILED = 22,
	SYSCFG_STATUS_UNKNOWN = 0,
} SYSCFG_STATUSES;

#ifdef __cplusplus
}
#endif

#endif /* SYSCONFIG_CHANGE_H_ */
