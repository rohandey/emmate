# OneWire Module

## Overview

The EmMate's OneWire module is based on Maxim's OneWire Library. Please check the [OneWire Library on mbed portal](https://os.mbed.com/teams/Maxim-Integrated/code/OneWire/)

#### Module Specific Configurations

The following configurations are needed for the OneWire module to work.

![](img/one-wire-config.png)