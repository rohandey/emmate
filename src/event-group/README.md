# Event Group Module

## Overview

The Event Group module is a wrapper library around FreeRTOS's Event Group Library.

To know more about event groups, please read the [FreeRTOS's documentation](https://www.freertos.org/FreeRTOS-Event-Groups.html)

---
**This module is a mandatory requirement for the framework, please do not disable it from the Menuconfig Application**


![](img/event-group-config.png)
---