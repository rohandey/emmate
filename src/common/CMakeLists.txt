if(CONFIG_USE_BULK_DATA_TRANSACTOR)
	add_subdirectory(bulk-data-transactor)
endif()

if(CONFIG_USE_DATA_STRUCTURES)
	add_subdirectory(data-structures)
endif()

if(CONFIG_USE_ERRORS)
	add_subdirectory(errors)
endif()	

set(srcs
	heap_debug.c
	)

add_library(common STATIC ${srcs})

if(CONFIG_PLATFORM_ESP_IDF)
	list(APPEND IDF_LIBS
				idf::heap
			)
	target_link_libraries(common PRIVATE ${IDF_LIBS})
endif()

target_include_directories(common 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)