set(srcs
	linked-list/gll.c
	)

add_library(data-structures STATIC ${srcs})

target_include_directories(data-structures 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/linked-list
							)