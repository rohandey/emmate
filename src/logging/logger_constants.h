/*
 * File Name: logger_constants.h
 * Description:
 *
 *  Created on: 02-May-2019
 *      Author: Rohan Dey
 */

#ifndef LOGGING_CONSTANTS_H_
#define LOGGING_CONSTANTS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"

/* Definitions for identifier (TAG) of all emmate modules */
#define LTAG_ALL_MODULE				"*"				/**< TAG to use 'all' modules */
#define LTAG_BOARDS					"boards"		/**< TAG to use 'boards' module */
#define LTAG_COMMON					"common"		/**< TAG to use 'common' module */
#define LTAG_CONN					"conn"			/**< TAG to use 'connectivity' module */
#define LTAG_CONN_BLE				"ble"			/**< TAG to use 'ble' module */
#define LTAG_CONN_BLECONFIG			"ble-config"	/**< TAG to use 'ble-config' module */
#define LTAG_CONN_BLEGATT			"ble-gatt"		/**< TAG to use 'ble-gatt' module */
#define LTAG_CONN_WIFI				"wifi"			/**< TAG to use 'wifi' module */
#define LTAG_CONN_WIFIPLAT			"wifi-plat"		/**< TAG to use 'wifi platform' module */
#define LTAG_CONNPROTO_HTTPCLI		"http-cli"		/**< TAG to use 'HTTP client' module */
#define LTAG_CONNPROTO_HTTPCLIAPI	"http-cli-api"	/**< TAG to use 'HTTP client API' module */
#define LTAG_CONNPROTO_SNTPCLI		"sntp-cli"		/**< TAG to use 'SNTP client' module */
#define LTAG_EVENTGROUP				"event-group"	/**< TAG to use 'event group' module */
#define LTAG_HMI					"hmi"			/**< TAG to use 'HMI' module */
#define LTAG_HMI_LED				"hmi-led"		/**< TAG to use 'HMI led helper' module */
#define LTAG_HMI_BUTTON				"hmi-button"	/**< TAG to use 'HMI button helper' module */
#define LTAG_SYSTEM_HMI				"system-hmi"	/**< TAG to use 'System HMIs' module */
#define LTAG_SOM_REG				"som-reg"		/**< TAG to use 'SOM Registration' module */
#define LTAG_IOT					"iot"			/**< TAG to use 'IoT' module */
#define LTAG_IOT_AWS				"aws-iot"		/**< TAG to use 'AWS IoT' module */
#define LTAG_OTA					"fota"			/**< TAG to use 'OTA' module */
#define LTAG_INPROC					"inproc"		/**< TAG to use 'input processor' module */
#define LTAG_PERSISTENT				"persistent"	/**< TAG to use 'persistent' module */
#define LTAG_PERSISTENT_NVS			"nvs"			/**< TAG to use 'NVS' module */
#define LTAG_SYSTEM					"system"		/**< TAG to use 'system' module */
#define LTAG_SYSTEM_DEVCFG			"devcfg"		/**< TAG to use 'device config' module */
#define LTAG_SYSTEM_RELSOM			"relsom"		/**< TAG to use 'release somthing' module */
#define LTAG_SYSTEM_SYSCFG			"syscfg"		/**< TAG to use 'system config' module */
#define LTAG_SYSTEM_SYSINFO			"sysinfo"		/**< TAG to use 'system info' module */
#define LTAG_SYSTEM_SYSINIT			"sysinit"		/**< TAG to use 'system init' module */
#define LTAG_SYSTEM_SYSTIME			"systime"		/**< TAG to use 'system time' module */
#define LTAG_THREADING				"threading"		/**< TAG to use 'threading' module */
#define LTAG_UTILS					"core-utils"	/**< TAG to use 'utils' module */
#define LTAG_TCPIP					"tcpip-adaptor"	/**< TAG to use 'tcpip adaptor' module */
#define LTAG_PARSER_UTILS			"parser-utils"	/**< TAG to use 'parser utils' module */
#define LTAG_BTN_HELPER				"btn-helper"	/**< TAG to use 'button helper' module */
#define LTAG_ADC					"adc-core"		/**< TAG to use 'ADC peripheral' module */
#define LTAG_BDTRANSACTOR			"bd-transact"	/**< TAG to use 'bulk data transactor' module */
#define LTAG_I2C					"i2c-core"		/**< TAG to use 'i2c peripheral' module */
#define LTAG_FATFS					"fatfs-helper"	/**< TAG to use 'FAT file system' module */
#define LTAG_HWIDENTIFY				"hw-identify"	/**< TAG to use 'system hardware identify' module */
#define LTAG_UART					"uart-core"		/**< TAG to use 'UART peripheral' module */
#define LTAG_GPIO					"gpio-core"		/**< TAG to use 'GPIO peripheral' module */
#define LTAG_NETIF_INFO				"netifinfo"		/**< TAG to use 'Net IF Info' module */

#if CONFIG_HAVE_IOT_CLOUD_MIGCLOUD
/*********************************** IoT Cloud - migcloud LTAG ************************************************/
#define LTAG_MIG_STAT				"mig-stat"		/**< TAG to use 'migcloud status update' module */
#define LTAG_MIG_HB					"mig-hb"		/**< TAG to use 'migcloud heartbeat' module */
#define LTAG_MIG_APPCFG				"mig-appcfg"	/**< TAG to use 'migcloud application configuration' module */
#define LTAG_MIG_POSTDATA			"mig-apppost"	/**< TAG to use 'migcloud application postdata' module */
#define LTAG_MIG_STANDBY			"mig-standby"	/**< TAG to use 'migcloud standby' module */
#define LTAG_MIG_NETRESET			"mig-netrst"	/**< TAG to use 'migcloud net reset' module */
#endif

/*********************************** External-ICs LTAG ************************************************/
#define LTAG_DHT22_LIB					"DHT22-lib"					/**< TAG to use 'DHT22 External IC Library' module */
#define LTAG_LM35_LIB					"lm35-lib"					/**< TAG to use 'LM35 External IC Library' module */
#define LTAG_DS1307_LIB					"ds1307-lib"				/**< TAG to use 'DS1307 External IC Library' module */
#define LTAG_EEPROM_24AA02UID_LIB		"eeprom-24AA02UID-lib"		/**< TAG to use 'EEPROM_24AA02UID External IC Library' module */
#define LTAG_EEPROM_24C0XX_LIB			"eeprom-24C0xx-lib"			/**< TAG to use 'EEPROM_24C0xx External IC Library' module */
#define LTAG_MQ5_LIB					"mq5_lib"					/**< TAG to use 'MQ5 LPG Gas Sensor External IC Library' module */

#ifdef __cplusplus
}
#endif

#endif /* LOGGING_CONSTANTS_H_ */
