/*
 * File Name: led_platform.c
 * File Path: /emmate/src/hmi/led/esp-idf/led_platform.c
 * Description:
 *
 *  Created on: 14-Apr-2019
 *      Author: Rohan Dey
 */

#include "led_platform.h"
#include "thing.h"
#include "core_config.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "driver/gpio.h"

#define TAG	LTAG_HMI_LED

#if LEDS_NUMBER > 0
static const uint8_t m_board_led_list[LEDS_NUMBER] = LEDS_LIST;
#endif

#if LEDS_NUMBER > 0
/* Static Function Declarations */
static void init_bsp_board_leds(void);

bool get_bsp_board_led_state(uint32_t led_idx) {
	bool pin_set = gpio_get_level(m_board_led_list[led_idx]) ? true : false;
	return (pin_set == (LEDS_ACTIVE_STATE ? true : false));
}

void on_bsp_board_led(uint32_t led_idx) {
	gpio_set_level(m_board_led_list[led_idx], LEDS_ACTIVE_STATE ? 1 : 0);
}

void off_bsp_board_led(uint32_t led_idx) {
	gpio_set_level(m_board_led_list[led_idx], LEDS_ACTIVE_STATE ? 0 : 1);
}

void off_bsp_board_leds(void) {
	uint32_t i;
	for (i = 0; i < LEDS_NUMBER; ++i) {
		off_bsp_board_led(i);
	}
}

void on_bsp_board_leds(void) {
	uint32_t i;
	for (i = 0; i < LEDS_NUMBER; ++i) {
		on_bsp_board_led(i);
	}
}

void invert_bsp_board_led(uint32_t led_idx) {
	bool val = get_bsp_board_led_state(led_idx);
	if (val)
		off_bsp_board_led(led_idx);
	else
		on_bsp_board_led(led_idx);
}

static void init_bsp_board_leds(void) {
	core_err ret = CORE_FAIL;
	uint32_t i;
	for (i = 0; i < LEDS_NUMBER; ++i) {
		// Set GPIO pad for IO.
		gpio_pad_select_gpio(m_board_led_list[i]);

		// Set GPIO pad for IO. direction
		ret = gpio_set_direction(m_board_led_list[i], GPIO_MODE_OUTPUT);
		if (ret != CORE_OK) {
			// TODO: handle errors
			CORE_LOGE(TAG, "gpio_set_direction failed for pin no. %d", m_board_led_list[i]);
		}
	}
	off_bsp_board_leds();
}

uint32_t led_idx_to_pin_bsp_board(uint32_t led_idx) {
	return m_board_led_list[led_idx];
}

uint32_t pin_to_led_idx_bsp_board(uint32_t pin_number) {
	uint32_t ret = 0xFFFFFFFF;
	uint32_t i;
	for (i = 0; i < LEDS_NUMBER; ++i) {
		if (m_board_led_list[i] == pin_number) {
			ret = i;
			break;
		}
	}
	return ret;
}
#endif //LEDS_NUMBER > 0

void init_platform_leds(uint32_t init_flags) {

#if LEDS_NUMBER > 0
	if (init_flags & BSP_INIT_LEDS) {
		init_bsp_board_leds();
	}
#endif //LEDS_NUMBER > 0
}
