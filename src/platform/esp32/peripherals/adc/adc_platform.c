/*
 * File Name: adc_platform.c
 * File Path: /emmate/src/platform/esp32/peripherals/adc/adc_platform.c
 * Description:
 *
 *  Created on: 28-May-2019
 *      Author: Noyel Seth
 */


#include "adc_platform.h"
#include "adc_platform_channel.h"

#include <stdbool.h>
#include <string.h>


/*
 *
 */
bool select_adc_channel(uint8_t adc_channel_gpio, adc_pf_unit_t *unit_t, adc_pf_channel_t* channel_t){

	bool ret=true;
	adc_pf_channel_t channel = -1;
	adc_pf_unit_t unit=-1;
	switch (adc_channel_gpio) {
/**************************************************** ADC 1 Channel ***********************************/
#if ADC_1_PF_CHANNEL_0_GPIO != UNASSIGN_PIN
		case ADC_1_PF_CHANNEL_0_GPIO: {
			channel = ADC1_PF_CHANNEL_0;
			unit = ADC_PF_UNIT_1;
			break;
		}
#endif

//		case ADC_1_PF_CHANNEL_1_GPIO: {
//			channel = ADC1_PF_CHANNEL_1;
//			unit = ADC_PF_UNIT_1;
//			break;
//		}
//
//		case ADC_1_PF_CHANNEL_2_GPIO: {
//			channel = ADC1_PF_CHANNEL_2;
//			unit = ADC_PF_UNIT_1;
//			break;
//		}

#if ADC_1_PF_CHANNEL_3_GPIO != UNASSIGN_PIN
		case ADC_1_PF_CHANNEL_3_GPIO: {
			channel = ADC1_PF_CHANNEL_3;
			unit = ADC_PF_UNIT_1;
			break;
		}
#endif

#if ADC_1_PF_CHANNEL_4_GPIO != UNASSIGN_PIN
		case ADC_1_PF_CHANNEL_4_GPIO: {
			channel = ADC1_PF_CHANNEL_4;
			unit = ADC_PF_UNIT_1;
			break;
		}
#endif

#if ADC_1_PF_CHANNEL_5_GPIO != UNASSIGN_PIN
		case ADC_1_PF_CHANNEL_5_GPIO: {
			channel = ADC1_PF_CHANNEL_5;
			unit = ADC_PF_UNIT_1;
			break;
		}
#endif

#if ADC_1_PF_CHANNEL_6_GPIO != UNASSIGN_PIN
		case ADC_1_PF_CHANNEL_6_GPIO: {
			channel = ADC1_PF_CHANNEL_6;
			unit = ADC_PF_UNIT_1;
			break;
		}
#endif

#if ADC_1_PF_CHANNEL_7_GPIO != UNASSIGN_PIN
		case ADC_1_PF_CHANNEL_7_GPIO: {
			channel = ADC1_PF_CHANNEL_7;
			unit = ADC_PF_UNIT_1;
			break;
		}
#endif


/****************************************************  ADC 2 Channel ***********************************/

#if ADC_2_PF_CHANNEL_0_GPIO != UNASSIGN_PIN
		case ADC_2_PF_CHANNEL_0_GPIO: {
			channel = ADC2_PF_CHANNEL_0;
			unit = ADC_PF_UNIT_2;
			break;
		}
#endif

#if ADC_2_PF_CHANNEL_1_GPIO != UNASSIGN_PIN
		case ADC_2_PF_CHANNEL_1_GPIO: {
			channel = ADC2_PF_CHANNEL_1;
			unit = ADC_PF_UNIT_2;
			break;
		}
#endif

#if ADC_2_PF_CHANNEL_2_GPIO != UNASSIGN_PIN
		case ADC_2_PF_CHANNEL_2_GPIO: {
			channel = ADC2_PF_CHANNEL_2;
			unit = ADC_PF_UNIT_2;
			break;
		}
#endif

#if ADC_2_PF_CHANNEL_3_GPIO != UNASSIGN_PIN
		case ADC_2_PF_CHANNEL_3_GPIO: {
			channel = ADC2_PF_CHANNEL_3;
			unit = ADC_PF_UNIT_2;
			break;
		}
#endif

#if ADC_2_PF_CHANNEL_4_GPIO != UNASSIGN_PIN
		case ADC_2_PF_CHANNEL_4_GPIO: {
			channel = ADC2_PF_CHANNEL_4;
			unit = ADC_PF_UNIT_2;
			break;
		}
#endif

#if ADC_2_PF_CHANNEL_5_GPIO != UNASSIGN_PIN
		case ADC_2_PF_CHANNEL_5_GPIO: {
			channel = ADC2_PF_CHANNEL_5;
			unit = ADC_PF_UNIT_2;
			break;
		}
#endif

#if ADC_2_PF_CHANNEL_6_GPIO != UNASSIGN_PIN
		case ADC_2_PF_CHANNEL_6_GPIO: {
			channel = ADC2_PF_CHANNEL_6;
			unit = ADC_PF_UNIT_2;
			break;
		}
#endif

//		case ADC_2_PF_CHANNEL_7_GPIO: {
//			channel = ADC2_PF_CHANNEL_7;
//			unit = ADC_PF_UNIT_2;
//			break;
//		}
//
//		case ADC_2_PF_CHANNEL_8_GPIO: {
//			channel = ADC2_PF_CHANNEL_8;
//			unit = ADC_PF_UNIT_2;
//			break;
//		}
//
//		case ADC_2_PF_CHANNEL_9_GPIO: {
//			channel = ADC2_PF_CHANNEL_9;
//			unit = ADC_PF_UNIT_2;
//			break;
//		}

		default:{
			ret = false;
			break;
		}
	}

	if(channel_t != NULL){
		*channel_t = channel;
	}
	if(unit_t != NULL){
		*unit_t = unit;
	}

	return ret;
}




/*
 *
 */
bool validate_adc_channel(uint8_t adc_channel_gpio){
	bool ret = select_adc_channel(adc_channel_gpio, NULL, NULL);
	return ret;
}
