
/*
 * platform_info.c
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */
#include <string.h>
#include "system_info.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "esp_spi_flash.h"

#define TAG	LTAG_SYSTEM_SYSINFO

static void read_save_hardware_info(HardwareInfo *hw_info) {
	esp_chip_info_t chip_info;
	esp_chip_info(&chip_info);

	strcpy(hw_info->chip_name, "ESP32");
	strcpy(hw_info->chip_mfgr, "ESSPRESIF");
	hw_info->has_wifi = true;
	hw_info->has_bt = (chip_info.features & CHIP_FEATURE_BT) ? true : false;
	hw_info->has_ble = (chip_info.features & CHIP_FEATURE_BLE) ? true : false;
	hw_info->has_gsm_2g = false;
	hw_info->has_gsm_3g = false;
	hw_info->has_gsm_4g = false;
	hw_info->has_lora = false;
	hw_info->has_nbiot = false;
	hw_info->flash_size = spi_flash_get_chip_size();	// TODO: spi_flash_get_chip_size() to be called through core api
	hw_info->ram_size = (520 * 1024);
}

static void read_save_sdk_info(SDKInfo *sdk_info) {
	strcpy(sdk_info->sdk_name, "ESP-IDF");
	strcpy(sdk_info->sdk_version, CONFIG_PLATFORM_IDF_VERSION);
}

void read_save_platform_info(PlatformInfo *pl_info)
{
	read_save_hardware_info(&pl_info->hw_info);
	read_save_sdk_info(&pl_info->sdk_info);

//	esp_chip_info(&pl_info->hw_info.chip_info);
}

//void read_framework_info()
//{
//
//}

void print_system_specific_info(PlatformInfo *pl_info)
{
	CORE_LOGW(TAG, "-----------------------------");
	CORE_LOGW(TAG, "Hardware Information");
	CORE_LOGW(TAG, "-----------------------------");
	CORE_LOGW(TAG,"Chip Name: %s", pl_info->hw_info.chip_name);
	CORE_LOGW(TAG,"Chip Manufacturer: %s", pl_info->hw_info.chip_mfgr);
	CORE_LOGW(TAG, "Features:");
	CORE_LOGW(TAG, "\tWIFI : %s", pl_info->hw_info.has_wifi ? "YES" : "NO");
	CORE_LOGW(TAG, "\tBT : %s", pl_info->hw_info.has_bt ? "YES" : "NO");
	CORE_LOGW(TAG, "\tBLE : %s", pl_info->hw_info.has_ble ? "YES" : "NO");
	CORE_LOGW(TAG, "\tGSM 2G : %s", pl_info->hw_info.has_gsm_2g ? "YES" : "NO");
	CORE_LOGW(TAG, "\tGSM 3G : %s", pl_info->hw_info.has_gsm_3g ? "YES" : "NO");
	CORE_LOGW(TAG, "\tGSM 4G : %s", pl_info->hw_info.has_gsm_4g ? "YES" : "NO");
	CORE_LOGW(TAG, "\tLORA : %s", pl_info->hw_info.has_lora ? "YES" : "NO");
	CORE_LOGW(TAG, "\tNBIOT : %s", pl_info->hw_info.has_nbiot ? "YES" : "NO");
	CORE_LOGW(TAG, "FLASH SIZE : %d bytes", pl_info->hw_info.flash_size);
	CORE_LOGW(TAG, "RAM SIZE : %d bytes", pl_info->hw_info.ram_size);
	CORE_LOGW(TAG, "-----------------------------");
	CORE_LOGW(TAG, "SDK Information");
	CORE_LOGW(TAG, "-----------------------------");
	CORE_LOGW(TAG, "Running SDK : %s", pl_info->sdk_info.sdk_name);
	CORE_LOGW(TAG, "SDk Version : %s", pl_info->sdk_info.sdk_version);

//	CORE_LOGW(TAG,"CPU cores: %d, WiFi%s%s, ", pl_info->hw_info.chip_info.cores, (pl_info->hw_info.chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "", (pl_info->hw_info.chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");
//	CORE_LOGW(TAG,"Silicon Revision: %d, ", pl_info->hw_info.chip_info.revision);
//	// TODO: spi_flash_get_chip_size() to be called through core api
//	CORE_LOGW(TAG,"%dMB %s flash", spi_flash_get_chip_size() / (1024 * 1024), (pl_info->hw_info.chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");
}
