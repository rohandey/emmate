/*
 * File Name: clcd_core.c
 * 
 * Description:
 *
 *  Created on: 02-Sep-2019
 *      Author: Noyel Seth
 */

#include "core_config.h"
#include "clcd_core.h"
#include "gpio_helper_api.h"
#include "delay_utils.h"
#include <string.h>

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG "clcd_core"

#define CLCD_4BIT_MODE		CONFIG_CLCD_4BIT_MODE_ENABLE

#define CLCD_INIT_DONE_MSG		"Init_Done.."

#define CMD_MODE 0
#define DATA_MODE 1

#define LCD_WRITE_MODE  0
#define LCD_READ_MODE  1

//#define EIGHT_BIT_TWO_LINE_DISPLAY		write_lcd_cmd(TWO_LINE_EIGHT_BIT_DISPLAY_CMD);
//#define ELEVEN_BIT_TWO_LINE_DISPLAY		write_lcd_cmd(TWO_LINE_ELEVEN_BIT_DISPLAY_CMD);
//#define DISPLAY_ON_CURSOR_ON			write_lcd_cmd(DISPLAY_ON_CURSOR_ON_CMD);
//#define CLEAR_DISPLAY					write_lcd_cmd(CLEAR_DISPLAY_CMD);
//#define ENTRY_MODE						write_lcd_cmd(INCREMET_CURSOR_CMD);

#define LCD_STROBE() {set_gpio_value(LCD_EN, HIGH); \
					delay_us(10);\
					set_gpio_value(LCD_EN, LOW); \
					delay_us(100); }  // commands need > 37us to settle

#if CLCD_4BIT_MODE
void default_clcd_function_set(unsigned char default_cmd) {

	// this is according to the hitachi HD44780 datasheet
	// figure 24, pg 46
	unsigned char cmd = default_cmd;

	set_gpio_value(LCD_D7, ((cmd >> 3) & 0x01));
	set_gpio_value(LCD_D6, ((cmd >> 2) & 0x01));
	set_gpio_value(LCD_D5, ((cmd >> 1) & 0x01));
	set_gpio_value(LCD_D4, ((cmd >> 0) & 0x01));

	set_gpio_value(LCD_RS, CMD_MODE);
	set_gpio_value(LCD_RW, LCD_WRITE_MODE);

	LCD_STROBE()
	;
}

#else

void default_clcd_function_set(unsigned char default_cmd) {
	// this is according to the hitachi HD44780 datasheet
	// figure 23, pg 45
	unsigned char cmd = default_cmd << 4;
	set_gpio_value(LCD_D7, ((cmd >> 7) & 0x01));
	set_gpio_value(LCD_D6, ((cmd >> 6) & 0x01));
	set_gpio_value(LCD_D5, ((cmd >> 5) & 0x01));
	set_gpio_value(LCD_D4, ((cmd >> 4) & 0x01));

	set_gpio_value(LCD_D3, ((cmd >> 3) & 0x01));
	set_gpio_value(LCD_D2, ((cmd >> 2) & 0x01));
	set_gpio_value(LCD_D1, ((cmd >> 1) & 0x01));
	set_gpio_value(LCD_D0, ((cmd >> 0) & 0x01));

	set_gpio_value(LCD_RS, CMD_MODE);
	set_gpio_value(LCD_RW, LCD_WRITE_MODE);

	LCD_STROBE()
	;
}

#endif

void is_busy(void) {
//#if CLCD_4BIT_MODE
	//delay_us(100);
//#else
	//LCD_D7_RC5_SetDigitalInput();
	set_gpio_value(LCD_RS, CMD_MODE);
	set_gpio_value(LCD_RW, LCD_READ_MODE);

	delay_us(10);
	do {
		LCD_STROBE()
		;
	}while (get_gpio_value(LCD_BUSY) == HIGH);

	set_gpio_value(LCD_RW, LCD_WRITE_MODE);
	delay_us(10);
	//printf("%s\n", __func__);
	//LCD_D7_RC5_SetDigitalOutput();
//#endif
}

void write_lcd_cmd(unsigned char cmd) {
#if CLCD_4BIT_MODE

	unsigned char cmd_higher_nibble = ((cmd >> 4) & 0x0F);
	unsigned char cmd_lower_nibble = (cmd & 0x0F);

	set_gpio_value(LCD_D7, ((cmd_higher_nibble >> 3) & 0x01));
	set_gpio_value(LCD_D6, ((cmd_higher_nibble >> 2) & 0x01));
	set_gpio_value(LCD_D5, ((cmd_higher_nibble >> 1) & 0x01));
	set_gpio_value(LCD_D4, ((cmd_higher_nibble >> 0) & 0x01));

	set_gpio_value(LCD_RS, CMD_MODE);
	set_gpio_value(LCD_RW, LCD_WRITE_MODE);

	LCD_STROBE()
	;

	//delay_us(100);

	set_gpio_value(LCD_D7, ((cmd_lower_nibble >> 3) & 0x01));
	set_gpio_value(LCD_D6, ((cmd_lower_nibble >> 2) & 0x01));
	set_gpio_value(LCD_D5, ((cmd_lower_nibble >> 1) & 0x01));
	set_gpio_value(LCD_D4, ((cmd_lower_nibble >> 0) & 0x01));

	set_gpio_value(LCD_RS, CMD_MODE);
	set_gpio_value(LCD_RW, LCD_WRITE_MODE);

	LCD_STROBE()
	;

#else

	set_gpio_value(LCD_D7, ((cmd >> 7) & 0x01));
	set_gpio_value(LCD_D6, ((cmd >> 6) & 0x01));
	set_gpio_value(LCD_D5, ((cmd >> 5) & 0x01));
	set_gpio_value(LCD_D4, ((cmd >> 4) & 0x01));

	set_gpio_value(LCD_D3, ((cmd >> 3) & 0x01));
	set_gpio_value(LCD_D2, ((cmd >> 2) & 0x01));
	set_gpio_value(LCD_D1, ((cmd >> 1) & 0x01));
	set_gpio_value(LCD_D0, ((cmd >> 0) & 0x01));

	set_gpio_value(LCD_RS, CMD_MODE);
	set_gpio_value(LCD_RW, LCD_WRITE_MODE);

	LCD_STROBE()
	;
#endif
}

void write_lcd_data(unsigned char data) {
#if CLCD_4BIT_MODE

	unsigned char data_higher_nibble = ((data >> 4) & 0x0F);
	unsigned char data_lower_nibble = (data & 0x0F);

	set_gpio_value(LCD_D7, ((data_higher_nibble >> 3) & 0x01));
	set_gpio_value(LCD_D6, ((data_higher_nibble >> 2) & 0x01));
	set_gpio_value(LCD_D5, ((data_higher_nibble >> 1) & 0x01));
	set_gpio_value(LCD_D4, ((data_higher_nibble >> 0) & 0x01));

	set_gpio_value(LCD_RS, DATA_MODE);
	set_gpio_value(LCD_RW, LCD_WRITE_MODE);

	LCD_STROBE()
	;

	//delay_us(100);

	set_gpio_value(LCD_D7, ((data_lower_nibble >> 3) & 0x01));
	set_gpio_value(LCD_D6, ((data_lower_nibble >> 2) & 0x01));
	set_gpio_value(LCD_D5, ((data_lower_nibble >> 1) & 0x01));
	set_gpio_value(LCD_D4, ((data_lower_nibble >> 0) & 0x01));

	set_gpio_value(LCD_RS, DATA_MODE);
	set_gpio_value(LCD_RW, LCD_WRITE_MODE);

	LCD_STROBE()
	;
#else
	set_gpio_value(LCD_D7, ((data >> 7) & 0x01));
	set_gpio_value(LCD_D6, ((data >> 6) & 0x01));
	set_gpio_value(LCD_D5, ((data >> 5) & 0x01));
	set_gpio_value(LCD_D4, ((data >> 4) & 0x01));

	set_gpio_value(LCD_D3, ((data >> 3) & 0x01));
	set_gpio_value(LCD_D2, ((data >> 2) & 0x01));
	set_gpio_value(LCD_D1, ((data >> 1) & 0x01));
	set_gpio_value(LCD_D0, ((data >> 0) & 0x01));

	set_gpio_value(LCD_RS, DATA_MODE);
	set_gpio_value(LCD_RW, LCD_WRITE_MODE);

	LCD_STROBE()
	;
#endif
}

void write_str(char* str) {
	char i = 0;
	char len = strlen(str);
	while (i < len) {
		write_lcd_data(*str++);
		is_busy();
		i++;
	}
}

core_err init_clcd_core(void) {
	core_err ret = CORE_FAIL;

#if CLCD_4BIT_MODE

	ret = configure_gpio(LCD_D7, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

	ret = configure_gpio(LCD_D6, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

	ret = configure_gpio(LCD_D5, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

	ret = configure_gpio(LCD_D4, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

#else

	// Configure High-Nibble DataBus GPIO

	ret = configure_gpio(LCD_D7, GPIO_IO_MODE_INPUT_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

	ret = configure_gpio(LCD_D6, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

	ret = configure_gpio(LCD_D5, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

	ret = configure_gpio(LCD_D4, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

	// -----------------------------------------------------------------------------------------
	// Configure Lower-Nibble DataBus GPIO

	ret = configure_gpio(LCD_D3, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

	ret = configure_gpio(LCD_D2, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

	ret = configure_gpio(LCD_D1, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

	ret = configure_gpio(LCD_D0, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

#endif

	// Configure controlBus GPIO
	ret = configure_gpio(LCD_RS, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}
	ret = configure_gpio(LCD_RW, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}
	ret = configure_gpio(LCD_EN, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

	set_gpio_value(LCD_EN, LOW);
	delay_ms(250);

	default_clcd_function_set(0x03);
	delay_us(4500);	// Wait for more than 4.1 ms

	default_clcd_function_set(0x03);
	delay_us(4500);	// Wait for more than 4.1 ms

	// third go!
	default_clcd_function_set(0x03);
	delay_us(150);	// Wait for more than 100 us

#if CLCD_4BIT_MODE
	default_clcd_function_set(0x02);
	//delay_us(10);
	//FOUR_BIT_TWO_LINE_DISPLAY;
	write_lcd_cmd(TWO_LINE_FOUR_BIT_DISPLAY_CMD);
#else
	//EIGHT_BIT_TWO_LINE_DISPLAY;
	write_lcd_cmd(TWO_LINE_EIGHT_BIT_DISPLAY_CMD);
#endif
	delay_ms(250);

	//write_lcd_cmd(DISPLAY_OFF_CURSOR_OFF_CMD);
	write_lcd_cmd(DISPLAY_ON_CURSOR_ON_CMD);
	//DISPLAY_ON_CURSOR_ON;
	delay_ms(250);

	write_lcd_cmd(CLEAR_DISPLAY_CMD);
	//CLEAR_DISPLAY;
	delay_ms(250);

	write_lcd_cmd(INCREMET_CURSOR_CMD);
	//ENTRY_MODE;
	delay_ms(250);

	write_lcd_cmd(DISPLAY_ON_CURSOR_OFF_CMD);
	//write_lcd_cmd(DISPLAY_ON_CURSOR_BLINK_CMD);
	delay_ms(250);

	//printf("init complete\n");
	write_str(CLCD_INIT_DONE_MSG);
	delay_ms(500);
	//delay_ms(2000);
	write_lcd_cmd(CLEAR_DISPLAY_CMD);
	is_busy();
	write_lcd_cmd(RETURN_HOME_CMD);
	is_busy();
	//write_lcd_cmd(DISPLAY_ON_CURSOR_OFF_CMD);
	//is_busy();
	CORE_LOGI(TAG, "CLCD Core init done\n");
	return ret;

}
