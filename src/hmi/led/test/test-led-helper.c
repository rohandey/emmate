/*
 * test-led-helper.c
 *
 *  Created on: 24-Aug-2019
 *      Author: Rohan Dey
 */

#include "test-led-helper.h"

#include "led_helper.h"
#include "led_patterns.h"
#include "threading.h"
#include "thing.h"

#define TAG	"test-led-helper"

static void show_testapp_notification_type6() {
	core_err ret = CORE_FAIL;
	int num_leds = 1;

	ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_led_notification failed!");
		return;
	}

	ret = set_led_type_idx(1, MONO_LED, SYSTEM_HMI_LED_BLUE, 0, 0);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "set_led_type_idx failed!");
		return;
	}

	ret = map_patterns_to_led(1, 1, LED_MONO_1000_1_1);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "map_patterns_to_led failed!");
		return;
	}

	ret = start_led_notification();
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "start_led_notification failed!");
		return;
	}

	CORE_LOGI(TAG, "show_testapp_notification_type5 started successfully ...");
}

static void show_testapp_notification_type5() {
	core_err ret = CORE_FAIL;
	int num_leds = 1;

	ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_led_notification failed!");
		return;
	}

	ret = set_led_type_idx(1, MONO_LED, SYSTEM_HMI_LED_GREEN, 0, 0);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "set_led_type_idx failed!");
		return;
	}

	ret = map_patterns_to_led(1, 1, LED_MONO_1000_1_1);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "map_patterns_to_led failed!");
		return;
	}

	ret = start_led_notification();
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "start_led_notification failed!");
		return;
	}

	CORE_LOGI(TAG, "show_testapp_notification_type6 started successfully ...");
}

static void show_testapp_notification_type4() {
	core_err ret = CORE_FAIL;
	int num_leds = 1;

	ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_led_notification failed!");
		return;
	}

	ret = set_led_type_idx(1, MONO_LED, SYSTEM_HMI_LED_MONO_RED, 0, 0);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "set_led_type_idx failed!");
		return;
	}

	ret = map_patterns_to_led(1, 1, LED_MONO_1000_1_1);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "map_patterns_to_led failed!");
		return;
	}

	ret = start_led_notification();
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "start_led_notification failed!");
		return;
	}

	CORE_LOGI(TAG, "show_testapp_notification_type4 started successfully ...");
}

static void show_testapp_notification_type3() {
	core_err ret = CORE_FAIL;
	int num_leds = 1;

//	ret = init_led_notification(0, NOTIFY_TIME_FOREVER);
	ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_led_notification failed!");
		return;
	}

//	ret = set_led_type_idx(0, RGB_LED, SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE);
//	ret = set_led_type_idx(1, 10, SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE);
	ret = set_led_type_idx(1, RGB_LED, SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "set_led_type_idx failed!");
		return;
	}

//	ret = map_patterns_to_led(0, 1, LED_RED_1000_1_1);
//	ret = map_patterns_to_led(1, 0, LED_RED_1000_1_1);
//	ret = map_patterns_to_led(1, 1, LED_RED_1000_1_1, LED_GREEN_1000_1_1);
//	ret = map_patterns_to_led(1, 2, LED_RED_1000_1_1);	// Program will crash
	ret = map_patterns_to_led(1, 1, LED_RED_1000_1_1);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "map_patterns_to_led failed!");
		return;
	}

	ret = start_led_notification();
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "start_led_notification failed!");
		return;
	}

	CORE_LOGI(TAG, "show_testapp_notification_type3 started successfully ...");
}

static void show_testapp_notification_type2() {
	core_err ret = CORE_FAIL;
	int num_leds = 3;

	ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_led_notification failed!");
		return;
	}

	ret = set_led_type_idx(1, MONO_LED, SYSTEM_HMI_LED_MONO_RED, 0, 0);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "set_led_type_idx failed!");
		return;
	}
	ret = set_led_type_idx(2, MONO_LED, SYSTEM_HMI_LED_GREEN, 0, 0);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "set_led_type_idx failed!");
		return;
	}
	ret = set_led_type_idx(3, MONO_LED, SYSTEM_HMI_LED_BLUE, 0, 0);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "set_led_type_idx failed!");
		return;
	}

	ret = map_patterns_to_led(1, 2, LED_MONO_100_100_1, LED_MONO_500_500_1);
	ret = map_patterns_to_led(2, 1, LED_MONO_250_250_1);
	ret = map_patterns_to_led(3, 3, LED_MONO_1000_1_1, LED_MONO_1_1000_1, LED_MONO_100_100_2);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "map_patterns_to_led failed!");
		return;
	}

	ret = start_led_notification();
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "start_led_notification failed!");
		return;
	}

	CORE_LOGI(TAG, "show_testapp_notification_type2 started successfully ...");
}

static void show_testapp_notification_type1(int option) {
	core_err ret = CORE_FAIL;
	int num_leds = 1;

	ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_led_notification failed!");
		return;
	}

	ret = set_led_type_idx(num_leds, RGB_LED, SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "set_led_type_idx failed!");
		return;
	}

	switch (option) {
	case 1:
		ret = map_patterns_to_led(num_leds, 1, LED_RED_1000_1_1);
		break;
	case 2:
		ret = map_patterns_to_led(num_leds, 1, LED_GREEN_1000_1_1);
		break;
	case 3:
		ret = map_patterns_to_led(num_leds, 1, LED_BLUE_1000_1_1);
		break;
	case 4:
		ret = map_patterns_to_led(num_leds, 1, LED_WHITE_1000_1_1);
		break;
	}

	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "map_patterns_to_led failed!");
		return;
	}

	ret = start_led_notification();
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "start_led_notification failed!");
		return;
	}

	CORE_LOGI(TAG, "show_testapp_notification_type1 started successfully ...");
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static void notification_task1(void * params) {
#define TAG	"notification_task1"
	while (1) {
		CORE_LOGI(TAG, "===========================");
		CORE_LOGI(TAG, "notification_task1");
		show_testapp_notification_type4();
		CORE_LOGI(TAG, "===========================");
		TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
	}
}
static void notification_task2(void * params) {
#define TAG	"notification_task2"
	while (1) {
		CORE_LOGI(TAG, "===========================");
		CORE_LOGI(TAG, "notification_task2");
		show_testapp_notification_type5();
		CORE_LOGI(TAG, "===========================");
		TaskDelay(DELAY_500_MSEC / TICK_RATE_TO_MS);
	}
}
static void notification_task3(void * params) {
#define TAG	"notification_task3"
	while (1) {
		CORE_LOGI(TAG, "===========================");
		CORE_LOGI(TAG, "notification_task3");
		show_testapp_notification_type6();
		CORE_LOGI(TAG, "===========================");
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void test_led_helper_start(void) {
	CORE_LOGI(TAG, "===========================");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG, "Starting Led Helper Test Program");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG, "===========================");

	int led_option = 0;
	int count = 0;

	TEST_CASES test_case = TEST_CASE_4;

	while (1) {

		switch (test_case) {
		case TEST_CASE_1: {
			if (++led_option > 4)
				led_option = 1;
			if (++count < 3) {
				CORE_LOGW(TAG, "Calling show_testapp_notification_type1 ... %d", led_option);
				show_testapp_notification_type1(led_option);
				CORE_LOGW(TAG, "Returned show_testapp_notification_type1 ... %d", led_option);
			} else if (count == 3) {
				CORE_LOGW(TAG, "Stopping LED Notification");
				stop_led_notification();
			}
		}
			break;

		case TEST_CASE_2: {
			CORE_LOGW(TAG, "Calling show_testapp_notification_type2 ...");
			show_testapp_notification_type2();
			CORE_LOGW(TAG, "Returned show_testapp_notification_type2 ...");
		}
			break;

		case TEST_CASE_3: {
			CORE_LOGW(TAG, "Calling show_testapp_notification_type3 ...");
			show_testapp_notification_type3();
			CORE_LOGW(TAG, "Returned show_testapp_notification_type3 ...");
		}
			break;

		case TEST_CASE_4: {
			TaskCreate(notification_task1, "notify-1", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_10, NULL);
			TaskCreate(notification_task2, "notify-2", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_10, NULL);
			TaskCreate(notification_task3, "notify-3", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_10, NULL);
			test_case = test_case + 100;
			break;
		}
		default:
			break;
		}

		TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
	}
}
