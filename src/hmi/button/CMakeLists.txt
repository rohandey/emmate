set(srcs
	button_helper.c
	button_platform.c
	)

add_library(button STATIC ${srcs})

# Private linkages
list(APPEND EXTRA_LIBS
					emmate_config
					${CMAKE_PROJECT_NAME}_inc
					)

if(CONFIG_USE_COMMON)
	list(APPEND EXTRA_LIBS common)
	if(CONFIG_USE_ERRORS)
		list(APPEND EXTRA_LIBS errors)
	endif()
	if(CONFIG_USE_DATA_STRUCTURES)
		list(APPEND EXTRA_LIBS data-structures)
	endif()
endif()

if(CONFIG_USE_LOGGING)
	list(APPEND EXTRA_LIBS logging)
endif()

if(CONFIG_USE_THREADING)
	list(APPEND EXTRA_LIBS threading)
endif()


# Public linkages
if(CONFIG_USE_PERIPHERALS AND CONFIG_USE_GPIO)
	list(APPEND EXTRA_PUBLIC_LIBS gpio)
endif()

# Link the libraries
target_link_libraries(button PRIVATE ${EXTRA_LIBS})
target_link_libraries(button PUBLIC ${EXTRA_PUBLIC_LIBS})

target_include_directories(button 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)
							