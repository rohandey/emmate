# Utils
## Overview
The utils module exposes utility functions that can be used by any application.

## How to use this module from an application

##### Header(s) to include

```
core_utils.h
delay_utils.h
```

#### Module Specific Configurations
---
**This module is a mandatory requirement for the framework, so it cannot be disabled from the Menuconfig Application**

---