/*
 * delay_utils.c
 *
 *  Created on: 29-Aug-2019
 *      Author: Rohan Dey
 */


#include "delay_utils.h"
#include "system_utils.h"
#include "threading.h"

void delay_ms(uint16_t ms_delay){

#if CONFIG_USE_THREADING
	TaskDelay(ms_delay/TICK_RATE_TO_MS);
#endif

}


void delay_us(uint16_t us_delay){
	core_us_delay(us_delay);
}

