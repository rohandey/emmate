/*
 * File Name: conn.h
 * File Path: /emmate/src/conn/conn.h
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef CONN_H_
#define CONN_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_error.h"
#include "core_constant.h"


#if CONFIG_USE_BLE
#include "ble.h"
#endif
#if CONFIG_USE_WIFI
#include "wifi.h"
#endif
#if IFACE_NBIOT
#endif
#if IFACE_GSM
#endif


//#if CONFIG_USE_DEVCONFIG
//#include "device_config.h"
//#endif
#include "core_interfaces.h"


typedef enum {
	NETCONNSTAT_CONNECTING = 0,
	NETCONNSTAT_CONNECTED,
	NETCONNSTAT_DISCONNECTED
} NETWORK_CONN_STATUS;

typedef struct {
	NETWORK_CONN_STATUS netstat;
} ConnData;

/**
 * @brief initialize the configured Network interface
 *
 * @param dev_cfg	pass DeviceConfig data to config the Network interface.
 *
 * @return
 * 		- CORE_OK 	on sucess
 *		- CORE_FAIL on fail
 **/
//core_err init_network(DeviceConfig *dev_cfg);


/**
 * @brief make a connection between network interface and the network
 *
 * @param dev_cfg	pass DeviceConfig data to connection network interface with the network
 *
 * @return
 * 		- CORE_OK 	on sucess
 *		- CORE_FAIL on fail
 **/
//core_err connect_to_network(DeviceConfig *dev_cfg);

/**
 * @brief 			Returns the network connection status.
 * 					Statuses can be found in the enum NETWORK_CONN_STATUS
 *
 * @return
 * 			NETWORK_CONN_STATUS
 **/
NETWORK_CONN_STATUS get_network_conn_status();

/**
 * @brief 			Starts the network connection thread
 *
 * @param dev_cfg	pass DeviceConfig data for connecting to appropriate network
 *
 * @return
 * 		- CORE_OK 	on sucess
 *		- CORE_FAIL on fail
 **/
//core_err start_network_connection(DeviceConfig *dev_cfg);
core_err start_network_connection();

#ifdef __cplusplus
}
#endif

#endif /* CONN_H_ */
