/*
 * File Name: netifinfo.c
 * File Path: /emmate/src/conn/netifinfo.c
 * Description:
 *
 *  Created on: 30-Mar-2020
 *      Author: Noyel Seth
 */


#include "netifinfo.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include <string.h>


#if CONFIG_USE_WIFI
#include "persistent_mem_wifi.h"
#endif

#if CONFIG_USE_BLE
#include "persistent_mem_ble.h"
#endif

#define TAG LTAG_NETIF_INFO

NetIFInfo netifinfo;

#if CONFIG_USE_WIFI
/**
 *
 */
static void set_wifi_info(void* ifinfo){
	CORE_LOGD(TAG, "Execute %s", __func__);

	if(ifinfo != NULL){
		memcpy(&netifinfo.wifi_info, ifinfo, sizeof(netifinfo.wifi_info));
		core_err res = write_wifi_mac_to_persistent_mem(netifinfo.wifi_info.wifi_mac);
		if(res != CORE_OK){
			CORE_LOGE(TAG, "Failed to write WiFi MAC Address into Persistent Memory");
		}
	}
}
#endif


#if CONFIG_USE_BLE

/**
 *
 */
static void set_ble_info(void* ifinfo){
	CORE_LOGD(TAG, "Execute %s", __func__);

	if (ifinfo != NULL) {
		memcpy(&netifinfo.ble_info, ifinfo, sizeof(netifinfo.ble_info));
		core_err res = write_ble_mac_to_persistent_mem(netifinfo.ble_info.ble_mac);
		if (res != CORE_OK) {
			CORE_LOGE(TAG, "Failed to write BLE MAC Address into Persistent Memory");
		}
	}


}

#endif



/**
 *
 */
core_err netifinfo_init(){

	core_err res ;
#if CONFIG_USE_WIFI
	res = read_wifi_mac_from_persistent_mem(netifinfo.wifi_info.wifi_mac);
	register_wifi_info_recv_cb_handler(set_wifi_info);
#endif

#if CONFIG_USE_BLE
	res = read_ble_mac_from_persistent_mem(netifinfo.ble_info.ble_mac);
	register_ble_info_recv_cb_handler(set_ble_info);
#endif

	return CORE_OK;
}


/**
 *
 */
void get_netif_info(void* ifinfo){

	memcpy(ifinfo, &netifinfo, sizeof(netifinfo));

}

