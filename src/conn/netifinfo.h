/*
 * File Name: netifinfo.h
 * File Path: /emmate/src/conn/netifinfo.h
 * Description:
 *
 *  Created on: 30-Mar-2020
 *      Author: Noyel Seth
 */

#ifndef NETIFINFO_H_
#define NETIFINFO_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"


#if CONFIG_USE_BLE
#include "ble.h"
#endif

#if CONFIG_USE_WIFI
#include "wifi.h"
#endif

#if CONFIG_USE_ETH
#endif

#if CONFIG_USE_NBIOT
#endif

#if CONFIG_USE_GSM
#endif



typedef struct{

#if CONFIG_USE_WIFI
	WifiInfo wifi_info;
#endif

#if CONFIG_USE_BLE
	BLEInfo ble_info;
#endif

}NetIFInfo;



/**
 * @brief     Get the Wi-Fi Network Information (Connected SSID, MAC, IP)
 *
 * @param[out]  ifinfo  Network interface's information returned as an out parameter.
 *
 */
void get_netif_info(void* ifinfo);

/**
 *
 */
core_err netifinfo_init();



#ifdef __cplusplus
}
#endif


#endif /* NETIFINFO_H_ */
