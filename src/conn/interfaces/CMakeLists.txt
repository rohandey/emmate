if(CONFIG_USE_BLE)
	add_subdirectory(ble)
endif()

if(CONFIG_USE_ETH)
	add_subdirectory(eth)
endif()

if(CONFIG_USE_GSM)
	add_subdirectory(gsm)
endif()

if(CONFIG_USE_WIFI)
	add_subdirectory(wifi)
endif()

set(srcs
	nw_interface_platform.c
	)

add_library(interfaces STATIC ${srcs})

list(APPEND EXTRA_LIBS emmate_config)
					
if(CONFIG_USE_COMMON)
	list(APPEND EXTRA_LIBS common)
	if(CONFIG_USE_ERRORS)
		list(APPEND EXTRA_LIBS errors)
	endif()
endif()

if(CONFIG_USE_LOGGING)
	list(APPEND EXTRA_LIBS logging)
endif()

target_link_libraries(interfaces PRIVATE ${EXTRA_LIBS})

target_include_directories(interfaces 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)