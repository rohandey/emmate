/*
 * File Name: gsm_constants.h
 * File Path: /emmate/src/conn/interfaces/gsm/gsm_constants.h
 * Description:
 *
 *  Created on: 24-May-2019
 *      Author: Rohan Dey
 */

#ifndef GSM_CONSTANTS_H_
#define GSM_CONSTANTS_H_

#define GSM_IMEI_LEN			16		/**< Length in bytes of a GSM module's IMEI number */

#endif /* GSM_CONSTANTS_H_ */
