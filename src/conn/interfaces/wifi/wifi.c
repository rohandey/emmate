/*
 * File Name: wifi.c
 * File Path: /emmate/src/conn/wifi/wifi.c
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */

#include "wifi.h"
#include "wifi_platform.h"
#include "system_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "persistent_mem.h"
#include "core_interfaces.h"
#include "threading.h"
#include <string.h>

#define TAG LTAG_CONN_WIFI

static WiFiCredentials m_wifi_cred;
static WiFiIFInfoRecvHandler wifi_info_hdl=NULL;
static WifiInfo wifi_info;
static int m_curr_config_idx = 0;
static int m_retry = 0;

static WIFI_CONNECTION_STATUS wifi_connection_stat = WIFI_NOT_CONNECTED;

core_err register_wifi_info_recv_cb_handler(WiFiIFInfoRecvHandler handler) {
	if (handler != NULL) {
		wifi_info_hdl = handler;
		return CORE_OK;
	}
	return CORE_FAIL;
}


char * get_wifi_connected_ssid() {
	if (m_curr_config_idx < 0) {
		return m_wifi_cred.cfg[0].wifi_ssid;
	} else {
		return m_wifi_cred.cfg[m_curr_config_idx].wifi_ssid;
	}
}

void wifi_event_handler(uint8_t event_id) {
	switch (event_id) {
	case WIFI_EVENT_AP_START:
		break;

	case WIFI_EVENT_AP_STOP:
		break;

	case WIFI_EVENT_STA_START: {
		connect_wifi_platform();
		break;
	}

	case WIFI_EVENT_STA_STOP:
		break;

	case WIFI_EVENT_STA_GOT_IP: {

		CORE_LOGD(TAG, "WiFi Connected To:");
		CORE_LOGD(TAG, "-----------------------------------");
		if (m_curr_config_idx < 0) {
			CORE_LOGD(TAG, "[%d] SSID: %s", 0, get_wifi_connected_ssid());
			CORE_LOGD(TAG, "[%d] PASS: %s", 0, m_wifi_cred.cfg[0].wifi_pwd);
		} else {
			CORE_LOGD(TAG, "[%d] SSID: %s", m_curr_config_idx, get_wifi_connected_ssid());
			CORE_LOGD(TAG, "[%d] PASS: %s", m_curr_config_idx, m_wifi_cred.cfg[m_curr_config_idx].wifi_pwd);
		}

		bzero(&wifi_info, sizeof(wifi_info));
		sprintf(wifi_info.sta_mode.wifi_sta_ssid, "%s", get_wifi_connected_ssid());

		get_wifi_sta_ip(wifi_info.sta_mode.wifi_got_ip);

		get_wifi_sta_mac(wifi_info.wifi_mac);

		if(wifi_info_hdl!=NULL){
			wifi_info_hdl(&wifi_info);
		}

		/* CONN_GOT_IP_BIT event group bit set when WiFi got IP */
		if (get_system_evtgrp_hdl() != NULL) {
			event_group_set_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT);
			event_group_clear_bits(get_system_evtgrp_hdl(),	CONN_CONNECTING_BIT);
			event_group_clear_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_FAILED);
		}

		wifi_connection_stat = WIFI_CONNECTED;
		m_retry = 0;
		m_curr_config_idx = -1;
		break;
	}

	case WIFI_EVENT_AP_STACONNECTED:
		break;

	case WIFI_EVENT_AP_STADISCONNECTED:
		break;

	case WIFI_EVENT_STA_DISCONNECTED: {

		wifi_connection_stat = WIFI_NOT_CONNECTED;

		/* Got wi-fi disconnected event, try to reconnect again */

		/* If Maximum Level 2 retry limit is reached then inform the System to take further action*/
		if (m_curr_config_idx == (m_wifi_cred.config_count - 1)) {
			// TODO: Instead of reporting to system after connection failure, try the entire cycle for few more times.

			/* We have tried with all the available configurations but could not connect. now inform the system */
			CORE_LOGE(TAG,
					"Could not connect to the AP. Tried with all available configurations. Informing the system module to take further action.");
			if (get_system_evtgrp_hdl() != NULL) {
				event_group_set_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_FAILED);
				event_group_clear_bits(get_system_evtgrp_hdl(), CONN_CONNECTING_BIT);
				event_group_clear_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT);
			}
		} else {
			/* If Maximum Level 1 retry limit is reached try with another config from the credential list */
			if (++m_retry > CONFIG_WIFI_MAX_RETRY_PER_CONFIG) {

				CORE_LOGW(TAG, "Could not connect to the AP, tried %d times", m_retry);
				CORE_LOGW(TAG, "CONFIG_COUNT = %d, CURR_CONFIG_IDX = %d", m_wifi_cred.config_count, m_curr_config_idx);

				m_retry = 0;

				if ((m_wifi_cred.config_count >= 1) && (++m_curr_config_idx <= (m_wifi_cred.config_count - 1))) {

					CORE_LOGI(TAG, "Trying with the new configurations:");
					CORE_LOGI(TAG, "-----------------------------------");
					CORE_LOGI(TAG, "[%d] SSID: %s", m_curr_config_idx, m_wifi_cred.cfg[m_curr_config_idx].wifi_ssid);
					CORE_LOGI(TAG, "[%d] PASS: %s", m_curr_config_idx, m_wifi_cred.cfg[m_curr_config_idx].wifi_pwd);

					set_wifi_platform_configs(m_wifi_cred.cfg[m_curr_config_idx].wifi_ssid,
							m_wifi_cred.cfg[m_curr_config_idx].wifi_pwd, NW_IF_WIFI_STA);

					TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);

					connect_wifi_platform();
				}
			} else {
				connect_wifi_platform();

				wifi_connection_stat = WIFI_CONNECTING;

				/* Clear the CONN_GOT_IP_BIT bit as Wi-Fi is disconnected */
				if (get_system_evtgrp_hdl() != NULL) {
					event_group_set_bits(get_system_evtgrp_hdl(), CONN_CONNECTING_BIT);
					event_group_clear_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT);
				}

				CORE_LOGI(TAG, "retry to connect to the AP");

//			// TODO: Quick Fix done. Need to change this
//			CORE_LOGE(TAG, "maximum connection retry limit reached! rebooting ...\n");
//			s_retry_num = 0;
//			core_system_restart();
			}
		}

		break;
	}
	default:
		break;
	}
}

//core_err init_wifi_sta_mode(WifiConfig *wifi_cfg) {
core_err init_wifi_sta_mode() {
	core_err ret = CORE_FAIL;

	/* The Wi-Fi Credentials must already be stored in memory, so read them first */
	read_wifi_credentials();

	if (m_wifi_cred.config_count > 0) {
		/* Initialize the wifi driver with the 1st config and see if it can connect */
		m_curr_config_idx = -1;
		ret = init_wifi_platform(WIFI_CORE_MODE_STA, wifi_event_handler, m_wifi_cred.cfg[0].wifi_ssid,
				m_wifi_cred.cfg[0].wifi_pwd);
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "init_wifi_platform failed: err = %d", ret);
			return CORE_FAIL;
		}
	} else {
		CORE_LOGE(TAG, "No WiFi Credentials stored!");
		return CORE_FAIL;
	}
	return ret;
}

void reconnect_wifi() {
	m_curr_config_idx = -1;
	connect_wifi_platform();
}

core_err connect_wifi() {
	core_err ret = CORE_FAIL;

	ret = start_wifi_platform();
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "connect_wifi failed");
		return CORE_FAIL;
	}
	return ret;
}

core_err delete_wifi_config() {
	m_wifi_cred.config_count = 0;
	memset(&m_wifi_cred, 0, sizeof(WiFiCredentials));

	if (write_wifi_credentials_to_persistent_mem(&m_wifi_cred) != CORE_OK) {
		CORE_LOGE(TAG, "write_wifi_credentials_to_persistent_mem failed!");
		return CORE_FAIL;
	}

	return CORE_OK;
}

core_err save_wifi_config(WifiConfig *wifi_cfg) {
	/* First read the persistent memory for any saved wifi credentials */
	read_wifi_credentials();

	/* Check if we are allowed to store mode credentials */
	if (m_wifi_cred.config_count > CONFIG_MAX_NUM_WIFI_CONFIGS) {
		CORE_LOGE(TAG,
				"Maximum number of Wi-Fi Configurations is already stored. Cannot store any more configurations!");
		CORE_LOGE(TAG, "Delete configurations to store further");
		return CORE_FAIL;
	}

	memcpy(&m_wifi_cred.cfg[m_wifi_cred.config_count++], wifi_cfg, sizeof(WifiConfig));

	if (write_wifi_credentials_to_persistent_mem(&m_wifi_cred) != CORE_OK) {
		CORE_LOGE(TAG, "write_wifi_credentials_to_persistent_mem failed!");
		return CORE_FAIL;
	}

	return CORE_OK;
}

core_err read_wifi_credentials() {
	if (read_wifi_credentials_from_persistent_mem(&m_wifi_cred) != CORE_OK) {
		CORE_LOGE(TAG, "read_wifi_credentials_from_persistent_mem failed!");
		return CORE_FAIL;
	}

	return CORE_OK;
}

WiFiCredentials * get_wifi_credentials() {
	if (m_wifi_cred.config_count == 0)
		return NULL;

	return &m_wifi_cred;
}

core_err deinit_wifi() {
	core_err ret = deinit_wifi_platform();
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "deinit_wifi failed");
		return CORE_FAIL;
	}
	wifi_connection_stat = WIFI_NOT_CONNECTED;
	return ret;
}

core_err get_wifi_sta_mac(uint8_t *mac) {
	core_err err = get_wifi_pf_sta_mac(mac);
	CORE_LOGD(TAG, "Wi-Fi station mac[%p]: %x:%x:%x:%x:%x:%x", mac, mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
	if (err != CORE_OK) {
		CORE_LOGE(TAG, "Failed to get WIFI Station MAC");
		err = CORE_FAIL;
	}
	return err;
}

WIFI_CONNECTION_STATUS get_wifi_connection_status() {
	return wifi_connection_stat;
}


/**
 *
 */
core_err get_wifi_sta_ip(char *ip) {
	if (get_wifi_pf_sta_ip(ip) == CORE_OK) {
		return CORE_OK;
	}
	return CORE_FAIL;
}


