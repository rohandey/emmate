# Wi-Fi Module
## Overview
The Wi-Fi module exposes APIs to connect to any Wi-Fi Access Point.

## Features
1. Configure the Wi-Fi SSID & Password
2. Connect to any Wi-Fi Access Point
3. Get the Wi-Fi hardware's MAC address

## How to use this module from an Application

**Note: It is not recommended to use the Wi-Fi module's APIs directly from the user's application. The application should always use the [Connectivity Module APIs](../../README.md) for basic network related operations**

However, for advanced use cases these APIs may be called directly from the application.

##### Header(s) to include

```
wifi.h
wifi_constants.h
```

#### Module Specific Configurations

The following configurations are needed for the Wi-Fi module to work.

![](img/wifi-config.png)

#### Module Specific APIs

1. Initialize Wi-Fi with SSID and Password

```c
WifiConfig wifi_cfg;
strcpy(wifi_cfg.wifi_ssid, "MY-SSID");
strcpy(wifi_cfg.wifi_pwd, "PASSWORD");

core_err ret = save_wifi_config(&wifi_cfg);
if (ret == CORE_OK) {
	init_wifi_sta_mode();
}
```

2. Connect to a Wi-Fi Access Point

```c
connect_wifi();

```

3. Check if Wi-Fi is connected

```c
/* Wait untill the wifi connection is successful */
while (get_wifi_connection_status() != WIFI_CONNECTED) {
	TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
}
CORE_LOGI(TAG, "WiFi connection successful");
```

4. Get WiFi Station's Physical Address (MAC)

```c
uint8_t wifi_sta_mac[6] = {0};
get_wifi_sta_mac(wifi_sta_mac);
CORE_LOGI(TAG, "Wi-Fi STA MAC: %02x:%02x:%02x:%02x:%02x:%02x", wifi_sta_mac[0], wifi_sta_mac[1], wifi_sta_mac[2],
			wifi_sta_mac[3], wifi_sta_mac[4], wifi_sta_mac[5]);
```

