set(srcs
	http_client_api.c
	http_client_core.c
	http_client_platform.c
	)

add_library(http STATIC ${srcs})

# Private Linkages
list(APPEND EXTRA_LIBS emmate_config)

if(CONFIG_USE_COMMON)
	list(APPEND EXTRA_LIBS common)
	if(CONFIG_USE_ERRORS)
		list(APPEND EXTRA_LIBS errors)
	endif()
	if(CONFIG_USE_DATA_STRUCTURES)
		list(APPEND EXTRA_LIBS data-structures)
	endif()
endif()

if(CONFIG_USE_LOGGING)
	list(APPEND EXTRA_LIBS logging)
endif()

if(CONFIG_USE_THREADING)
	list(APPEND EXTRA_LIBS threading)
endif()

if(CONFIG_USE_CONN)
	list(APPEND EXTRA_LIBS conn)
endif()

# Public Linkages

# Link the libraries
target_link_libraries(http PRIVATE ${EXTRA_LIBS})

# Platform Linkages
if(CONFIG_PLATFORM_ESP_IDF)
	list(APPEND IDF_LIBS
				idf::esp_http_client
			)
	target_link_libraries(http PUBLIC ${IDF_LIBS})
endif()


target_include_directories(http 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)