# SNTP Client Module
## Overview
The SNTP Client initializes and syncs the system time with an SNTP server. This module cannot be accessed directly from an application. The SNTP module is controlled by the SysTime Module.

**All the time related functionalities can be used through the SysTime Module.**

## Features