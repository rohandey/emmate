/*
 * File Name: system_constants.h
 * File Path: /emmate/src/system/system_constants.h
 * Description:
 *
 *  Created on: 11-Jun-2019
 *      Author: Rohan Dey
 */

#ifndef SYSTEM_CONSTANTS_H_
#define SYSTEM_CONSTANTS_H_

#define MODE_NAME_STARTUP						"Startup Mode"
#define MODE_NAME_SYSTEM_CONFIG_MODE_FACTORY	"Factory Device Config Mode"
#define MODE_NAME_NETWORK_CONNECTION_MODE		"Network Connection Mode"
#define MODE_NAME_SETUP_SYSTIME_SNTP			"Setup System Time Mode"
#define MODE_NAME_SOM_REGISTRATION_MODE			"SOM Registration Mode"
#define MODE_NAME_SYSTEM_HEARTBEAT				"System Heartbeat Mode"
#define MODE_NAME_START_APPLICATION				"Start Application Mode"
#define MODE_NAME_MAINTENANCE_MODE				"System Maintenance Mode"
#define MODE_NAME_OTA_UPDATE_MODE				"FOTA Update Mode"
#define MODE_NAME_SYS_DO_NOTHING				"System Do Nothing"

#endif /* SYSTEM_CONSTANTS_H_ */
