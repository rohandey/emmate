/*
 * File Name: systime.h
 * File Path: /emmate/src/system/systime/systime.h
 * Description:
 *
 *  Created on: 27-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef SYSTIME_H_
#define SYSTIME_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_error.h"
#include "core_constant.h"
#include <stdbool.h>
#include <limits.h>
#include <float.h>
//#if CONFIG_PLATFORM_ESP_IDF
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>

/**
 * @brief  	Set the system time.
 * 			This function internally calls settimeofday(const struct timeval *tv, const struct timezone *tz) standard
 * 			function and returns the same values as returned by settimeofday.
 * 			See settimeofday(2) - Linux man page for more details
 *
 * @param[in]	tv Pre-populated struct timeval
 * @param[in]	tz Pre-populated struct timezone
 *
 * @return
 * 		- 0 		on success
 * 		- -1 		on failure
 * 		- EPERM		This process cannot set the clock because it is not privileged.
 * 		- ENOSYS	The operating system does not support setting time zone information, and tzp is not a null pointer.
 * */
int core_settimeofday(const struct timeval *tv, const struct timezone *tz);

/**
 * @brief  	Gets the system time as an out param of type struct tm
 *
 * @param[out]	curr_time Internally populated struct tm
 *
 * @return
 * 		- 0 		on success
 * 		- -1 		on failure
 * */
int get_systime(struct tm *curr_time);

/**
 * @brief  	Gets the system time in seconds as an out param of type time_t
 *
 * @param[out]	curr_time_secs The system time in seconds
 *
 * @return
 * 		- 0 		on success
 * 		- -1 		on failure
 * */
int get_systime_seconds(time_t *curr_time_secs);

/**
 * @brief  	Gets the system time in milli-seconds as an out param of type uint64_t
 *
 * @param[out]	curr_time_ms The system time in milli-seconds
 *
 * @return
 * 		- 0 		on success
 * 		- -1 		on failure
 * */
int get_systime_millis(uint64_t *curr_time_ms);

/**
 * @brief  	Checks whether the system time has been set or not.
 * 			The system time can be set by either the SNMP module or by calling 'core_settimeofday'
 *
 * @return
 * 		- true		if system time is set
 * 		- false		if system time is not set
 * */
bool is_systime_set();

/**
 * @brief  	Converts a struct tm into time_t. Internally calls mktime
 *
 * @param[in]	time	struct tm*
 *
 * @return
 * 		time_t
 * */
time_t convert_tm_to_seconds(struct tm* time);

/**
 * @brief  	Converts a struct tm into milli-seconds (uint64_t). Internally calls mktime
 *
 * @param[in]	time	struct tm*
 *
 * @return
 * 		uint64_t
 * */
uint64_t convert_tm_to_millis(struct tm* time);

/**
 * @brief  	Converts seconds given in 'time_t' format to struct tm. Internally calls gmtime_r
 *
 * @param[in]	seconds	time_t
 * @param[in]	time	struct tm*
 *
 * @return	struct tm *
 * */
struct tm * convert_seconds_to_tm(const time_t seconds, struct tm *time);

/**
 * @brief  	Converts millisecond given in 'long long' format to struct tm. Internally calls gmtime_r
 *
 * @param[in]	millis	long long
 * @param[in]	time	struct tm*
 *
 * @return	struct tm *
 * */
struct tm * convert_millis_to_tm(uint64_t millis, struct tm *time);

/**
 * @brief  	Converts seconds given in 'string' format to struct tm. Internally calls gmtime_r
 *
 * @param[in]	secs	char *
 * @param[in]	time	struct tm*
 *
 * @return	struct tm *
 * */
struct tm * convert_str_seconds_to_tm(char *secs, struct tm *time);

/**
 * @brief  	Converts millisecond given in 'string' format to struct tm. Internally calls gmtime_r
 *
 * @param[in]	millis	char *
 * @param[in]	time	struct tm*
 *
 * @return	struct tm *
 * */
struct tm * convert_str_millis_to_tm(char *millis, struct tm *time);

/**
 * @brief		This function evaluates the difference of the input time with the current time and returns the difference in seconds
 * 				i.e., (input time) - (current time)
 * 				This function internally calls difftime function
 *
 *
 * @note		If the current time is less than input time then a -ve value is returned
 * 				If the system time is not set then this function returns -DBL_MAX
 *
 * @param[in]	time	struct tm*
 *
 * @return	double		Difference of time in seconds.
 * 						If the system time is not set then this function returns -DBL_MAX
 * */
double diff_time_with_now(struct tm *time_to_cmp);

/**
 * @brief		This function evaluates the difference of the 2 input times and returns the difference in seconds
 * 				i.e., (end_time) - (begin_time)
 * 				This function internally calls difftime function
 *
 * @note		If the end_time is less than begin_time then a -ve value is returned
 *
 * @param[in]	time	struct tm*
 *
 * @return	double		Difference of time in seconds.
 * */
double diff_between_time(struct tm *end_time, struct tm *begin_time);

//#endif	// CONFIG_PLATFORM_ESP_IDF

//typedef struct {
//#if CONFIG_PLATFORM_ESP_IDF
//	time_t now;
//	struct tm timeinfo;
//#else
//#endif
//} SysTimeCore;

/**
 * @brief		This function starts a thread to acquire system time from SNTP
 *
 * @return
 * 			CORE_OK:	If thread is successfully started
 * 			CORE_FAIL:	If thread creation fails
 * */
core_err start_system_time_module();

#ifdef __cplusplus
}
#endif

#endif /* SYSTIME_H_ */
