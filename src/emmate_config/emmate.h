/*
 * emmate.h
 *
 *  Created on: 06-Apr-2020
 *      Author: Rohan Dey
 */

#ifndef EMMATE_H_
#define EMMATE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"

#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#include "threading.h"

#ifdef __cplusplus
}
#endif

#endif /* EMMATE_H_ */
