set(srcs
	threading.c
	threading_platform.c
	)

add_library(threading STATIC ${srcs})

list(APPEND EXTRA_LIBS emmate_config)
					
if(CONFIG_USE_COMMON)
	list(APPEND EXTRA_LIBS common)
	if(CONFIG_USE_ERRORS)
		list(APPEND EXTRA_LIBS errors)
	endif()
endif()

if(CONFIG_USE_LOGGING)
	list(APPEND EXTRA_LIBS logging)
endif()

target_link_libraries(threading PRIVATE ${EXTRA_LIBS})

if(CONFIG_PLATFORM_ESP_IDF)
	list(APPEND IDF_LIBS
				idf::freertos
			)
	target_link_libraries(threading PRIVATE ${IDF_LIBS})
endif()

target_include_directories(threading 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)