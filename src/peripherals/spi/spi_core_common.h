/*
 * File Name: spi_core_common.h
 *
 * Description:
 *
 *  Created on: 28-Sep-2019
 *      Author: Noyel Seth
 */

#ifndef SRC_PERIPHERALS_SPI_SPI_CORE_COMMON_H_
#define SRC_PERIPHERALS_SPI_SPI_CORE_COMMON_H_

#include "spi_common_platform.h"

#ifdef __cplusplus
extern "C" {
#endif

//alias for different chips
//typedef enum {
//#define SPI_HOST    SPI1_HOST
#define	SPI_HOST_1 SPI_PF_HOST_1 /** < SPI Bus 1 */
#define	SPI_HOST_2 SPI_PF_HOST_2 /** < SPI BUS 2 */
//} SPI_HOST;

//Maximum amount of bytes that can be put in one DMA descriptor
#define SPI_CORE_MAX_DMA_LEN	SPI_PF_MAX_DMA_LEN

typedef enum {
	SPI_BUS_NOT_INTITIALIZE = 0, SPI_BUS_READY, SPI_BUS_IDLE, SPI_BUS_BUSY,
} SPI_BUS_STATUS;

typedef struct {
	uint8_t spi_host; /** < SPI peripheral that controls this bus */
	int mosi_io_num; /** < GPIO pin for Master Out Slave In (=spi_d) signal, or -1 if not used. */
	int miso_io_num; /** < GPIO pin for Master In Slave Out (=spi_q) signal, or -1 if not used. */
	int sclk_io_num; /** < GPIO pin for Spi CLocK signal, or -1 if not used. */
	int max_transfer_sz; /** < Maximum transfer size, in bytes. Defaults to 4094 if 0. */
	SPI_BUS_STATUS stat; /** < SPI bus status */
} spi_bus_config;

#ifdef __cplusplus
}
#endif

#endif /* SRC_PERIPHERALS_SPI_SPI_CORE_COMMON_H_ */
