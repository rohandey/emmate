/*
 * File Name: i2c_core.c
 * File Path: /emmate/src/peripherals/i2c/i2c_core.c
 * Description:
 *
 *  Created on: 30-May-2019
 *      Author: Noyel Seth
 */

//#include "i2c_core_local.h"
#include "i2c_core.h"


//#include "threading.h"
#include "core_constant.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#include <string.h>
#include <stdlib.h>

#define TAG 	LTAG_I2C

I2C_CORE_DRV_OBJ i2c_core_drv_obj[I2C_CORE_NUM_MAX] = { 0 };

/**************************************************************************************************************/
/**
 *
 */
I2C_CORE_CLIENT_OBJ* _client_i2c_instance_get(uint32_t i2c_port_num) {
	I2C_CORE_CLIENT_OBJ *i2c_core_client_obj = (I2C_CORE_CLIENT_OBJ*) calloc(1, sizeof(I2C_CORE_CLIENT_OBJ));
	if (i2c_core_client_obj == NULL) {
		CORE_LOGD(TAG, "Failed to allocat memory for i2c_core_client_obj");
	} else {
		i2c_core_client_obj->i2c_drv_obj = &i2c_core_drv_obj[i2c_port_num];
		CORE_LOGD(TAG, "I2C_CORE_DRV_OBJ's address %p", i2c_core_client_obj->i2c_drv_obj);
		uint16_t val = rand();
		sprintf(i2c_core_client_obj->node_name, "%s%d-%d", I2C_CLIENT_NODE_PREFIX_NAME, i2c_port_num, val);
		CORE_LOGD(TAG, "I2C client node name %s", i2c_core_client_obj->node_name);

		if (i2c_core_client_obj->i2c_drv_obj->i2c_client_list != NULL) {
			if (gll_add(i2c_core_client_obj->i2c_drv_obj->i2c_client_list, i2c_core_client_obj->node_name, 0) == C_OK) {
				return i2c_core_client_obj;
			} else {
				// TODO: Nothing to do.
			}
		} else {
			CORE_LOGE(TAG, "I2C_CORE_DRV_OBJ's client list is NULL");
		}
	}
	return DRV_INVALID_HANDLE;
}

/**
 *
 */
I2C_CORE_DRV_OBJ* _drv_i2c_instance_get(uint32_t i2c_port_num) {
	if (i2c_port_num >= I2C_CORE_NUM_MAX) {
		return DRV_INVALID_HANDLE;
	} else {
		if (i2c_core_drv_obj[i2c_port_num].status == I2C_CORE_NOT_INTITIALIZE) {
			i2c_core_drv_obj[i2c_port_num].i2c_conn_data.i2c_port = i2c_port_num;
			i2c_core_drv_obj[i2c_port_num].i2c_conn_data.is_internal_pullup_enable = false;
			i2c_core_drv_obj[i2c_port_num].i2c_conn_data.scl_io_num = 0;
			i2c_core_drv_obj[i2c_port_num].i2c_conn_data.sda_io_num = 0;
			i2c_core_drv_obj[i2c_port_num].i2c_conn_data.master.clk_speed = 0;
			if (i2c_core_drv_obj[i2c_port_num].mutex_handle == NULL) {
				i2c_core_drv_obj[i2c_port_num].mutex_handle = SemaphoreCreateMutex();
				if (i2c_core_drv_obj[i2c_port_num].mutex_handle == NULL) {
					CORE_LOGE(TAG, "Failed to create SemaphoreMutex");
					return DRV_INVALID_HANDLE;
				} else {
					if (i2c_core_drv_obj[i2c_port_num].i2c_client_list == NULL) {
						i2c_core_drv_obj[i2c_port_num].i2c_client_list = gll_init();
						if (i2c_core_drv_obj[i2c_port_num].i2c_client_list == NULL) {
							CORE_LOGE(TAG, "Failed to create i2c_client_list for i2c port %d", i2c_port_num);
							return DRV_INVALID_HANDLE;
						}
					}
				}
			} else {
				//CORE_LOGE(TAG, "i2c_core_drv_obj[i2c_port_num].mutex_handle != NULL");
			}
		} else {
			//CORE_LOGI(TAG, "i2c_core_drv_obj[i2c_port_num].status = %d\r\n", i2c_core_drv_obj[i2c_port_num].status);
		}
		return &i2c_core_drv_obj[i2c_port_num];
	}
	return DRV_INVALID_HANDLE;
}

/**
 *
 */
bool _find_running_i2c_client(gll_t* list, char* node_name, size_t *node_pos) {
	bool ret = false;
	//CORE_LOGD(TAG, "client list add %p", list);
	//CORE_LOGD(TAG, "%s: node_name %s",__func__, node_name);
	gll_t * i2c_client_list = list;
	if (i2c_client_list != NULL && i2c_client_list->size != -1) {
		size_t list_size = i2c_client_list->size;
		CORE_LOGD(TAG, "i2c client list size: %d", list_size);
		size_t count = 0;
		for (count = 0; count < list_size; count++) {
			char *name = (char*) gll_get(i2c_client_list, count);
			CORE_LOGD(TAG, "client node name %s", name);
			if (strcmp(node_name, name) == 0) {
				ret = true;
				CORE_LOGD(TAG, "Matched client node name %s", name);
				*node_pos = count;
				return ret;
			}
		}
	}
	*node_pos = -1;
	return ret;
}

/**************************************************************************************************************
 **************************************************************************************************************
 **************************************************************************************************************/
/**
 *
 */
I2C_CORE_DRV_HANDLE init_i2c_core_master(uint32_t i2c_port, uint32_t sda_io_num, uint32_t scl_io_num,
		uint32_t clk_speed, bool is_internal_pullup_enable) {

	core_err ret;
	uint32_t i2c_mode = I2C_CORE_MODE_MASTER;

	I2C_CORE_DRV_OBJ *i2c_core_drv_obj = NULL;
	I2C_CORE_CLIENT_OBJ *i2c_core_client_obj = NULL;

	// get the i2c intance
	i2c_core_drv_obj = _drv_i2c_instance_get(i2c_port);

	if (i2c_core_drv_obj != DRV_INVALID_HANDLE) {
		// init the i2c intance with configured data pass by user.
		if (i2c_core_drv_obj->status == I2C_CORE_NOT_INTITIALIZE) {

			i2c_core_drv_obj->i2c_conn_data.i2c_mode = i2c_mode;
			i2c_core_drv_obj->i2c_conn_data.i2c_port = i2c_port;

			i2c_core_drv_obj->i2c_conn_data.sda_io_num = sda_io_num;
			i2c_core_drv_obj->i2c_conn_data.scl_io_num = scl_io_num;

			i2c_core_drv_obj->i2c_conn_data.master.clk_speed = clk_speed;

			i2c_core_drv_obj->i2c_conn_data.is_internal_pullup_enable = is_internal_pullup_enable;

			// Initialize the i2c
			ret = init_platform_i2c(&i2c_core_drv_obj->i2c_conn_data);
			if (ret == CORE_OK) {
				i2c_core_drv_obj->status = I2C_CORE_READY;

				// create i2c client for this current i2c intance
				i2c_core_client_obj = _client_i2c_instance_get(i2c_port);
				if (i2c_core_client_obj == NULL) {
					CORE_LOGE(TAG, "Failed to create i2c_core_client_obj");
				} else {
					i2c_core_drv_obj->numClients++;
					return (I2C_CORE_DRV_HANDLE) i2c_core_client_obj;
				}

			}
		} else {
			if (i2c_core_drv_obj->numClients >= CORE_I2C_CLIENTS_NUMBER) {
				return DRV_INVALID_HANDLE;
			} else {
				if (i2c_core_drv_obj->i2c_conn_data.scl_io_num == scl_io_num
						&& i2c_core_drv_obj->i2c_conn_data.sda_io_num == sda_io_num) {

					/*
					 * create i2c client for this current i2c intance
					 * if Passed SDA & SCL is equal to the running i2c SDA & SCL signal line
					 */
					i2c_core_client_obj = _client_i2c_instance_get(i2c_port);
					if (i2c_core_client_obj == NULL) {
						CORE_LOGE(TAG, "Failed to create i2c_core_client_obj");
					} else {
						i2c_core_drv_obj->numClients++;
						return (I2C_CORE_DRV_HANDLE) i2c_core_client_obj;
					}
				} else {
					return DRV_INVALID_HANDLE;
				}
			}
		}
	}

	return DRV_INVALID_HANDLE;
}

/**
 *
 */
I2C_CORE_DRV_HANDLE init_i2c_core_slave(uint32_t i2c_port, uint32_t sda_io_num, uint32_t scl_io_num,
		uint8_t addr_10bit_en, uint16_t slave_addr, bool is_internal_pullup_enable) {

	core_err ret;
	uint32_t i2c_mode = I2C_CORE_MODE_SLAVE;

	I2C_CORE_DRV_OBJ *i2c_core_drv_obj = NULL;
	I2C_CORE_CLIENT_OBJ *i2c_core_client_obj = NULL;

	// get the i2c intance
	i2c_core_drv_obj = _drv_i2c_instance_get(i2c_port);

	if (i2c_core_drv_obj != DRV_INVALID_HANDLE) {
		// init the i2c intance with configured data pass by user.
		if (i2c_core_drv_obj->status == I2C_CORE_NOT_INTITIALIZE) {

			i2c_core_drv_obj->i2c_conn_data.i2c_mode = i2c_mode;
			i2c_core_drv_obj->i2c_conn_data.i2c_port = i2c_port;

			i2c_core_drv_obj->i2c_conn_data.sda_io_num = sda_io_num;
			i2c_core_drv_obj->i2c_conn_data.scl_io_num = scl_io_num;

			i2c_core_drv_obj->i2c_conn_data.slave.addr_10bit_en = addr_10bit_en;
			i2c_core_drv_obj->i2c_conn_data.slave.slave_addr = slave_addr;

			i2c_core_drv_obj->i2c_conn_data.is_internal_pullup_enable = is_internal_pullup_enable;

			// Initialize the i2c
			ret = init_platform_i2c(&i2c_core_drv_obj->i2c_conn_data);
			if (ret == CORE_OK) {
				i2c_core_drv_obj->status = I2C_CORE_READY;

				// create i2c client for this current i2c intance
				i2c_core_client_obj = _client_i2c_instance_get(i2c_port);
				if (i2c_core_client_obj == NULL) {
					CORE_LOGE(TAG, "Failed to create i2c_core_client_obj");
				} else {
					i2c_core_drv_obj->numClients++;
					return (I2C_CORE_DRV_HANDLE) i2c_core_client_obj;
				}

			}
		} else {
			if (i2c_core_drv_obj->numClients >= CORE_I2C_CLIENTS_NUMBER) {
				return DRV_INVALID_HANDLE;
			} else {
				if (i2c_core_drv_obj->i2c_conn_data.scl_io_num == scl_io_num
						&& i2c_core_drv_obj->i2c_conn_data.sda_io_num == sda_io_num) {

					/*
					 * create i2c client for this current i2c intance
					 * if Passed SDA & SCL is equal to the running i2c SDA & SCL signal line
					 */
					i2c_core_client_obj = _client_i2c_instance_get(i2c_port);
					if (i2c_core_client_obj == NULL) {
						CORE_LOGE(TAG, "Failed to create i2c_core_client_obj");
					} else {
						i2c_core_drv_obj->numClients++;
						return (I2C_CORE_DRV_HANDLE) i2c_core_client_obj;
					}
				} else {
					return DRV_INVALID_HANDLE;
				}
			}
		}
	}

	return DRV_INVALID_HANDLE;
}

/**
 *
 */
core_err i2c_core_master_write_slave(I2C_CORE_DRV_HANDLE i2c_drv_handle, uint8_t slave_addr, uint8_t cmd_addr,
		uint8_t *data_wr, size_t size) {
	core_err ret = CORE_FAIL;

	I2C_CORE_DRV_OBJ *i2c_core_drv_obj = NULL;
	I2C_CORE_CLIENT_OBJ *i2c_core_client_obj = NULL;

	size_t node_pos = 0;

	if (i2c_drv_handle != NULL) {
		i2c_core_client_obj = (I2C_CORE_CLIENT_OBJ *) i2c_drv_handle;
		i2c_core_drv_obj = i2c_core_client_obj->i2c_drv_obj;
		if (i2c_core_drv_obj->mutex_handle != NULL) {
			// take mutex for complete the process
			SemaphoreTake(i2c_core_drv_obj->mutex_handle, DELAY_2_SEC);
			bool client_found = _find_running_i2c_client(i2c_core_client_obj->i2c_drv_obj->i2c_client_list,
					i2c_core_client_obj->node_name, &node_pos);
			if (client_found == true && node_pos != -1) {
				ret = i2c_platform_master_write_slave(i2c_core_client_obj->i2c_drv_obj->i2c_conn_data.i2c_port,
						slave_addr, cmd_addr, data_wr, size);
			} else {
				ret = CORE_ERR_INVALID_ARG;
			}
			// give mutex after complete the process
			SemaphoreGive(i2c_core_drv_obj->mutex_handle);
		}
	}
	return ret;

}

/**
 *
 */
core_err i2c_core_master_write_slave_without_cmd(I2C_CORE_DRV_HANDLE i2c_drv_handle, uint8_t slave_addr,
		uint8_t *data_wr, size_t size) {
	core_err ret = CORE_FAIL;

	I2C_CORE_DRV_OBJ *i2c_core_drv_obj = NULL;
	I2C_CORE_CLIENT_OBJ *i2c_core_client_obj = NULL;

	size_t node_pos = 0;

	if (i2c_drv_handle != NULL) {
		i2c_core_client_obj = (I2C_CORE_CLIENT_OBJ *) i2c_drv_handle;
		i2c_core_drv_obj = i2c_core_client_obj->i2c_drv_obj;
		if (i2c_core_drv_obj->mutex_handle != NULL) {
			// take mutex for complete the process
			SemaphoreTake(i2c_core_drv_obj->mutex_handle, DELAY_2_SEC);
			bool client_found = _find_running_i2c_client(i2c_core_client_obj->i2c_drv_obj->i2c_client_list,
					i2c_core_client_obj->node_name, &node_pos);
			if (client_found == true && node_pos != -1) {
				ret = i2c_platform_master_write_slave_without_cmd(i2c_core_client_obj->i2c_drv_obj->i2c_conn_data.i2c_port,
						slave_addr, data_wr, size);
			} else {
				ret = CORE_ERR_INVALID_ARG;
			}
			// give mutex after complete the process
			SemaphoreGive(i2c_core_drv_obj->mutex_handle);
		}
	}
	return ret;

}

/**
 *
 */
core_err i2c_core_master_read_slave(I2C_CORE_DRV_HANDLE i2c_drv_handle, uint8_t slave_addr, uint8_t cmd_addr,
		uint8_t *data_rd, size_t size, uint32_t delay_before_restart) {
	core_err ret = CORE_FAIL;

	I2C_CORE_DRV_OBJ *i2c_core_drv_obj = NULL;
	I2C_CORE_CLIENT_OBJ *i2c_core_client_obj = NULL;

	size_t node_pos = 0;

	if (i2c_drv_handle != NULL) {
		i2c_core_client_obj = (I2C_CORE_CLIENT_OBJ *) i2c_drv_handle;
		i2c_core_drv_obj = i2c_core_client_obj->i2c_drv_obj;
		if (i2c_core_drv_obj->mutex_handle != NULL) {

			// take mutex for complete the process
			SemaphoreTake(i2c_core_drv_obj->mutex_handle, DELAY_2_SEC);
			bool client_found = _find_running_i2c_client(i2c_core_client_obj->i2c_drv_obj->i2c_client_list,
					i2c_core_client_obj->node_name, &node_pos);

			if (client_found == true && node_pos != -1) {
				ret = i2c_platform_master_read_slave(i2c_core_client_obj->i2c_drv_obj->i2c_conn_data.i2c_port,
						slave_addr, cmd_addr, data_rd, size, delay_before_restart);
			} else {
				ret = CORE_ERR_INVALID_ARG;
			}

			// give mutex after complete the process
			SemaphoreGive(i2c_core_drv_obj->mutex_handle);
		}
	}
	return ret;
}

/**
 *
 */
core_err i2c_core_master_read_slave_without_cmd(I2C_CORE_DRV_HANDLE i2c_drv_handle, uint8_t slave_addr,
		uint8_t *data_rd, size_t size) {
	core_err ret = CORE_FAIL;

	I2C_CORE_DRV_OBJ *i2c_core_drv_obj = NULL;
	I2C_CORE_CLIENT_OBJ *i2c_core_client_obj = NULL;

	size_t node_pos = 0;

	if (i2c_drv_handle != NULL) {
		i2c_core_client_obj = (I2C_CORE_CLIENT_OBJ *) i2c_drv_handle;
		i2c_core_drv_obj = i2c_core_client_obj->i2c_drv_obj;
		if (i2c_core_drv_obj->mutex_handle != NULL) {

			// take mutex for complete the process
			SemaphoreTake(i2c_core_drv_obj->mutex_handle, DELAY_2_SEC);
			bool client_found = _find_running_i2c_client(i2c_core_client_obj->i2c_drv_obj->i2c_client_list,
					i2c_core_client_obj->node_name, &node_pos);
			if (client_found == true && node_pos != -1) {
				ret = i2c_platform_master_read_slave_without_cmd(
						i2c_core_client_obj->i2c_drv_obj->i2c_conn_data.i2c_port, slave_addr, data_rd, size);
			} else {
				ret = CORE_ERR_INVALID_ARG;
			}

			// give mutex after complete the process
			SemaphoreGive(i2c_core_drv_obj->mutex_handle);
		}
	}
	return ret;
}

/**
 *
 */
core_err i2c_core_slave_read_master(I2C_CORE_DRV_HANDLE i2c_drv_handle, uint8_t *data_rd, size_t max_size,
		uint32_t ticks_to_wait) {
	core_err ret = CORE_FAIL;

	I2C_CORE_DRV_OBJ *i2c_core_drv_obj = NULL;
	I2C_CORE_CLIENT_OBJ *i2c_core_client_obj = NULL;

	size_t node_pos = 0;

	if (i2c_drv_handle != NULL) {
		i2c_core_client_obj = (I2C_CORE_CLIENT_OBJ *) i2c_drv_handle;
		i2c_core_drv_obj = i2c_core_client_obj->i2c_drv_obj;
		if (i2c_core_drv_obj->mutex_handle != NULL) {

			// take mutex for complete the process
			SemaphoreTake(i2c_core_drv_obj->mutex_handle, DELAY_2_SEC);
			bool client_found = _find_running_i2c_client(i2c_core_client_obj->i2c_drv_obj->i2c_client_list,
					i2c_core_client_obj->node_name, &node_pos);
			if (client_found == true && node_pos != -1) {
				ret = i2c_platform_slave_read_master(i2c_core_client_obj->i2c_drv_obj->i2c_conn_data.i2c_port, data_rd,
						max_size, ticks_to_wait);
			} else {
				ret = CORE_ERR_INVALID_ARG;
			}

			// give mutex after complete the process
			SemaphoreGive(i2c_core_drv_obj->mutex_handle);
		}
	}

	return ret;
}

/**
 *
 */
core_err i2c_core_slave_write_master(I2C_CORE_DRV_HANDLE i2c_drv_handle, uint8_t *data_wr, size_t size,
		uint32_t ticks_to_wait) {
	core_err ret = CORE_FAIL;

	I2C_CORE_DRV_OBJ *i2c_core_drv_obj = NULL;
	I2C_CORE_CLIENT_OBJ *i2c_core_client_obj = NULL;

	size_t node_pos = 0;

	if (i2c_drv_handle != NULL) {
		i2c_core_client_obj = (I2C_CORE_CLIENT_OBJ *) i2c_drv_handle;
		i2c_core_drv_obj = i2c_core_client_obj->i2c_drv_obj;
		if (i2c_core_drv_obj->mutex_handle != NULL) {

			// take mutex for complete the process
			SemaphoreTake(i2c_core_drv_obj->mutex_handle, DELAY_2_SEC);
			bool client_found = _find_running_i2c_client(i2c_core_client_obj->i2c_drv_obj->i2c_client_list,
					i2c_core_client_obj->node_name, &node_pos);
			if (client_found == true && node_pos != -1) {
				ret = i2c_platform_slave_write_master(i2c_core_client_obj->i2c_drv_obj->i2c_conn_data.i2c_port, data_wr,
						size, ticks_to_wait);
				if(ret != -1){
					ret = CORE_OK;
				}else{
					ret = CORE_FAIL;
				}
			} else {
				ret = CORE_ERR_INVALID_ARG;
			}

			// give mutex after complete the process
			SemaphoreGive(i2c_core_drv_obj->mutex_handle);
		}
	}

	return ret;
}

/**
 *
 */
core_err deinit_i2c_core_master(I2C_CORE_DRV_HANDLE i2c_drv_handle) {
	core_err ret = CORE_FAIL;
	size_t node_pos = 0;

	I2C_CORE_DRV_OBJ *i2c_core_drv_obj = NULL;
	I2C_CORE_CLIENT_OBJ *i2c_core_client_obj = NULL;
	size_t i2c_port_num = 0;

	if (i2c_drv_handle != NULL) {
		i2c_core_client_obj = (I2C_CORE_CLIENT_OBJ *) i2c_drv_handle;
//		i2c_port_num = i2c_core_client_obj->i2c_drv_obj->i2c_conn_data.i2c_port;
		size_t list_size = i2c_core_client_obj->i2c_drv_obj->i2c_client_list->size;
		bool client_found = _find_running_i2c_client(i2c_core_client_obj->i2c_drv_obj->i2c_client_list,
				i2c_core_client_obj->node_name, &node_pos);

		if (list_size > 1) {
			if (client_found == true && node_pos != -1) {
				gll_remove(i2c_core_client_obj->i2c_drv_obj->i2c_client_list, node_pos);
				i2c_core_client_obj->i2c_drv_obj->numClients--;
				free(i2c_core_client_obj);
				i2c_drv_handle = i2c_core_client_obj = NULL;
				return ret = CORE_OK;
			} else {
				return ret = CORE_ERR_INVALID_ARG;
			}
		} else {
			if (client_found == true && node_pos != -1) {
				gll_remove(i2c_core_client_obj->i2c_drv_obj->i2c_client_list, node_pos);
				i2c_core_client_obj->i2c_drv_obj->numClients--;
				ret = deinit_platform_i2c(&i2c_core_client_obj->i2c_drv_obj->i2c_conn_data);
				if (ret == CORE_OK) {
					i2c_core_drv_obj = i2c_core_client_obj->i2c_drv_obj;
					free(i2c_core_client_obj);
					i2c_drv_handle = i2c_core_client_obj = NULL;

					i2c_core_drv_obj->status = I2C_CORE_NOT_INTITIALIZE;
					i2c_core_drv_obj->i2c_conn_data.i2c_port = 0;
					i2c_core_drv_obj->i2c_conn_data.is_internal_pullup_enable = false;
					i2c_core_drv_obj->i2c_conn_data.scl_io_num = 0;
					i2c_core_drv_obj->i2c_conn_data.sda_io_num = 0;
					i2c_core_drv_obj->i2c_conn_data.master.clk_speed = 0;
					i2c_core_drv_obj->i2c_conn_data.slave.addr_10bit_en = 0;
					i2c_core_drv_obj->i2c_conn_data.slave.slave_addr = 0;

					if (i2c_core_drv_obj->mutex_handle != NULL) {
						SemaphoreDelete(i2c_core_drv_obj->mutex_handle);
						i2c_core_drv_obj->mutex_handle = NULL;
						if (i2c_core_drv_obj->i2c_client_list != NULL) {
							gll_destroy(i2c_core_drv_obj->i2c_client_list);
							i2c_core_drv_obj->i2c_client_list = NULL;
						}
					}
				} else {
					return ret;
				}

			} else {
				return ret = CORE_ERR_INVALID_ARG;
			}
		}
	} else {
		return ret = CORE_ERR_INVALID_ARG;
	}

	return ret;
}
