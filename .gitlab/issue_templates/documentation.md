<!--

* Use this issue template for suggesting new docs or updates to existing docs.

### Problem to solve

<!-- Include the following detail as necessary:
* What product or feature(s) affected?
* What docs or doc section affected? Include links or paths.
* Is there a problem with a specific document, or a feature/process that's not addressed sufficiently in docs?
* Any other ideas or requests?
-->

### Further details

<!--
* Any concepts, procedures, reference info we could add to make it easier to successfully use EmMate?
* Include use cases, benefits, and/or goals for this work.
-->

### Other links/references

/label ~Documentation